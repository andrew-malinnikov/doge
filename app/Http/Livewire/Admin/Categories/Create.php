<?php

namespace App\Http\Livewire\Admin\Categories;

use App\Store\Product\Category;
use App\Store\Product\Size;
use Livewire\Component;

class Create extends Component
{
    public $name;
    public $slug;
    public $sizeType;
    public $sizeTypes;

    public function mount()
    {
        $this->sizeTypes = Size::getAllTypes();
    }

    public function handle()
    {
        $category = Category::create([
            'name' => $this->name,
            'slug' => $this->slug,
            'size_type' => $this->sizeType,
        ]);

        session()->flash('message', 'Готово!');

        return redirect()->route('admin.products.index');
    }

    public function render()
    {
        return view('livewire.admin.categories.create');
    }
}
