<?php

namespace App\Http\Livewire\Admin\Orders;

use App\Http\Livewire\PaginatedComponent;
use App\Sales\Order\Order;
use App\Sales\Order\Status\OrderStatusManager;

class Index extends PaginatedComponent
{
    public $status = OrderStatusManager::AWAITING_PAYMENT;

    public function render()
    {
        return view('livewire.admin.orders.index', [
            'statuses' => OrderStatusManager::getWithLabels(),
            'orders' => $this->fetchOrders(),
        ]);
    }

    protected function fetchOrders()
    {
        $query = Order::query();

        if ($this->status) {
            $query->where('status', $this->status);
        }

        return $query->latest()->paginate(9);
    }
}
