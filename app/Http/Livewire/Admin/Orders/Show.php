<?php

namespace App\Http\Livewire\Admin\Orders;

use App\Sales\Order\Order;
use Livewire\Component;

class Show extends Component
{
    public $order;

    protected $listeners = [
        'orderStatusChanged' => '$refresh',
    ];

    public function mount(Order $order)
    {
        $this->order = $order;
    }

    public function render()
    {
        return view('livewire.admin.orders.show');
    }
}
