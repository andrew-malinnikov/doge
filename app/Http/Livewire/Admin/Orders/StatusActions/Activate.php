<?php

namespace App\Http\Livewire\Admin\Orders\StatusActions;

use App\Sales\Order\Order;
use App\Sales\Order\Status\Active;
use Livewire\Component;

class Activate extends Component
{
    public $order;

    public function mount(Order $order)
    {
        $this->order = $order;
    }

    public function handle()
    {
        (new Active())->transfer($this->order);
        $this->emitUp('orderStatusChanged');
    }

    public function render()
    {
        return view('livewire.admin.orders.status-actions.activate');
    }
}
