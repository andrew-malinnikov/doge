<?php

namespace App\Http\Livewire\Admin\Orders\StatusActions;

use App\Sales\Order\Order;
use App\Sales\Order\Status\Cancelled;
use Livewire\Component;

class Cancel extends Component
{
    public $order;

    public function mount(Order $order)
    {
        $this->order = $order;
    }

    public function handle()
    {
        (new Cancelled())->transfer($this->order);
        $this->emitUp('orderStatusChanged');
    }

    public function render()
    {
        return view('livewire.admin.orders.status-actions.cancel');
    }
}
