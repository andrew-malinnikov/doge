<?php

namespace App\Http\Livewire\Admin\Orders\StatusActions;

use App\Sales\Order\Order;
use App\Sales\Order\Status\Shipped;
use Livewire\Component;

class Ship extends Component
{
    public $order;
    public $trackingNumber;

    public function mount(Order $order)
    {
        $this->order = $order;
    }

    public function handle()
    {
        $this->validate([
            'trackingNumber' => ['required', 'string', 'max:255'],
        ]);
        (new Shipped())->transfer($this->order, $this->trackingNumber);
        $this->emit('orderStatusChanged');
    }

    public function render()
    {
        return view('livewire.admin.orders.status-actions.ship');
    }
}
