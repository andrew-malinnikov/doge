<?php

namespace App\Http\Livewire\Admin\Products;

use App\Store\Color\Color;
use App\Store\Product\Category;
use App\Store\Product\Product;
use App\Store\Product\Size;
use App\Store\StockData;
use Livewire\Component;

class CreateProduct extends Component
{
    public $sku;
    public $name;
    public $price;
    public $quantity;
    public $category_id;
    public $color_id;
    public $gender;
    public $size_id;
    public $weight;
    public $photo;

    public $categories;
    public $relevantSizes;

    public function mount()
    {
        $this->mountForm();
    }

    public function updatedCategoryId()
    {
        $this->fetchRelevantSizes();
    }

    protected function mountForm()
    {
        $this->categories = Category::children()->get();
        $this->category_id = optional($this->categories->first())->id;
        $this->fetchRelevantSizes();
    }

    public function fetchRelevantSizes()
    {
        $sizeType = optional(Category::find($this->category_id))->size_type;
        $this->relevantSizes = Size::whereType($sizeType)->get();
        $this->size_id = optional($this->relevantSizes->first())->id;
    }

    public function handle()
    {
        $this->runValidation();

        $product = Product::create([
            'sku' => $this->sku,
            'name' => $this->name,
            'slug' => mb_strtolower(str_replace(' ', '-', $this->name)),
            'price' => (int) $this->price * 100,
            'category_id' => $this->category_id,
            'color_id' => $this->color_id,
            'gender' => $this->gender,
            'weight' => $this->weight,
        ]);

        $product->addToStock(new StockData([
            'quantity' => (int) $this->quantity,
            'size' => Size::find($this->size_id)
        ]));

        redirect()->route('admin.products.show', ['product' => $product]);
    }

    protected function runValidation()
    {
        $this->validate([
            'name' => ['required'],
            'price' => ['required', 'numeric', 'min:1'],
            'category_id' => ['required', 'exists:categories,id'],
            'color_id' => ['nullable', 'exists:colors,id'],
            'size_id' => ['nullable', 'exists:sizes,id'],
            'weight' => ['numeric', 'min:1'],
        ]);
    }

    public function render()
    {
        return view('livewire.admin.products.create-product', [
            'colors' => Color::all()
        ]);
    }
}
