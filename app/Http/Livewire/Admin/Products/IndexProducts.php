<?php

namespace App\Http\Livewire\Admin\Products;

use App\Http\Livewire\PaginatedComponent;
use App\Store\Product\Product;

class IndexProducts extends PaginatedComponent
{
    public $search;

    public function updatingSearch()
    {
        $this->resetPage();
    }

    public function render()
    {
        return view('livewire.admin.products.index-products', [
            'products' => $this->fetchProducts(),
        ]);
    }

    protected function fetchProducts()
    {
        return Product::with('category')->paginate(8);
    }
}
