<?php

namespace App\Http\Livewire\Admin\Products;

use App\Store\Color\Color;
use App\Store\Product\Photo\PhotoUploader;
use App\Store\Product\Product;
use App\Store\Product\Size;
use App\Store\StockData;
use Illuminate\Support\Facades\Validator;
use Livewire\Component;

class ShowProduct extends Component
{
    public $product;
    public $new_stock_size_id;
    public $new_stock_quantity;

    public $new_name;
    public $new_weight;
    public $new_color_id;
    public $new_price;
    public $new_gender;

    public $photos = [];
    protected $listeners = [
        'fileSelected',
    ];

    public function mount(Product $product)
    {
        $this->product = $product;
        $this->prepareForm();
    }

    public function fileSelected($file)
    {
        $validator = Validator::make(['file' => $file], [
            'file' => ['base64max:20000'/**'base64dimensions:ratio=1'*/]
        ]);

        if (!$validator->fails()) {
            $this->resetErrorBag('file');

            return resolve(PhotoUploader::class)->handle($this->product, $file);
        }

        foreach ($validator->getMessageBag()->getMessages()['file'] as $message) {
            $this->addError('file', $message);
        }
    }

    protected function prepareForm()
    {
        $this->new_name = $this->product->name;
        $this->new_color_id = $this->product->color_id;
        $this->new_weight = $this->product->weight;
        $this->new_price = $this->product->formatted_price;
        $this->new_gender = $this->product->gender;

        $this->photos = $this->product->photos;
    }

    public function updateDetails()
    {
        $this->validate([
            'new_name' => 'required',
            'new_weight' => ['nullable', 'integer', 'min:1'],
            'new_price' => ['required', 'numeric', 'min:1'],
            'new_color_id' => ['nullable', 'exists:colors,id'],
            'new_gender' => ['nullable', 'in:1,2'],
        ]);

        $this->product->update([
            'name' => $this->new_name,
            'price' => (int) $this->new_price * 100,
            'color_id' => $this->new_color_id,
            'weight' => $this->new_weight,
            'gender' => $this->new_gender,
        ]);

        $this->emitSelf('inlineFlashMessage', 'productDetails');
    }

    public function publish()
    {
        $this->product->publish();

        $this->emitSelf('inlineFlashMessage', 'published');
    }

    public function unpublish()
    {
        $this->product->unpublish();

        $this->emitSelf('inlineFlashMessage', 'unpublished');
    }

    public function addToStock()
    {
        $this->validate([
            'new_stock_quantity' => ['required', 'numeric', 'integer', 'min:1'],
            'new_stock_size_id' => ['nullable', 'exists:sizes,id'],
        ]);

        $this->product->addToStock(new StockData([
            'quantity' => (int) $this->new_stock_quantity,
            'size' => Size::find($this->new_stock_size_id),
        ]));

        $this->reset('new_stock_quantity', 'new_stock_size_id');
        $this->emitSelf('hideModal', 'addToStock');
    }

    public function render()
    {
        return view('livewire.admin.products.show-product', [
            'availableStocks' => $this->product->fresh()->stock,
            'sizes' => $this->getProperSizes(),
            'colors' => Color::all(),
        ]);
    }

    protected function getProperSizes()
    {
        return $this->product->category->size_type
            ? Size::whereType($this->product->category->size_type)->get()
            : collect();
    }
}
