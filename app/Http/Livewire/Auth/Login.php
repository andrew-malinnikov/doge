<?php

namespace App\Http\Livewire\Auth;

use Livewire\Component;

class Login extends Component
{
    public $email;
    public $password;
    public $remember;

    public function handle()
    {
        $credentials = $this->validate([
            'email' => ['required', 'email'],
            'password' => ['required'],
        ]);

        if (!auth()->attempt($credentials, $this->remember)) {
            $this->addError('email', 'Не можем найти такой аккаунт.');

            return;
        }

        session()->flash('You are logged in');

        return redirect()->route('admin.dashboard');
    }

    public function render()
    {
        return view('livewire.auth.login');
    }
}
