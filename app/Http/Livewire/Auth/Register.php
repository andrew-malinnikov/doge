<?php

namespace App\Http\Livewire\Auth;

use App\Users\User;
use Livewire\Component;

class Register extends Component
{
    public $name;
    public $email;
    public $password;
    public $password_confirmation;

    public function handle()
    {
        $this->validate([
            'name' => ['required'],
            'email' => ['required', 'email', 'unique:users'],
            'password' => ['required', 'min:6', 'same:password_confirmation'],
        ]);

        $user = $this->createUser();

        auth()->login($user);

        return redirect()->route('admin.dashboard');
    }

    public function render()
    {
        return view('livewire.auth.register');
    }

    protected function createUser()
    {
        return User::create([
            'name' => $this->name,
            'email' => $this->email,
            'password' => bcrypt($this->password),
        ]);
    }
}
