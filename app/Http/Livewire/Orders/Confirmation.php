<?php

namespace App\Http\Livewire\Orders;

use App\Sales\Order\Order;
use Livewire\Component;

class Confirmation extends Component
{
    public $order;

    public function mount($orderNumber)
    {
        $this->order = Order::where('number', $orderNumber)->firstOrFail();
    }

    public function render()
    {
        return view('livewire.orders.confirmation');
    }
}
