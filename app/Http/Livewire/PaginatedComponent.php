<?php

namespace App\Http\Livewire;

use Livewire\Component;
use Livewire\WithPagination;

abstract class PaginatedComponent extends Component
{
    use WithPagination;

    public function paginationView()
    {
        return 'partials.pagination';
    }
}
