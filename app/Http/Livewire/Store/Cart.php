<?php

namespace App\Http\Livewire\Store;

use App\Store\Cart\CartItem;
use App\Store\Cart\CartRepository;
use App\Store\Cart\NotEnoughItemsInStockException;
use Livewire\Component;

class Cart extends Component
{
    public $cart;

    protected $listeners = [
        'cartStateChanged' => '$refresh',
    ];

    public function mount()
    {
        $this->cart = resolve(CartRepository::class)->resolve();
    }

    public function increase($productId, $sizeId)
    {
        $item = new CartItem([
            'productId' => $productId,
            'sizeId' => $sizeId,
            'quantity' => 1,
        ]);

        try {
            $this->cart->addItem($item);
            $this->emit('cartStateChanged');
        } catch (NotEnoughItemsInStockException $e) {
            $this->addError('stock_capped:'.$productId.'-'.$sizeId, 'У нас больше нету.');
        }
    }

    public function decrease($productId, $sizeId)
    {
        $item = new CartItem([
            'productId' => $productId,
            'sizeId' => $sizeId,
            'quantity' => 1,
        ]);
        $this->cart->removeItem($item);
        $this->emit('cartStateChanged');
        $this->resetErrorBag('stock_capped:'.$productId.'-'.$sizeId);
    }

    public function drop($productId, $sizeId)
    {
        $item = new CartItem([
            'productId' => $productId,
            'sizeId' => $sizeId,
            'quantity' => 1,
        ]);
        $this->cart->dropItem($item);
        $this->emit('cartStateChanged');
    }

    public function render()
    {
        return view('livewire.store.cart', [
            'items' => $this->cart->fresh()->getItems(),
            'cartCost' => $this->cart->calculate(),
        ]);
    }
}
