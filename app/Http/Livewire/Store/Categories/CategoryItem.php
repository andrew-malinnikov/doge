<?php

namespace App\Http\Livewire\Store\Categories;

use App\Store\Product\Product;
use Livewire\Component;

class CategoryItem extends Component
{
    public $category;
    public $selectedGender;

    public function mount($category, $selectedGender = null)
    {
        $this->category = $category;
        $this->selectedGender = $selectedGender;
    }

    public function genderPreferenceSet($selectedGender)
    {
        $this->selectedGender = $selectedGender;
    }

    public function render()
    {
        return view('livewire.store.categories.category-item', [
            'products' => $this->fetchProducts(),
        ]);
    }

    protected function fetchProducts()
    {
        return Product::query()
            ->where('category_id', $this->category->id)
            ->published()
            ->where(function ($query) {
                if ($this->selectedGender) {
                    $query->where('gender', $this->selectedGender)
                        ->orWhereNull('gender');
                }
            })
            ->get();
    }
}
