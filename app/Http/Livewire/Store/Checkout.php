<?php

namespace App\Http\Livewire\Store;

use App\Sales\Order\Order;
use App\Sales\Order\PlaceOrderData;
use App\Store\Cart\CartRepository;
use App\Store\Stock\Rules\SufficientStock;
use Livewire\Component;

class Checkout extends Component
{
    public $email;
    public $phone;
    public $name;
    public $line_1;
    public $city;
    public $country;
    public $zip_code;
    public $cart;

    protected $listeners = [
        'cartStateChanged' => '$refresh',
    ];

    public function mount()
    {
        $this->cart = resolve(CartRepository::class)->resolve()->fresh();
    }

    public function submit()
    {
        $this->validate([
            'cart' => [function ($attr, $value, $fail) {
                $cart = resolve(CartRepository::class)->resolveFromSession();
                if (!$cart || $cart->isEmpty()) {
                    $fail('В корзине ничего нет, как, что куда мы повезем?');
                }
            }, new SufficientStock()],
            'email' => ['required', 'email'],
            'name' => ['required', 'string', 'max:250'],
            'line_1' => ['required', 'string', 'max:250'],
            'city' => ['required', 'string', 'max:250'],
            'country' => ['required', 'string', 'max:250'],
            'zip_code' => ['required', 'string', 'max:250'],
        ]);

        $order = Order::place(PlaceOrderData::fromCheckout($this));

        resolve(CartRepository::class)->destroy();

        $this->redirect(route('order-confirmation', ['orderNumber' => $order->number]));
    }

    public function render()
    {
        return view('livewire.store.checkout');
    }
}
