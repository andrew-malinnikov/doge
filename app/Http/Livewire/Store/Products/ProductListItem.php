<?php

namespace App\Http\Livewire\Store\Products;

use App\Store\Cart\CartItem;
use App\Store\Cart\CartRepository;
use App\Store\Cart\NotEnoughItemsInStockException;
use Livewire\Component;

class ProductListItem extends Component
{
    public $product;
    public $selectedSizeId;

    public function mount($product)
    {
        $this->product = $product;
    }

    public function toggleSelectedSize($sizeId)
    {
        if ($this->getErrorBag()->has('size')) {
            $this->emitSelf('inlineFlashMessage', 'sizeSelected');
        }
        $this->resetErrorBag('size');
        $this->resetErrorBag('stock_capped');
        $this->selectedSizeId = $this->selectedSizeId == $sizeId
            ? null
            : $sizeId;
    }

    public function addToCart()
    {
        if ($this->product->category->isSizeable() && null === $this->selectedSizeId) {
            $this->addError('size', 'А скажете размерчик?');

            return;
        }

        $cart = resolve(CartRepository::class)->resolve();
        $item = new CartItem([
            'productId' => $this->product->id,
            'sizeId' => $this->selectedSizeId,
            'quantity' => 1,
        ]);

        try {
            $cart->addItem($item);
        } catch (NotEnoughItemsInStockException $e) {
            $this->addError('stock_capped', 'У нас больше нету.');

            return;
        }

        $this->emit('cartStateChanged');
    }

    public function render()
    {
        return view('livewire.store.products.product-list-item');
    }
}
