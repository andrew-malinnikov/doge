<?php

namespace App\Http\Livewire\Store;

use App\Store\Cart\CartRepository;
use App\Store\Product\Category;
use Livewire\Component;

class Store extends Component
{
    public $for = null;
    public $gender = null;

    protected $updatesQueryString = [
        'for' => ['except' => ''],
        'gender' => ['except' => ''],
    ];

    public function mount()
    {
        $this->fillQueryParamsFromRequest();
    }

    protected function fillQueryParamsFromRequest()
    {
        $this->fill(request(['for', 'gender']));
        if (!in_array($this->for, ['humans', 'doges'])) {
            $this->for = null;
        }
        if (!in_array($this->gender, [1, 2])) {
            $this->gender = null;
        }
    }

    public function toggleGender($gender)
    {
        $this->gender == $gender
            ? $this->gender = null
            : $this->gender = $gender;
    }

    public function setParentCategory($category)
    {
        $this->for = $category;

        if ('humans' != $this->for) {
            $this->gender = null;
        }
    }

    public function render()
    {
        return view('livewire.store.store', [
            'cart' => resolve(CartRepository::class)->resolveFromSession(),
            'topLevelCategories' => Category::whereNull('parent_id')->get(),
            'categories' => $this->fetchCategories(),
        ]);
    }

    protected function fetchCategories()
    {
        $query = Category::children()
            ->whereHas('products', fn ($q) => $q->published());

        if ($this->for) {
            $query->where('parent_id', Category::whereSlug($this->for)->first()->id ?? null);
        }

        return $query->get();
    }
}
