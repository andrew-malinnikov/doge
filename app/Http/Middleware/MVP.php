<?php

namespace App\Http\Middleware;

use Closure;

class MVP
{
    /**
     * Handle an incoming request.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if ('production' === app()->environment()) {
            return redirect()->route('home');
        }

        return $next($request);
    }
}
