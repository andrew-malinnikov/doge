<?php

namespace App\Providers;

use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;
use Illuminate\Support\Facades\Event;

class EventServiceProvider extends ServiceProvider
{
    /**
     * The event listener mappings for the application.
     *
     * @var array
     */
    protected $listen = [
        \App\Sales\Order\OrderWasPlaced::class => [
            \App\Store\Stock\Restock::class,
        ],
        \App\Sales\Order\Status\Events\OrderWasShipped::class => [
        ],
        \App\Sales\Order\Status\Events\OrderWasCancelled::class => [
            \App\Store\Stock\ReleaseToStock::class,
        ],
    ];

    /**
     * Register any events for your application.
     *
     * @return void
     */
    public function boot()
    {
        parent::boot();
    }
}
