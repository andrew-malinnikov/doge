<?php

namespace App\Providers;

use Illuminate\Foundation\Support\Providers\RouteServiceProvider as ServiceProvider;
use Illuminate\Support\Facades\Route;

class RouteServiceProvider extends ServiceProvider
{
    public const ADMIN_NAMESPACE = 'App\Http\Controllers\Admin';
    public const APP_NAMESPACE = 'App\Http\Controllers';
    public const AUTH_NAMESPACE = 'App\Http\Controllers\Auth';

    /**
     * The path to the "home" route for your application.
     *
     * @var string
     */
    public const HOME = '/';

    /**
     * Define your route model bindings, pattern filters, etc.
     *
     * @return void
     */
    public function boot()
    {
        parent::boot();
    }

    /**
     * Define the routes for the application.
     *
     * @return void
     */
    public function map()
    {
        $this->mapWebRoutes();
        $this->mapAdminRoutes();
        $this->mapAuthRoutes();
    }

    /**
     * Define the "web" routes for the application.
     *
     * These routes all receive session state, CSRF protection, etc.
     *
     * @return void
     */
    protected function mapWebRoutes()
    {
        Route::middleware('web')
            ->namespace(static::APP_NAMESPACE)
            ->group(base_path('routes/web.php'));
    }

    protected function mapAdminRoutes()
    {
        Route::prefix('admin')
            ->middleware(['web', 'auth:web', 'admin'])
            ->namespace(static::ADMIN_NAMESPACE)
            ->group(base_path('routes/admin.php'));
    }

    protected function mapAuthRoutes()
    {
        Route::middleware(['web'])
            ->namespace(static::AUTH_NAMESPACE)
            ->group(base_path('routes/auth.php'));
    }
}
