<?php

namespace App\Sales\Order;

use Spatie\DataTransferObject\DataTransferObject;

class Address extends DataTransferObject
{
    public $name;
    public $line_1;
    public $city;
    public $country;
    public $zip_code;
    public $delivery_notes;
}
