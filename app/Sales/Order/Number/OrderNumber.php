<?php

namespace App\Sales\Order\Number;

use Illuminate\Support\Facades\Facade;

class OrderNumber extends Facade
{
    /**
     * Get facade accessor.
     *
     * @return mixed
     */
    protected static function getFacadeAccessor()
    {
        return RandomOrderNumberGenerator::class;
    }
}
