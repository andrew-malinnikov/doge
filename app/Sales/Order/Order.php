<?php

namespace App\Sales\Order;

use App\Sales\Order\Number\OrderNumber;
use App\Sales\Order\Status\OrderStatusManager;
use App\Store\Cart\CartCost;
use App\Store\Product\Price;
use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    /**
     * Don't protect against mass assignment.
     *
     * @var array
     */
    protected $guarded = [];

    public static function place(PlaceOrderData $data)
    {
        $cost = $data->cart->calculate();

        $order = self::create([
            'status' => OrderStatusManager::AWAITING_PAYMENT,
            'number' => OrderNumber::generate(),
            'email' => $data->email,
            'phone' => $data->phone,
            'shipping_name' => $data->address->name,
            'shipping_address_line_1' => $data->address->line_1,
            'shipping_address_city' => $data->address->city,
            'shipping_address_country' => $data->address->country,
            'shipping_address_zip_code' => $data->address->zip_code,
            'shipping_delivery_notes' => $data->address->delivery_notes,
            'items_cost' => $cost->itemsCost->get(),
            'shipping_cost' => $cost->shippingCost->get(),
            'total_cost' => $cost->totalCost->get(),
        ]);

        $data->cart->getItems()->convertToOrder($order);

        OrderWasPlaced::dispatch($order->id);

        return $order;
    }

    public function items()
    {
        return $this->hasMany(OrderItem::class);
    }

    public function status()
    {
        return (new OrderStatusManager($this->status))->get();
    }

    public function costs()
    {
        return new CartCost([
            'totalCost' => new Price($this->total_cost),
            'shippingCost' => new Price($this->shipping_cost),
            'itemsCost' => new Price($this->items_cost),
        ]);
    }
}
