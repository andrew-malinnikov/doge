<?php

namespace App\Sales\Order;

use App\Store\Product\Product;
use App\Store\Product\Size;
use Illuminate\Database\Eloquent\Model;

class OrderItem extends Model
{
    /**
     * Don't protect against mass assignment.
     *
     * @var array
     */
    protected $guarded = [];

    public function product()
    {
        return $this->belongsTo(Product::class);
    }

    /**
     * @return BelongsTo
     */
    public function size()
    {
        return $this->belongsTo(Size::class);
    }

    public function productPath()
    {
        return $this->product
            ? route('admin.products.show', ['product' => $this->product])
            : '#';
    }
}
