<?php

namespace App\Sales\Order;

use Illuminate\Foundation\Events\Dispatchable;

class OrderWasPlaced
{
    use Dispatchable;

    public $orderId;

    public function __construct($orderId)
    {
        $this->orderId = $orderId;
    }
}
