<?php

namespace App\Sales\Order;

use App\Store\Cart\Cart;
use Spatie\DataTransferObject\DataTransferObject;

class PlaceOrderData extends DataTransferObject
{
    public Cart $cart;
    public $email;
    public $phone;
    public Address $address;

    public static function fromCheckout($checkout)
    {
        return new static([
            'cart' => $checkout->cart,
            'email' => $checkout->email,
            'phone' => $checkout->phone,
            'address' => new Address([
                'name' => $checkout->name,
                'line_1' => $checkout->line_1,
                'city' => $checkout->city,
                'country' => $checkout->country,
                'zip_code' => $checkout->zip_code,
            ])
        ]);
    }
}
