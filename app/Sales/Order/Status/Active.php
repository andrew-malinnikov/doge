<?php

namespace App\Sales\Order\Status;

use App\Sales\Order\Order;
use App\Sales\Order\Status\Events\OrderWasActivated;

class Active implements OrderStatus
{
    public function transfer(Order $order)
    {
        OrderWasActivated::dispatchIf(
            $order->update(['status' => $this->get()]),
            $order->id
        );
    }

    public function get()
    {
        return OrderStatusManager::ACTIVE;
    }

    public function label()
    {
        return 'Активный';
    }

    public function color()
    {
        return 'green';
    }

    public function accessors()
    {
        return collect([new Shipped(), new Cancelled()]);
    }

    public function livewireComponentName()
    {
        return 'admin.orders.status-actions.activate';
    }
}
