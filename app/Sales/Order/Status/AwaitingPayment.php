<?php

namespace App\Sales\Order\Status;

class AwaitingPayment implements OrderStatus
{
    public function get()
    {
        return OrderStatusManager::AWAITING_PAYMENT;
    }

    public function label()
    {
        return 'Ожидает оплаты';
    }

    public function color()
    {
        return 'orange';
    }

    public function accessors()
    {
        return collect([new Active(), new Cancelled()]);
    }

    public function livewireComponentName()
    {
        return null;
    }
}
