<?php

namespace App\Sales\Order\Status;

use App\Sales\Order\Order;
use App\Sales\Order\Status\Events\OrderWasCancelled;

class Cancelled implements OrderStatus
{
    public function transfer(Order $order)
    {
        $order->update([
            'status' => $this->get(),
        ]);
        OrderWasCancelled::dispatch($order->id);
    }

    public function get()
    {
        return OrderStatusManager::CANCELLED;
    }

    public function accessors()
    {
        return collect();
    }

    public function livewireComponentName()
    {
        return 'admin.orders.status-actions.cancel';
    }

    public function label()
    {
        return 'Отменен';
    }

    public function color()
    {
        return 'red';
    }
}
