<?php

namespace App\Sales\Order\Status;

use App\Sales\Order\Order;

class Delivered implements OrderStatus
{
    public function transfer(Order $order)
    {
        $order->update(['status' => $this->get()]);
    }

    public function get()
    {
        return OrderStatusManager::DELIVERED;
    }

    public function accessors()
    {
        return collect();
    }

    public function livewireComponentName()
    {
        return 'admin.orders.status-actions.deliver';
    }

    public function label()
    {
        return 'Доставлен';
    }

    public function color()
    {
        return 'indigo';
    }
}
