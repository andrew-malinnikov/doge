<?php

namespace App\Sales\Order\Status\Events;

use Illuminate\Foundation\Events\Dispatchable;

class OrderWasShipped
{
    use Dispatchable;

    public $orderId;

    public function __construct($orderId)
    {
        $this->orderId = $orderId;
    }
}
