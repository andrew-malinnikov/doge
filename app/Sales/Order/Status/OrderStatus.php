<?php

namespace App\Sales\Order\Status;

interface OrderStatus
{
    public function get();

    public function accessors();

    public function livewireComponentName();

    public function label();

    public function color();
}
