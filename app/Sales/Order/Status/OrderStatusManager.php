<?php

namespace App\Sales\Order\Status;

use InvalidArgumentException;

class OrderStatusManager
{
    public const AWAITING_PAYMENT = 'AWAITING_PAYMENT';
    public const PAYMENT_FAILED = 'PAYMENT_FAILED';
    public const ACTIVE = 'ACTIVE';
    public const SHIPPED = 'SHIPPED';
    public const DELIVERED = 'DELIVERED';
    public const CANCELLED = 'CANCELLED';

    public const AVAILABLE_STATUSES = [
        self::AWAITING_PAYMENT => AwaitingPayment::class,
        // self::PAYMENT_FAILED => PaymentFailed::class,
        self::ACTIVE => Active::class,
        self::SHIPPED => Shipped::class,
        self::DELIVERED => Delivered::class,
        self::CANCELLED => Cancelled::class,
    ];

    protected $status;

    public function __construct($status)
    {
        if (!in_array($status, array_keys(self::AVAILABLE_STATUSES))) {
            throw new InvalidArgumentException('Invalid order status');
        }
        $this->status = $status;
    }

    public function get()
    {
        $classString = self::AVAILABLE_STATUSES[$this->status];

        return new $classString();
    }

    public static function getWithLabels()
    {
        return collect(self::AVAILABLE_STATUSES)->mapWithKeys(function ($item, $key) {
            return [$key => (new $item())->label()];
        });
    }
}
