<?php

namespace App\Sales\Order\Status;

class PaymentFailed implements OrderStatus
{
    public function get()
    {
        return OrderStatusManager::PAYMENT_FAILED;
    }

    public function accessors()
    {
        return collect([new Active(), new Cancelled()]);
    }

    public function livewireComponentName()
    {
        return 'admin.orders.status-actions.fail-payment';
    }

    public function label()
    {
        return 'Оплата не прошла';
    }

    public function color()
    {
        return 'yellow';
    }
}
