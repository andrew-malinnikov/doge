<?php

namespace App\Sales\Order\Status;

use App\Sales\Order\Order;
use App\Sales\Order\Status\Events\OrderWasShipped;

class Shipped implements OrderStatus
{
    public function transfer(Order $order, $trackingNumber)
    {
        $updated = $order->update([
            'status' => $this->get(),
            'tracking_number' => $trackingNumber,
        ]);

        OrderWasShipped::dispatchIf($updated, $order->id);
    }

    public function get()
    {
        return OrderStatusManager::SHIPPED;
    }

    public function accessors()
    {
        return collect([new Delivered()]);
    }

    public function livewireComponentName()
    {
        return 'admin.orders.status-actions.ship';
    }

    public function label()
    {
        return 'Отправлен';
    }

    public function color()
    {
        return 'blue';
    }
}
