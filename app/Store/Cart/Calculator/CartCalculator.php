<?php

namespace App\Store\Cart\Calculator;

use App\Store\Cart\Cart;
use App\Store\Cart\CartCost;
use App\Store\Product\Price;

class CartCalculator
{
    public function calculate(Cart $cart)
    {
        $itemsCost = resolve(ItemsCost::class)
            ->withItems($cart->getItems())
            ->calculate();

        $shippingCost = resolve(ShippingCost::class)
            ->withItems($cart->getItems())
            ->calculate();

        return new CartCost([
            'itemsCost' => new Price($itemsCost->get()),
            'shippingCost' => new Price($shippingCost->get()),
            'totalCost' => new Price($itemsCost->get() + $shippingCost->get()),
        ]);
    }
}
