<?php

namespace App\Store\Cart\Calculator;

interface CartCostComponent
{
    public function calculate();

    public function get();
}
