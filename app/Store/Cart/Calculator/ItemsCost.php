<?php

namespace App\Store\Cart\Calculator;

class ItemsCost implements CartCostComponent
{
    protected $cost;

    protected $cartItems;

    public function calculate()
    {
        $cost = $this->cartItems->sum(function ($item) {
            return $item->cost()->get();
        });

        $this->cost = $cost;

        return $this;
    }

    public function withItems($items)
    {
        $this->cartItems = $items;

        return $this;
    }

    public function get()
    {
        return $this->cost;
    }
}
