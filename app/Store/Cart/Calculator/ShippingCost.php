<?php

namespace App\Store\Cart\Calculator;

class ShippingCost implements CartCostComponent
{
    protected $cartItems;
    protected $cost;

    public function calculate()
    {
        $this->cartItems->count() > 0
            ? $this->cost = 25000
            : $this->cost = 0;

        return $this;
    }

    public function withItems($items)
    {
        $this->cartItems = $items;

        return $this;
    }

    public function get()
    {
        return $this->cost;
    }
}
