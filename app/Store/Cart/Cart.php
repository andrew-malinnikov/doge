<?php

namespace App\Store\Cart;

use App\Store\Cart\Calculator\CartCalculator;
use App\Store\Product\Product;
use Illuminate\Database\Eloquent\Model;

class Cart extends Model
{
    /**
     * Don't protect against mass assignment.
     *
     * @var array
     */
    protected $guarded = [];

    public function calculate()
    {
        return resolve(CartCalculator::class)->calculate($this);
    }

    public function items()
    {
        return $this->belongsToMany(Product::class, 'cart_items')
            ->using(ProductInCart::class)
            ->withPivot(['quantity', 'size_id'])
            ->withTimestamps();
    }

    public function getItems()
    {
        return CartItemsCollection::make($this->items()->get());
    }

    public function totalItemsCount()
    {
        return $this->getItems()->sum('quantity');
    }

    public function isEmpty()
    {
        return 0 == $this->totalItemsCount();
    }

    public function addItem(CartItem $item)
    {
        $items = $this->getItems()->add($item);
        $this->persistItems($items);
    }

    public function removeItem(CartItem $item)
    {
        $items = $this->getItems()->remove($item);
        $this->persistItems($items);
    }

    public function dropItem(CartItem $item)
    {
        $items = $this->getItems()->drop($item);
        $this->persistItems($items);
    }

    public function clearItems()
    {
        $this->items()->detach();
    }

    protected function persistItems(CartItemsCollection $items)
    {
        $this->clearItems();

        $items->each(fn (CartItem $item) => $this->items()->attach(
            $item->productId,
            ['quantity' => $item->quantity, 'size_id' => $item->sizeId]
        ));
    }
}
