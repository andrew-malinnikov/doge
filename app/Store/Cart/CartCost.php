<?php

namespace App\Store\Cart;

use App\Store\Product\Price;
use Spatie\DataTransferObject\DataTransferObject;

class CartCost extends DataTransferObject
{
    public Price $itemsCost;
    public Price $shippingCost;
    public Price $totalCost;
}
