<?php

namespace App\Store\Cart;

use App\Sales\Order\Order;
use App\Store\Product\Price;
use App\Store\Product\Product;
use App\Store\Product\ProductInStock;
use Illuminate\Contracts\Support\Arrayable;
use Spatie\DataTransferObject\DataTransferObject;

class CartItem extends DataTransferObject implements Arrayable
{
    public $productId;
    public $quantity;
    public $sizeId;
    public $sizeLabel;
    public $productName;
    public $categoryId;
    public $categoryName;
    public $photo;

    public function product()
    {
        return Product::find($this->productId);
    }

    public function cost()
    {
        return new Price($this->product()->price * $this->quantity);
    }

    public function isInStock()
    {
        $query = ProductInStock::where('product_id', $this->productId);

        if (null === $this->sizeId) {
            $query->whereNull($this->sizeId);
        } else {
            $query->where('size_id', $this->sizeId);
        }

        $stock = $query->first();

        return $stock
            ? $this->quantity <= $stock->quantity
            : false;
    }

    public function convert(Order $order)
    {
        $order->items()->create([
            'product_id' => $this->productId,
            'quantity' => $this->quantity,
            'size_id' => $this->sizeId,
            'size_label' => $this->sizeLabel,
            'name' => $this->productName,
            'photo' => $this->photo,
            'category_id' => $this->categoryId,
            'category_name' => $this->categoryName,
        ]);
    }
}
