<?php

namespace App\Store\Cart;

use App\Sales\Order\Order;
use Illuminate\Support\Collection;

class CartItemsCollection extends Collection
{
    public static function make($items = [])
    {
        $collection = parent::make($items);

        return $collection->map(function ($item) {
            $productInCart = $item->pivot->load('size');

            return new CartItem([
                'productId' => $productInCart->product_id,
                'sizeId' => $productInCart->size_id,
                'quantity' => $productInCart->quantity,
                'productName' => $item->name,
                'sizeLabel' => optional($productInCart->size)->value,
                'categoryName' => optional($item->category)->name,
                'categoryId' => $item->category_id,
                'photo' => $item->masterPhotoPath(),
            ]);
        });
    }

    public function add($newItem)
    {
        $found = false;
        $this->transform(function ($collectionItem) use ($newItem, &$found) {
            if ($collectionItem->productId == $newItem->productId && $collectionItem->sizeId == $newItem->sizeId) {
                $collectionItem->quantity += $newItem->quantity;
                $found = true;
            }

            return $collectionItem;
        });

        if (!$found) {
            $this->push($newItem);
        }

        $this->each(function ($item) {
            if (!$item->isInStock()) {
                throw new NotEnoughItemsInStockException();
            }
        });

        return new static($this->items);
    }

    public function remove($removedItem)
    {
        $items = $this->transform(function ($collectionItem) use ($removedItem) {
            if ($collectionItem->productId == $removedItem->productId && $collectionItem->sizeId == $removedItem->sizeId) {
                $collectionItem->quantity -= $removedItem->quantity;
                if ($collectionItem->quantity <= 0) {
                    return null;
                }
            }

            return $collectionItem;
        });

        return new static($items->reject(fn ($item) => null === $item));
    }

    public function drop($removedItem)
    {
        $items = $this->transform(function ($collectionItem) use ($removedItem) {
            if ($collectionItem->productId == $removedItem->productId && $collectionItem->sizeId == $removedItem->sizeId) {
                return null;
            }

            return $collectionItem;
        });

        return new static($items->reject(fn ($item) => null === $item));
    }

    public function convertToOrder(Order $order)
    {
        $this->each->convert($order);
    }

    public function isSufficientStock()
    {
        return $this->filter->isInStock()->count() == $this->count();
    }
}
