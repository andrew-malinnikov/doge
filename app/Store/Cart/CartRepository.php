<?php

namespace App\Store\Cart;

class CartRepository
{
    public function resolve()
    {
        if ($cart = $this->resolveFromSession()) {
            return $cart;
        }

        return $this->store();
    }

    public function store()
    {
        $cart = Cart::create();

        session(['cart_id' => $cart->id]);

        return $cart;
    }

    public function resolveFromSession()
    {
        return session()->has('cart_id')
            ? Cart::find(session('cart_id'))
            : null;
    }

    public function destroy()
    {
        session()->forget('cart_id');
    }
}
