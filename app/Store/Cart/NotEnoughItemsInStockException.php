<?php

namespace App\Store\Cart;

use Exception;

class NotEnoughItemsInStockException extends Exception
{
}
