<?php

namespace App\Store\Cart;

use App\Store\Product\Size;
use Illuminate\Database\Eloquent\Relations\Pivot;

class ProductInCart extends Pivot
{
    protected $table = 'cart_items';

    /**
     * Define relationship for App\Size.
     *
     * @return BelongsTo
     */
    public function size()
    {
        return $this->belongsTo(Size::class);
    }
}
