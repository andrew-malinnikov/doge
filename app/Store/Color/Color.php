<?php

namespace App\Store\Color;

use Illuminate\Database\Eloquent\Model;

class Color extends Model
{
    /**
     * Indicates if the IDs are auto-incrementing.
     *
     * @var bool
     */
    public $incrementing = false;

    /**
     * Don't protect against mass assignment.
     *
     * @var array
     */
    protected $guarded = [];
}
