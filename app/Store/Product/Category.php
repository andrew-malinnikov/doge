<?php

namespace App\Store\Product;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    /**
     * Don't protect against mass assignment.
     *
     * @var array
     */
    protected $guarded = [];

    /**
     * Query constraint.
     *
     * @param  Illuminate\Database\Eloquent\Builder
     *
     * @return Illuminate\Database\Eloquent\Builder
     */
    public function scopeChildren($query)
    {
        return $query->whereNotNull('parent_id');
    }

    /**
     * define HasMany relationship.
     *
     * @return HasMany
     */
    public function products()
    {
        return $this->hasMany(Product::class);
    }

    public function isSizeable()
    {
        return null !== $this->size_type;
    }
}
