<?php

namespace App\Store\Product\Photo;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Storage;

class Photo extends Model
{
    /**
     * Don't protect against mass assignment.
     *
     * @var array
     */
    protected $guarded = [];

    /**
     * Define relationship for App\Product.
     *
     * @return BelongsTo
     */
    public function product()
    {
        return $this->belongsTo(Product::class);
    }

    public function path()
    {
        return Storage::disk($this->disk)->url($this->path);
    }
}
