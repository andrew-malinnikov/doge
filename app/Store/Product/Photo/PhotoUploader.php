<?php

namespace App\Store\Product\Photo;

use App\Store\Product\Product;
use Illuminate\Support\Facades\Storage;
use Intervention\Image\Facades\Image;

class PhotoUploader
{
    protected $image;

    public function handle(Product $product, $file)
    {
        $this->buildImage($file);
        Storage::disk('public')->put($this->path, $this->image->__toString(), 'public');
        $product->photos()->create([
            'disk' => 'public',
            'path' => $this->path,
        ]);
    }

    protected function buildImage($file)
    {
        $this->image = Image::make($file)->encode('jpg');
        $hash = md5($this->image->__toString());
        $this->path = "products/photos/{$hash}.jpg";
    }
}
