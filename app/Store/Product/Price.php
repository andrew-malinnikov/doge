<?php

namespace App\Store\Product;

class Price
{
    protected $price;

    public function __construct($price)
    {
        $this->price = $price;
    }

    public function format()
    {
        return (float) $this->price / 100 .' ₽';
    }

    public function get()
    {
        return $this->price;
    }
}
