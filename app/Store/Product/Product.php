<?php

namespace App\Store\Product;

use App\Store\Color\Color;
use App\Store\Product\Photo\Photo;
use App\Store\StockData;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;

class Product extends Model
{
    /**
     * Don't protect against mass assignment.
     *
     * @var array
     */
    protected $guarded = [];

    public function setSlugAttribute($value)
    {
        $slug = $value
            ?: mb_strtolower(str_replace(' ', '-', $this->attributes['name']));
        $this->attributes['slug'] = $slug;
    }

    public function getFormattedPriceAttribute()
    {
        return (float) ($this->price / 100);
    }

    /**
     * Query constraint.
     *
     * @param  Illuminate\Database\Eloquent\Builder
     *
     * @return Illuminate\Database\Eloquent\Builder
     */
    public function scopeSearch($query, $keyword)
    {
        collect(explode(' ', $keyword))->filter()->each(function ($keyword) use ($query) {
            $keyword = '%'.$keyword.'%';
            $query->where(function ($query) use ($keyword) {
                $query->where('name', 'like', $keyword);
            });
        });
    }

    /**
     * Define relationship for App\Category.
     *
     * @return BelongsTo
     */
    public function category()
    {
        return $this->belongsTo(Category::class);
    }

    /**
     * Add product with specific size into stock.
     */
    public function addToStock(StockData $data)
    {
        $stock = ProductInStock::firstOrNew([
            'product_id' => $this->id,
            'size_id' => optional($data->size)->id,
        ]);

        $stock->fill(['quantity' => $stock->quantity + $data->quantity]);

        return tap($stock)->save();
    }

    public function subtractStock(StockData $data)
    {
        $stock = ProductInStock::where('product_id', $this->id)
            ->size($data->size)
            ->first();

        $newQuantity = $stock->quantity - $data->quantity;

        0 == $newQuantity
            ? $stock->delete() : $stock->update(['quantity' => $newQuantity]);
    }

    /**
     * @return HasMany
     */
    public function stock()
    {
        return $this->hasMany(ProductInStock::class, 'product_id');
    }

    /**
     * Define relationship for Color.
     *
     * @return BelongsTo
     */
    public function color()
    {
        return $this->belongsTo(Color::class);
    }

    /**
     * define HasMany relationship.
     *
     * @return HasMany
     */
    public function photos()
    {
        return $this->hasMany(Photo::class);
    }

    public function masterPhotoPath()
    {
        return optional($this->photos->first())->path() ?? 'https://picsum.photos/500?grayscale';
    }

    /**
     * Query constraint.
     *
     * @param  Illuminate\Database\Eloquent\Builder
     *
     * @return Illuminate\Database\Eloquent\Builder
     */
    public function scopePublished($query)
    {
        return $query->whereNotNull('published_at');
    }

    /**
     * Is product published.
     *
     * @return bool
     */
    public function isPublished()
    {
        return null !== $this->published_at;
    }

    /**
     * Publish the product.
     *
     * @return self
     */
    public function publish()
    {
        return tap($this)->update(['published_at' => now()]);
    }

    /**
     * Unpublish the product.
     *
     * @return self
     */
    public function unpublish()
    {
        return tap($this)->update(['published_at' => null]);
    }

    /**
     * Is product out of stock completely.
     *
     * @return bool
     */
    public function isOutOfStock()
    {
        return 0 == $this->stock()->count();
    }
}
