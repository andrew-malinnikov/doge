<?php

namespace App\Store\Product;

use Illuminate\Database\Eloquent\Model;

class ProductInStock extends Model
{
    protected $table = 'product_in_stock';

    /**
     * Don't protect against mass assignment.
     *
     * @var array
     */
    protected $guarded = [];

    /**
     * Query constraint.
     *
     * @return Illuminate\Database\Eloquent\Builder
     */
    public function scopeSize($query, $size)
    {
        return $query->where(function ($query) use ($size) {
            null === $size
                ? $query->whereNull($size)
                : $query->where('size_id', $size->id);
        });
    }

    /**
     * Define relationship for Product.
     *
     * @return BelongsTo
     */
    public function product()
    {
        return $this->belongsTo(Product::class);
    }

    /**
     * Define relationship for App\Size.
     *
     * @return BelongsTo
     */
    public function size()
    {
        return $this->belongsTo(Size::class);
    }
}
