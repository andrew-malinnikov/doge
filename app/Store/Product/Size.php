<?php

namespace App\Store\Product;

use Illuminate\Database\Eloquent\Model;

class Size extends Model
{
    public const TYPE_DIGIT = 'digit';
    public const TYPE_LETTER = 'letter';

    public static function getAllTypes()
    {
        return [
            self::TYPE_DIGIT => 'Линейный (35,39,41...)',
            self::TYPE_LETTER => 'Буквенный (M,L,XL)',
        ];
    }

    /**
     * Don't protect against mass assignment.
     *
     * @var array
     */
    protected $guarded = [];
}
