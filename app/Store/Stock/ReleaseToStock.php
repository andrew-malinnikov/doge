<?php

namespace App\Store\Stock;

use App\Sales\Order\Order;
use App\Store\StockData;

class ReleaseToStock
{
    public function handle($event)
    {
        foreach (Order::find($event->orderId)->items as $orderItem) {
            $orderItem->product->addToStock(new StockData([
                'quantity' => $orderItem->quantity,
                'size' => $orderItem->size,
            ]));
        }
    }
}
