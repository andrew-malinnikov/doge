<?php

namespace App\Store\Stock;

use App\Sales\Order\Order;
use App\Store\StockData;

class Restock
{
    public function handle($event)
    {
        foreach (Order::find($event->orderId)->items as $orderItem) {
            $orderItem->product->subtractStock(new StockData([
                'quantity' => $orderItem->quantity,
                'size' => $orderItem->size,
            ]));
        }
    }
}
