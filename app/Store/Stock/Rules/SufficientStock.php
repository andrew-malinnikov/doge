<?php

namespace App\Store\Stock\Rules;

use App\Store\Cart\CartRepository;
use Illuminate\Contracts\Validation\Rule;

class SufficientStock implements Rule
{
    /**
     * Determine if the validation rule passes.
     *
     * @param string $attribute
     * @param mixed  $value
     *
     * @return bool
     */
    public function passes($attribute, $value)
    {
        return resolve(CartRepository::class)
            ->resolveFromSession()
            ->getItems()
            ->isSufficientStock();
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return 'Товары не в наличии.';
    }
}
