<?php

namespace App\Store;

use App\Store\Product\Size;
use Spatie\DataTransferObject\DataTransferObject;

class StockData extends DataTransferObject
{
    public int $quantity;
    public ?Size $size;
}
