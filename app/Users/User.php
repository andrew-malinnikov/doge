<?php

namespace App\Users;

use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Arr;

class User extends Authenticatable
{
    use Notifiable;
    protected $guarded = [];

    public function isAdmin()
    {
        return in_array($this->email, Arr::wrap(config('doge.admins')));
    }
}
