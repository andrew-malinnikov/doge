<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */
use App\Store\Product\Category;
use App\Store\Product\Size;
use Faker\Generator as Faker;

$factory->define(Category::class, function (Faker $faker) {
    return [
        'name' => $name = $faker->word,
        'slug' => $name,
        'size_type' => $faker->randomElement([Size::TYPE_LETTER, Size::TYPE_DIGIT]),
    ];
});
