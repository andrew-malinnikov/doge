<?php

use App\Store\Color\Color;
use Faker\Generator as Faker;

$factory->define(Color::class, function (Faker $faker) {
    return [
        'id' => $faker->unique()->hexColor,
        'slug' => $slug = $faker->word,
        'label' => $slug,
    ];
});
