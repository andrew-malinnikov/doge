<?php

use App\Sales\Order\Number\OrderNumber;
use App\Sales\Order\Order;
use App\Sales\Order\Status\OrderStatusManager;
use Faker\Generator as Faker;
use Illuminate\Support\Str;

$factory->define(Order::class, function (Faker $faker) {
    return [
        'number' => OrderNumber::generate(),
        'status' => $status = $faker->randomElement(array_keys(OrderStatusManager::AVAILABLE_STATUSES)),
        'email' => $faker->safeEmail,
        'phone' => $faker->phoneNumber,
        'shipping_name' => $faker->name,
        'shipping_address_line_1' => $faker->streetName,
        'shipping_address_city' => $faker->city,
        'shipping_address_country' => $faker->country,
        'shipping_address_zip_code' => $faker->unique()->bothify('#####'),
        'items_cost' => 45000,
        'shipping_cost' => 10000,
        'total_cost' => 55000,
        'tracking_number' => OrderStatusManager::SHIPPED == $status ? Str::random() : null,
    ];
});
