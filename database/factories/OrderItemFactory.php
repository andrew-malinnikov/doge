<?php

use App\Sales\Order\Order;
use App\Sales\Order\OrderItem;
use App\Store\Product\Product;
use Faker\Generator as Faker;

$factory->define(OrderItem::class, function (Faker $faker) {
    return [
        'order_id' => factory(Order::class),
        'product_id' => factory(Product::class),
        'quantity' => 1,
        'name' => $faker->word,
        'photo' => 'https://picsum.photos/500?grayscale',
    ];
});
