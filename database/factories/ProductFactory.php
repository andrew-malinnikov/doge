<?php

use App\Store\Product\Category;
use App\Store\Product\Product;
use Faker\Generator as Faker;

$factory->define(Product::class, function (Faker $faker) {
    return [
        'category_id' => factory(Category::class),
        'name' => $faker->randomElement(['Носки 1', 'Лопата', 'Нож']),
        'slug' => $faker->slug,
        'price' => $faker->randomNumber(),
        'color_id' => null,
        'weight' => $faker->randomNumber(),
        'published_at' => now(),
    ];
});
