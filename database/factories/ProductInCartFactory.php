<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */
use App\Store\Cart\Cart;
use App\Store\Cart\ProductInCart;
use App\Store\Product\Product;
use App\Store\Product\Size;
use Faker\Generator as Faker;

$factory->define(ProductInCart::class, function (Faker $faker) {
    return [
        'cart_id' => factory(Cart::class),
        'product_id' => factory(Product::class),
        'quantity' => $faker->randomNumber,
        'size_id' => factory(Size::class),
    ];
});
