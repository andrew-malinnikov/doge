<?php

use App\Store\Product\Product;
use App\Store\Product\ProductInStock;
use App\Store\Product\Size;
use Faker\Generator as Faker;

$factory->define(ProductInStock::class, function (Faker $faker) {
    return [
        'product_id' => factory(Product::class),
        'size_id' => factory(Size::class),
        'quantity' => $faker->randomElement(range(1, 10)),
    ];
});
