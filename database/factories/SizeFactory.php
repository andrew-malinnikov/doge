<?php

use App\Store\Product\Size;
use Faker\Generator as Faker;

$factory->define(Size::class, function (Faker $faker) {
    return [
        'type' => $faker->randomElement([Size::TYPE_LETTER, Size::TYPE_DIGIT]),
        'value' => 'XXL',
    ];
});
