<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('orders', function (Blueprint $table) {
            $table->id();
            $table->string('status');
            $table->string('number')->unique();
            $table->integer('items_cost');
            $table->integer('shipping_cost');
            $table->integer('total_cost');
            $table->string('email');
            $table->string('phone')->nullable();
            $table->string('shipping_name');
            $table->string('shipping_address_line_1');
            $table->string('shipping_address_city');
            $table->string('shipping_address_country');
            $table->string('shipping_address_zip_code');
            $table->text('shipping_delivery_notes')->nullable();
            $table->string('tracking_number')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('orders');
    }
}
