<?php

use App\Store\Product\Category;
use App\Store\Product\Size;
use Illuminate\Database\Seeder;

class CategorySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $human = factory(Category::class)->create([
            'parent_id' => null,
            'description' => null,
            'name' => 'Человеческое',
            'slug' => 'humans',
            'size_type' => null,
        ]);
        $doge = factory(Category::class)->create([
            'parent_id' => null,
            'description' => null,
            'name' => 'Пёсельное',
            'slug' => 'doges',
            'size_type' => null,
        ]);
        factory(Category::class)->create([
            'parent_id' => $human->id,
            'description' => 'Все носки изготовлены из вычесов российских овец и связаны на фабрике в г. Рассказово',
            'name' => 'Носки',
            'slug' => 'socks',
            'size_type' => Size::TYPE_DIGIT,
        ]);
        factory(Category::class)->create([
            'parent_id' => $human->id,
            'name' => 'Футболки',
            'slug' => 't-shirts',
            'size_type' => Size::TYPE_LETTER,
        ]);
        factory(Category::class)->create([
            'parent_id' => $human->id,
            'name' => 'Худи',
            'slug' => 'hoodies',
            'size_type' => Size::TYPE_LETTER,
        ]);
        factory(Category::class)->create([
            'parent_id' => $human->id,
            'name' => 'Шопперы',
            'slug' => 'shoppers',
            'size_type' => null,
        ]);
        factory(Category::class)->create([
            'parent_id' => $doge->id,
            'name' => 'Адресники',
            'slug' => 'address-badges',
            'size_type' => null,
        ]);
        factory(Category::class)->create([
            'parent_id' => $doge->id,
            'name' => 'Шнурки',
            'slug' => 'laces',
            'size_type' => null,
        ]);
    }
}
