<?php

use App\Store\Color\Color;
use Illuminate\Database\Seeder;

class ColorSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(Color::class)->create([
            'id' => '#1a202c',
            'slug' => 'black',
            'label' => 'Чёрный',
        ]);
        factory(Color::class)->create([
            'id' => '#ffffff',
            'slug' => 'white',
            'label' => 'Белый',
        ]);
        factory(Color::class)->create([
            'id' => '#ecb96c',
            'slug' => 'beige',
            'label' => 'Бежевый',
        ]);
        factory(Color::class)->create([
            'id' => '#a0aec0',
            'slug' => 'metallic',
            'label' => 'Стальной',
        ]);
        factory(Color::class)->create([
            'id' => '#ecc94b',
            'slug' => 'golden',
            'label' => 'Золотистый',
        ]);
        factory(Color::class)->create([
            'id' => '#e53e3e',
            'slug' => 'red',
            'label' => 'Красный',
        ]);
        factory(Color::class)->create([
            'id' => '#ed8936',
            'slug' => 'orange',
            'label' => 'Оранжевый',
        ]);
        factory(Color::class)->create([
            'id' => '#faf089',
            'slug' => 'yellow',
            'label' => 'Жёлтый',
        ]);
        factory(Color::class)->create([
            'id' => '#7acab0',
            'slug' => 'olive',
            'label' => 'Оливковый',
        ]);
        factory(Color::class)->create([
            'id' => '#cf88ec',
            'slug' => 'red-purple',
            'label' => 'Красно-фиолетовый',
        ]);
        factory(Color::class)->create([
            'id' => '#fed7e2',
            'slug' => 'pink-white',
            'label' => 'Розово-белый',
        ]);
        factory(Color::class)->create([
            'id' => '#38b2ac',
            'slug' => 'mint',
            'label' => 'Мятный',
        ]);
        factory(Color::class)->create([
            'id' => '#9cc7ae',
            'slug' => 'gray-green',
            'label' => 'Серо-зелёный',
        ]);
        factory(Color::class)->create([
            'id' => '#434190',
            'slug' => 'bilberry',
            'label' => 'Черничный',
        ]);
        factory(Color::class)->create([
            'id' => '#975a16',
            'slug' => 'brown',
            'label' => 'Коричневый',
        ]);
    }
}
