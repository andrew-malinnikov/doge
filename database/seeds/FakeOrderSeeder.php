<?php

use App\Sales\Order\Order;
use App\Sales\Order\OrderItem;
use App\Store\Product\Product;
use Illuminate\Database\Seeder;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\DB;

class FakeOrderSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS=0;');
        DB::table('orders')->truncate();
        DB::table('order_items')->truncate();
        DB::statement('SET FOREIGN_KEY_CHECKS=1;');

        factory(Order::class, 100)->create()->each(function ($order) {
            foreach (range(1, Arr::random(range(1, 5))) as $i) {
                $product = Product::inRandomOrder()->first();
                factory(OrderItem::class)->create([
                    'order_id' => $order->id,
                    'product_id' => $product->id,
                    'name' => $product->name,
                    'quantity' => Arr::random(range(1, 8)),
                    'size_label' => Arr::random(['XL', 'S', 'SM', 'XXL', '40-45']),
                ]);
            }
        });
    }
}
