<?php

use App\Store\Color\Color;
use App\Store\Product\Category;
use App\Store\Product\Product;
use App\Store\Product\Size;
use Illuminate\Database\Seeder;

class ProductSeeder extends Seeder
{
    public $categories;
    public $sizes;
    public $colors;

    public function __construct()
    {
        $this->categories = Category::all();
        $this->sizes = Size::all();
        $this->colors = Color::all();
    }

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->seedSocks();
        $this->seedShoppers();
        $this->seedAddressBadges();
    }

    public function seedAddressBadges()
    {
        factory(Product::class)->create([
            'category_id' => $this->addressBadges(),
            'name' => 'Адресник Косточка Стальной',
            'color_id' => $this->metallic(),
            'price' => 600 * 100,
            'weight' => 50,
        ]);
        factory(Product::class)->create([
            'category_id' => $this->addressBadges(),
            'name' => 'Адресник Косточка Золотистый',
            'color_id' => $this->golden(),
            'price' => 600 * 100,
            'weight' => 50,
        ]);
        factory(Product::class)->create([
            'category_id' => $this->addressBadges(),
            'name' => 'Адресник Круглый Стальной',
            'color_id' => $this->metallic(),
            'price' => 600 * 100,
            'weight' => 50,
        ]);
        factory(Product::class)->create([
            'category_id' => $this->addressBadges(),
            'name' => 'Адресник Круглый Золотистый',
            'color_id' => $this->golden(),
            'price' => 600 * 100,
            'weight' => 50,
        ]);
    }

    public function seedShoppers()
    {
        factory(Product::class)->create([
            'category_id' => $this->shoppers(),
            'name' => 'Шоппер Объятия',
            'color_id' => $this->beige(),
            'price' => 600 * 100,
            'weight' => 150,
        ]);
    }

    public function seedSocks()
    {
        factory(Product::class)->create([
            'category_id' => $this->socks(),
            'name' => 'Носки Маяк',
            'price' => 600 * 100,
            'weight' => 150,
        ]);
        factory(Product::class)->create([
            'category_id' => $this->socks(),
            'name' => 'Носки Горы',
            'price' => 600 * 100,
            'weight' => 150,
        ]);
        factory(Product::class)->create([
            'category_id' => $this->socks(),
            'name' => 'Носки Изнанка',
            'price' => 600 * 100,
            'weight' => 150,
        ]);
        factory(Product::class)->create([
            'category_id' => $this->socks(),
            'name' => 'Носки Рязань',
            'price' => 600 * 100,
            'weight' => 150,
        ]);
    }

    public function socks()
    {
        return $this->categories->first(function ($item) {
            return 'socks' == $item->slug;
        })->id;
    }

    public function shirts()
    {
        return $this->categories->first(fn ($category) => 't-shirts' == $category->slug)->id;
    }

    public function hoodies()
    {
        return $this->categories->first(fn ($category) => 'hoodies' == $category->slug)->id;
    }

    public function shoppers()
    {
        return $this->categories->first(fn ($category) => 'shoppers' == $category->slug)->id;
    }

    public function addressBadges()
    {
        return $this->categories->first(fn ($category) => 'address-badges' == $category->slug)->id;
    }

    public function laces()
    {
        return $this->categories->first(fn ($category) => 'laces' == $category->slug)->id;
    }

    public function beige()
    {
        return $this->colors->first(fn ($color) => 'beige' == $color->slug)->id;
    }

    public function black()
    {
        return $this->colors->first(fn ($color) => 'black' == $color->slug)->id;
    }

    public function mint()
    {
        return $this->colors->first(fn ($color) => 'mint' == $color->slug)->id;
    }

    public function bilberry()
    {
        return $this->colors->first(fn ($color) => 'bilberry' == $color->slug)->id;
    }

    public function odio()
    {
        return $this->colors->first(fn ($color) => 'odio' == $color->slug)->id;
    }

    public function olive()
    {
        return $this->colors->first(fn ($color) => 'olive' == $color->slug)->id;
    }

    public function brown()
    {
        return $this->colors->first(fn ($color) => 'brown' == $color->slug)->id;
    }

    public function grayGreen()
    {
        return $this->colors->first(fn ($color) => 'gray-green' == $color->slug)->id;
    }

    public function metallic()
    {
        return $this->colors->first(fn ($color) => 'metallic' == $color->slug)->id;
    }

    public function redPurple()
    {
        return $this->colors->first(fn ($color) => 'red-purple' == $color->slug)->id;
    }

    public function red()
    {
        return $this->colors->first(fn ($color) => 'red' == $color->slug)->id;
    }

    public function golden()
    {
        return $this->colors->first(fn ($color) => 'golden' == $color->slug)->id;
    }

    public function orange()
    {
        return $this->colors->first(fn ($color) => 'orange' == $color->slug)->id;
    }

    public function yellow()
    {
        return $this->colors->first(fn ($color) => 'yellow' == $color->slug)->id;
    }

    public function white()
    {
        return $this->colors->first(fn ($color) => 'white' == $color->slug)->id;
    }

    public function pinkWhite()
    {
        return $this->colors->first(fn ($color) => 'pink-white' == $color->slug)->id;
    }
}
