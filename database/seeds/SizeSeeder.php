<?php

use App\Store\Product\Size;
use Illuminate\Database\Seeder;

class SizeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(Size::class)->create([
            'type' => Size::TYPE_DIGIT,
            'value' => '34-36',
        ]);
        factory(Size::class)->create([
            'type' => Size::TYPE_DIGIT,
            'value' => '37-39',
        ]);
        factory(Size::class)->create([
            'type' => Size::TYPE_DIGIT,
            'value' => '40-45',
        ]);
        factory(Size::class)->create([
            'type' => Size::TYPE_LETTER,
            'value' => 'XS',
        ]);
        factory(Size::class)->create([
            'type' => Size::TYPE_LETTER,
            'value' => 'S',
        ]);
        factory(Size::class)->create([
            'type' => Size::TYPE_LETTER,
            'value' => 'M',
        ]);
        factory(Size::class)->create([
            'type' => Size::TYPE_LETTER,
            'value' => 'L',
        ]);
        factory(Size::class)->create([
            'type' => Size::TYPE_LETTER,
            'value' => 'XL',
        ]);
        factory(Size::class)->create([
            'type' => Size::TYPE_LETTER,
            'value' => 'XXL',
        ]);
    }
}
