<?php

use App\Store\Product\Product;
use App\Store\Product\ProductInStock;
use Illuminate\Database\Seeder;

class TestProductInCartAndInStockSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->call(TestProductInCartSeeder::class);

        $product = Product::first();

        factory(ProductInStock::class)->create([
            'product_id' => $product->id,
            'quantity' => 3,
            'size_id' => null,
        ]);
    }
}
