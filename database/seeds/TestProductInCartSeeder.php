<?php

use App\Store\Cart\CartRepository;
use App\Store\Cart\ProductInCart;
use App\Store\Product\Product;
use Illuminate\Database\Seeder;

class TestProductInCartSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $cart = resolve(CartRepository::class)->resolve();
        $product = factory(Product::class)->create();
        factory(ProductInCart::class)->create([
            'cart_id' => $cart->id,
            'product_id' => $product->id,
            'quantity' => 2,
            'size_id' => null,
        ]);
    }
}
