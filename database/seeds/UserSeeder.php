<?php

use App\Users\User;
use Illuminate\Database\Seeder;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(User::class)->create([
            'name' => 'andrew',
            'email' => 'andrew@gmail.com',
            'password' => bcrypt('andrew'),
        ]);
    }
}
