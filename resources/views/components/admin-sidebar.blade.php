<aside>
  <nav class="space-y-2">
    <div>
      <a class="px-4 py-1 block mr-4 rounded hover:bg-gray-300 text-lg" href="{{route('admin.products.index') }}">Продукция</a>
    </div>
    <div>
      <a class="px-4 py-1 block mr-4 rounded hover:bg-gray-300 text-lg" href="{{route('admin.orders.index') }}">Заказы</a>
    </div>
  </nav>
</aside>
