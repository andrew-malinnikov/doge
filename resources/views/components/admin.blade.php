<x-master>
  <div class="text-gray-900 bg-gray-200 h-full min-h-screen antialiased">
    <header class="container mx-auto py-6 px-4 flex items-center lg:px-0">
      <div class="container mx-auto">
        <a href="{{ route('admin.dashboard') }}"><img class="h-8 md:h-auto" src="/images/logo.png" alt="Собакен"></a>
      </div>
      <div>
        <div>
          <nav>
          </nav>
        </div>
        <div>
          <div class="flex">
            <span class="flex items-center space-x-1 text-xl">
              <span>магазикен</span>
              <svg class="w-6 h-6 stroke-current text-indigo-600" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"><polyline points="13 17 18 12 13 7"></polyline><polyline points="6 17 11 12 6 7"></polyline></svg>
            </span>
            <a href="{{route('store') }}"><img src="/images/doge_hugs.svg" class="h-12"></a>
          </div>
        </div>
      </div>
    </header>
    <section class="container mx-auto">
      <div class="sm:flex">
        <div class="hidden sm:block w-1/6">
          <x-admin-sidebar />
        </div>
        <main class="flex-1">
          @yield ('content')
        </main>
      </div>
    </section>
  </div>
</x-master>
