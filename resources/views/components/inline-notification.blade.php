  <span
    x-data="{ isShown: false }"
    x-init="
      @this.on('inlineFlashMessage', (destination) => {
        if (destination == $el.getAttribute('data-destination')) {
          isShown = true
          setTimeout(() => isShown = false, 2500)
        }
      })
    "
    x-show.transition.out.duration.1000ms="isShown"
    style="display: none;"
    class="text-green-600"
    data-destination="{{ $destination }}"
  >
  {{ $notification ?? 'Saved!' }}
  </span>
