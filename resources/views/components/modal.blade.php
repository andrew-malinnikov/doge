<div x-data="{ isOpen: false }" x-init="
    @this.on('showModal', (name) => {
      const modalName = $refs.modalContent.getAttribute('data-name')
      if (modalName == name) {
        isOpen = true
      }
    })
    @this.on('hideModal', (name) => {
      const modalName = $refs.modalContent.getAttribute('data-name')
      if (modalName == name) {
        isOpen = false
      }
    })
  " x-show="isOpen" style="display: none;" class="fixed bottom-0 inset-x-0 px-4 pb-4 sm:inset-0 sm:flex sm:items-center sm:justify-center">
  <div class="fixed inset-0 transition-opacity">
    <div class="absolute inset-0 bg-gray-500 opacity-75"></div>
  </div>
  <div @click.away="isOpen = false" class="bg-white rounded-lg overflow-hidden shadow-xl transform transition-all sm:max-w-lg sm:w-full">
    {{ $slot }}
  </div>
</div>
