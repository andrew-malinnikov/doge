<x-modal>
<div x-ref="modalContent" data-name="cancel">
  <div class="bg-white px-4 pt-5 pb-4 sm:p-6 sm:pb-4">
    <div>
      <div class="text-center sm:mt-0 sm:text-left">
        <h3 class="text-lg leading-6 font-medium text-gray-900">Отменить заказ</h3>
      </div>

      <div class="mt-6 flex space-x-4">
        <p class="text-gray-600">Заказ будет отменен, товары вернутся на остатки. </p>
      </div>
    </div>
  </div>
  <div class="bg-gray-50 px-4 py-3 sm:px-6 sm:flex sm:flex-row-reverse">
    <span class="flex w-full rounded-full shadow-sm sm:ml-3 sm:w-auto">
      <button wire:click="handle" type="button" class="inline-flex justify-center w-full rounded-full border border-transparent px-4 py-2 bg-red-600 text-base leading-6 font-medium text-white shadow-sm hover:bg-red-500 focus:outline-none focus:border-red-700 focus:shadow-outline-red transition ease-in-out duration-150 sm:text-sm sm:leading-5">
        Подтвердить
      </button>
    </span>
    <span class="mt-3 flex w-full rounded-full shadow-sm sm:mt-0 sm:w-auto">
      <button
        @click.prevent="isOpen = false"
        type="button"
        class="inline-flex justify-center w-full rounded-full border border-gray-300 px-4 py-2 bg-white text-base leading-6 font-medium text-gray-700 shadow-sm hover:text-gray-500 focus:outline-none focus:border-blue-300 focus:shadow-outline transition ease-in-out duration-150 sm:text-sm sm:leading-5">
        Cancel
      </button>
    </span>
  </div>
</div>
</x-modal>
