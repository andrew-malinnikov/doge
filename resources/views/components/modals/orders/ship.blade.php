<x-modal>
<div x-ref="modalContent" data-name="ship">
  <div class="bg-white px-4 pt-5 pb-4 sm:p-6 sm:pb-4">
    <div>
      <div class="text-center sm:mt-0 sm:text-left">
        <h3 class="text-xl leading-6 font-medium text-gray-900">Заказ отправлен</h3>
      </div>

      <div class="mt-6">
        <div>
          <p class="text-gray-600">Отправим уведомление получателю с его трэкингом.</p>
        </div>
        <div class="mt-4">
          <label for="tracking-number">Трэкинг-номер</label>
          <div>
            <input wire:model="trackingNumber" type="text" class="form-input w-full" id="tracking-number">
            <x-inline-validation-message field="trackingNumber" />
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="bg-gray-50 px-4 py-3 sm:px-6 sm:flex sm:flex-row-reverse">
    <span class="flex w-full rounded-full shadow-sm sm:ml-3 sm:w-auto">
      <button wire:click="handle" type="button" class="inline-flex justify-center w-full rounded-full border border-transparent px-4 py-2 bg-indigo-600 text-base leading-6 font-medium text-white shadow-sm hover:bg-indigo-500 focus:outline-none focus:border-indigo-700 focus:shadow-outline-indigo transition ease-in-out duration-150 sm:text-sm sm:leading-5">
        Подтвердить
      </button>
    </span>
    <span class="mt-3 flex w-full rounded-full shadow-sm sm:mt-0 sm:w-auto">
      <button
        @click.prevent="isOpen = false"
        type="button"
        class="inline-flex justify-center w-full rounded-full border border-gray-300 px-4 py-2 bg-white text-base leading-6 font-medium text-gray-700 shadow-sm hover:text-gray-500 focus:outline-none focus:border-blue-300 focus:shadow-outline transition ease-in-out duration-150 sm:text-sm sm:leading-5">
        Отмена
      </button>
    </span>
  </div>
</div>
</x-modal>
