@props([
  'route',
])

@php
$isActivePage = request()->route()->getName() == $route;

$classes = ['text-sm', 'no-underline', 'transition', 'duration-300', 'hover:underline', 'hover:text-indigo-600'];
if ($isActivePage) {
  array_push($classes, 'px-2 py-1 rounded bg-indigo-100 no-underline font-semibold text-indigo-600');
}
$classesString = implode(' ', $classes);
@endphp
<li>
  <a
    :class="'{{$classesString}}'"
    href="{{ $route !== '#' ? route($route) : '#' }}"
  >
    {{$slot}}
  </a>
  </li>
