<span class="px-1 rounded bg-{{$order->status()->color()}}-200 text-{{$order->status()->color()}}-600 text-xs uppercase tracking-wide font-semibold">
  {{ $order->status()->label() }}
</span>
