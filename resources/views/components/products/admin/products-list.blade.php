<div>
  <div class="flex flex-col">
    <div class="-my-2 py-2 overflow-x-auto sm:-mx-6 sm:px-6 lg:-mx-8 lg:px-8">
      <div class="align-middle inline-block min-w-full shadow overflow-hidden sm:rounded-lg border-b border-gray-200">
        <table class="min-w-full">
          <thead>
            <tr>
              <th class="px-6 py-3 border-b border-gray-200 bg-gray-100 text-left text-xs leading-4 font-medium text-gray-500 uppercase tracking-wider">
                Name
              </th>
              <th class="px-6 py-3 border-b border-gray-200 bg-gray-100 text-left text-xs leading-4 font-medium text-gray-500 uppercase tracking-wider">
                Price
              </th>
              <th class="hidden md:table-cell px-6 py-3 border-b border-gray-200 bg-gray-100 text-left text-xs leading-4 font-medium text-gray-500 uppercase tracking-wider">
                Color
              </th>
              <th class="hidden md:table-cell px-6 py-3 border-b border-gray-200 bg-gray-100 text-left text-xs leading-4 font-medium text-gray-500 uppercase tracking-wider">
                Weight
              </th>
              <th class="hidden md:table-cell px-6 py-3 border-b border-gray-200 bg-gray-100"></th>
            </tr>
          </thead>
          <tbody class="bg-white">
            @foreach ($products as $product)
              <tr>
                <td class="px-6 py-4 whitespace-no-wrap border-b border-gray-200">
                  <div class="flex items-top justify-between">
                    <div class="">
                      <div class="text-xs uppercase font-bold leading-5 text-gray-500">{{ $product->category->name }}</div>
                      <div class="text-base leading-5 font-medium text-indigo-700">
                        <a class="underline" href="{{ route('admin.products.show', ['product' => $product]) }}">{{ $product->name }}</a>
                      </div>
                    </div>
                    <div>
                      @if ($product->isPublished()) <span class="text-xs px-2 py-1 rounded-full bg-green-100 text-green-800 uppercase font-semibold">published</span>
                      @else <span class="text-xs px-2 py-1 rounded-full bg-red-100 text-red-800 uppercase font-semibold">unpublished</span>
                      @endif
                    </div>
                  </div>
                </td>
                <td class="px-6 py-4 whitespace-no-wrap border-b border-gray-200">
                  <div class="text-sm leading-5 text-gray-900">{{ $product->formatted_price }} ₽</div>
                </td>
                <td class="hidden md:table-cell px-6 py-4 whitespace-no-wrap border-b border-gray-200">
                  <span class="w-4 h-4 px-2 inline-flex text-xs leading-5 font-semibold rounded-full" style="background-color: {{$product->color_id }}"></span>
                </td>
                <td class="hidden md:table-cell px-6 py-4 whitespace-no-wrap border-b border-gray-200 text-sm leading-5 text-gray-500">
                  {{ $product->weight }} г.
                </td>
                <td class="hidden md:table-cell px-6 py-4 whitespace-no-wrap text-right border-b border-gray-200 text-sm leading-5 font-medium">
                  <a href="{{ route('admin.products.show', ['product' => $product]) }}" class="text-indigo-600 hover:text-indigo-900">Edit</a>
                </td>
              </tr>
            @endforeach
          </tbody>
        </table>
      </div>
    </div>
  </div>
</div>
