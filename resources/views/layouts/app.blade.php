@extends('layouts.master')

@section ('body')
  {{-- Header --}}
  @include ('partials.app.header')

  {{-- Main content --}}
  <div class="bg-gray-100">
    <div class="max-w-6xl mx-auto">
      @yield('content')
    </div>
  </div>
@stop
