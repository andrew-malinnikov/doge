<!DOCTYPE html>
  <html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

    <head>
      <meta charset="utf-8">

      {{-- CSRF Token --}}
      <meta name="csrf-token" content="{{ csrf_token() }}">

      <meta name="viewport" content="width=device-width, initial-scale=1">

      {{-- Page title --}}
      <title>@yield('page_title', 'Doge')</title>

      {{-- Styles --}}
      <link href="https://fonts.googleapis.com/css2?family=Montserrat:wght@400;600;800;900&display=swap" rel="stylesheet">
      <link rel="stylesheet" type="text/css" href="{{ mix('css/app.css') }}">
      @livewireStyles

      {{-- Alpine.js --}}
      <script src="https://cdn.jsdelivr.net/gh/alpinejs/alpine@v2.x.x/dist/alpine.min.js" defer></script>
    </head>

    <body class="antialiased font-sans text-gray-900">
      <div id="app">
        {{-- Do your content here --}}
        @yield('body')
      </div>

      {{-- Shove modals at the bottom --}}
      @stack('modals')

      {{-- Scripts --}}
      <script src="{{ mix('js/app.js') }}"></script>
      @livewireScripts
      @stack('scripts')
    </body>

</html>
