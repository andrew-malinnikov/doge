<div class="px-4">
  <div class="bg-white rounded-md shadow-md">
    <header class="py-4 px-4 border-b-2 border-gray-100">
      <h1 class="text-lg font-semibold">Новая Категория</h1>
    </header>
    <div class="px-4 mt-4">
      <div>
        <label>Название</label>
      </div>
      <div>
        <input wire:model="name" type="text" class="form-input w-full" placeholder="Носки">
      </div>
    </div>
    <div class="px-4 mt-4">
      <div>
        <label>Тип размера</label>
      </div>
      <div>
        <select wire:model="sizeType" class="form-select w-full">
            @foreach ($sizeTypes as $slug => $type)
              <option value="{{$slug}}">{{ $type }}</option>
            @endforeach
        </select>
      </div>
    </div>
    <div class="p-4 mt-4 bg-gray-100 text-right">
      <button type="button" wire:click="handle" class="bg-indigo-600 px-5 py-1 rounded-full text-white shadow-md hover:bg-indigo-800 transition duration-300">Создать</button>
    </div>
  </div>
</div>
