<div>
  <h1 class="bg-white px-4 py-3 text-xl font-semibold">Заказы</h1>
  <div class="px-5 mt-8">
    <h2 class="text-gray-500 uppercase font-semibold">Фильтры</h2>
    <div class="mt-4">
      <div>Статус</div>
      <div>
        <select wire:model="status" class="form-select w-full">
          @foreach ($statuses as $status => $label)
          <option value="{{ $status }}">{{ $label }}</option>
          @endforeach
          <option value="{{ null }}">Любой</option>
        </select>
      </div>
    </div>
  </div>
  <div class="bg-white mt-10">
    <div class="flex flex-col">
      <div class="-my-2 py-2 overflow-x-auto sm:-mx-6 sm:px-6 lg:-mx-8 lg:px-8">
        <div class="align-middle inline-block min-w-full shadow overflow-hidden sm:rounded-lg border-b border-gray-200">
          <table class="min-w-full">
            <thead>
              <tr>
                <th class="px-6 py-3 border-b border-gray-200 bg-gray-50 text-left text-xs leading-4 font-medium text-gray-500 uppercase tracking-wider">
                  Заказ
                </th>
                <th class="px-6 py-3 border-b border-gray-200 bg-gray-50 text-left text-xs leading-4 font-medium text-gray-500 uppercase tracking-wider">
                  Статус
                </th>
                <th class="hidden px-6 py-3 border-b border-gray-200 bg-gray-50"></th>
              </tr>
            </thead>
            <tbody class="bg-white">
              @foreach ($orders as $order)
              <tr>
                <td class="px-6 py-4 whitespace-no-wrap border-b border-gray-200">
                  <div class="flex items-center">
                    <div>
                      <div class="leading-5 font-medium text-gray-900">
                        <a class="underline text-indigo-800" href="{{ route('admin.orders.show', $order) }}">{{ $order->email }}</a>
                      </div>
                      <div class="text-base mt-1 font-bold leading-5 text-gray-900">{{ $order->costs()->totalCost->format() }}</div>
                      <div class="text-base mt-1  leading-5 text-gray-600">{{ $order->created_at->format('F d, Y') }}</div>
                    </div>
                  </div>
                </td>
                <td class="px-6 py-4 whitespace-no-wrap border-b border-gray-200">
                  <x-orders.status :order="$order" />
                </td>
              </tr>
              @endforeach
            </tbody>
          </table>
        </div>
      </div>
    </div>
  </div>
  <div>
    {{ $orders->onEachSide(1)->links()}}
  </div>
</div>
