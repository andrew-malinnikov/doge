<div class="px-4 space-y-6">
  <div class="flex items-center flex-row-reverse space-x-4 space-x-reverse">
    @foreach ($order->status()->accessors() as $nextStatus)
      @livewire(
        $nextStatus->livewireComponentName(),
        ['order' => $order],
        key($nextStatus->get())
      )
    @endforeach
  </div>
  <div class="bg-white rounded-lg shadow-md">
    <div class="px-4 py-6 md:px-12 md:py-12">
      <div class="pb-6 flex justify-between border-b border-gray-200">
        <div class="flex flex-col">
          <div class="text-xs uppercase font-semibold text-gray-500 md:text-sm">Заказ для </div>
          <div class="space-y-2">
            <div class="flex items-center space-x-1 text-indigo-600">
              <svg class="stroke-current w-4 h-4 text-gray-500 inline-block" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round">
                <path d="M4 4h16c1.1 0 2 .9 2 2v12c0 1.1-.9 2-2 2H4c-1.1 0-2-.9-2-2V6c0-1.1.9-2 2-2z"></path>
                <polyline points="22,6 12,13 2,6"></polyline>
              </svg>
              <span class="text-lg md:text-xl">{{ $order->email }}</span>
            </div>
            <div>
              @if ($order->phone)
                <div class="flex items-center space-x-1">
                  <svg class="stroke-current w-4 h-4 text-gray-500 inline-block" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round">
                    <polyline points="23 7 23 1 17 1"></polyline>
                    <line x1="16" y1="8" x2="23" y2="1"></line>
                    <path d="M22 16.92v3a2 2 0 0 1-2.18 2 19.79 19.79 0 0 1-8.63-3.07 19.5 19.5 0 0 1-6-6 19.79 19.79 0 0 1-3.07-8.67A2 2 0 0 1 4.11 2h3a2 2 0 0 1 2 1.72 12.84 12.84 0 0 0 .7 2.81 2 2 0 0 1-.45 2.11L8.09 9.91a16 16 0 0 0 6 6l1.27-1.27a2 2 0 0 1 2.11-.45 12.84 12.84 0 0 0 2.81.7A2 2 0 0 1 22 16.92z"></path>
                  </svg>
                  <span class="font-mono text-gray-600">{{ $order->phone }}</span>
                </div>
              @endif
            </div>
            <div class="flex items-center space-x-1">
              <svg class="stroke-current w-4 h-4 text-gray-500 inline-block" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"><rect x="3" y="4" width="18" height="18" rx="2" ry="2"></rect><line x1="16" y1="2" x2="16" y2="6"></line><line x1="8" y1="2" x2="8" y2="6"></line><line x1="3" y1="10" x2="21" y2="10"></line></svg>
              <span class="text-gray-700 text-base md:text-xl">{{ $order->created_at->format('F d, Y') }}</span>
            </div>
          </div>
        </div>
        <div class="flex flex-col items-end space-y-1">
          <x-orders.status :order="$order" />
          <span class="font-bold text-xl">{{ $order->costs()->totalCost->format()}}</span>
        </div>
      </div>
    </div>
    <div class="bg-white px-4 pb-6 md:px-12 md:pb-12">
      <div class="space-y-4 md:space-y-12">
        <div class="md:flex md:space-x-24">
          <div class="flex flex-col">
            <span class="text-xs uppercase font-semibold text-gray-600 md:text-sm md:text-gray-500">Номер заказа</span>
            <span class="font-mono text-gray-700"><code>{{ $order->number}}</code></span>
          </div>
          <div>
            @if ($order->tracking_number)
              <div class="flex flex-col">
                <span class="text-xs uppercase font-semibold text-gray-600 md:text-sm md:text-gray-500">Трэкинг номер</span>
                <span class="font-mono text-gray-700">{{ $order->tracking_number }}</span>
              </div>
            @endif
          </div>
        </div>
        <div class="md:flex md:space-x-6 lg:space-x-24">
          <div class="flex flex-col">
            <span class="text-xs uppercase font-semibold text-gray-600 md:text-sm md:text-gray-500">Получатель</span>
            <span class="md:text-xl">{{ $order->shipping_name}}</span>
          </div>
          <div class="flex flex-col">
            <span class="text-xs uppercase font-semibold text-gray-600 md:text-sm md:text-gray-500">Адрес</span>
            <span class="md:text-xl">{{ $order->shipping_address_line_1 }}</span>
          </div>
          <div class="flex flex-col">
            <span class="text-xs uppercase font-semibold text-gray-600 md:text-sm md:text-gray-500">Город</span>
            <span class="md:text-xl">{{ $order->shipping_address_city }}</span>
          </div>
          <div class="flex flex-col">
            <span class="text-xs uppercase font-semibold text-gray-600 md:text-sm md:text-gray-500">Страна</span>
            <span class="md:text-xl">{{ $order->shipping_address_country }}</span>
          </div>
          <div class="flex flex-col">
            <span class="text-xs uppercase font-semibold text-gray-600 md:text-sm md:text-gray-500">Индекс</span>
            <span class="md:text-xl">{{ $order->shipping_address_zip_code }}</span>
          </div>
        </div>
        <div>
          @if ($order->shipping_delivery_notes)
          <div class="flex flex-col">
            <span class="text-xs uppercase font-semibold text-gray-600 md:text-sm md:text-gray-500">Комментарии к заказу</span>
            <span class="md:text-xl">{{ $order->shipping_delivery_notes }}</span>
          </div>
          @endif
        </div>
      </div>
    </div>
    <div class="px-4 py-6 space-y-4 bg-gray-100 md:px-12 md:py-12 md:space-y-12">
      @foreach ($order->items as $orderItem)
      <div class="flex">
        <div class="w-1/5">
          <img class="rounded shadow border-4 border-white -m-1" src="{{ $orderItem->photo }}">
        </div>
        <div class="ml-2 flex-1">
          <h3><a class="text-indigo-600 hover:underline md:text-xl" href="{{ $orderItem->productPath() }}">{{ $orderItem->name }}</a></h3>
          <span>@if ($orderItem->size_label){{ $orderItem->size_label }} @endif</span>
        </div>
        <div>
          <span class="text-gray-600 text-sm font-semibold md:text-lg">{{ $orderItem->quantity }} шт.</span>
        </div>
      </div>
      @endforeach
    </div>
  </div>
</div>
