<div>
  <button
    wire:click="$emit('showModal', 'activate')"
    class="flex items-center space-x-1 px-3 py-1 text bg-indigo-600 text-white tracking-wide rounded hover:bg-indigo-500 transition duration-300"
  >
    Оплата получена
  </button>

  <x-modals.orders.activate  :order="$order" />
</div>
