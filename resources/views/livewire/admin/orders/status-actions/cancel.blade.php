<div>
  <button wire:click="$emit('showModal', 'cancel')" class="flex items-center space-x-1 px-3 py-1 text text-red-600 bg-white border border-gray-300 tracking-wide rounded hover:bg-red-200 hover:text-red-800 transition duration-300">
    Отменить
  </button>

  <x-modals.orders.cancel :order="$order" />
</div>
