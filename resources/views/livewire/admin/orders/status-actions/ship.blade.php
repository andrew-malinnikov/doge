<div>
  <button
    wire:click="$emit('showModal', 'ship')"
    class="flex items-center space-x-1 px-3 py-1 text bg-indigo-600 text-white tracking-wide rounded hover:bg-indigo-500 transition duration-300"
  >
    Отправить
  </button>

  <x-modals.orders.ship  :order="$order" />
</div>
