<div class="px-4">
  <div class="bg-white rounded-md shadow-md">
    <header class="py-4 px-4 border-b-2 border-gray-100">
      <h1 class="font-bold">Новый продукт</h1>
    </header>
    <div class="mt-6 space-y-4 lg:space-y-10">
      <div class="px-4">
        <div class="lg:flex">
          <div class="lg:w-1/3">
            <label>Категория</label>
          </div>
          <div class="flex-1">
            <select wire:model="category_id" wire:change="$emit('categorySelected')" class="form-select w-full">
              @foreach ($categories as $category)
                <option wire:key={{ $category->id }} value="{{$category->id}}">
                  {{$category->name}}
                </option>
              @endforeach
            </select>
          </div>
        </div>
      </div>
      <div class="px-4">
        <div class="lg:flex">
          <div class="lg:w-1/3">
            <label>Название</label>
          </div>
          <div class="flex-1">
            <input wire:model="name" type="text" class="form-input w-full">
          </div>
        </div>
      </div>
      <div class="px-4">
        <div class="lg:flex">
          <div class="lg:w-1/3">
            <label>Артикул</label>
          </div>
          <div class="flex-1">
            <input wire:model="sku" type="text" class="form-input w-full">
          </div>
        </div>
      </div>
      <div>
        @if ($relevantSizes->count() > 0)
        <div class="px-4">
          <div class="lg:flex">
            <div class="lg:w-1/3">
              <label>Размер</label>
            </div>
            <div class="flex-1">
              <select wire:model.lazy="size_id" wire:change="$emit('sizeSelected')" class="form-select w-full">
                <option class="hidden"></option>
                @foreach ($relevantSizes as $size)
                  <option value="{{$size->id}}">{{$size->value}}</option>
                @endforeach
              </select>
            </div>
          </div>
        </div>
        @endif
      </div>
      <div class="px-4">
        <div class="lg:flex">
          <div class="lg:w-1/3">
            <label>Цена и количество</label>
          </div>
          <div class="flex-1 flex space-x-4">
            <div class="w-1/2">
              <input wire:model="price" type="text" class="form-input w-full" placeholder="price">
            </div>
            <div class="w-1/2">
              <input wire:model="quantity" type="text" class="form-input w-full" placeholder="quantity">
            </div>
          </div>
        </div>
      </div>
      <div class="px-4">
        <div class="lg:flex">
          <div class="lg:w-1/3">
            <label>Цвет</label>
          </div>
          <div class="flex-1">
            <select wire:model="color_id" class="form-select w-full">
              <option>не указан</option>
              @foreach ($colors as $color)
                <option value="{{$color->id}}"> {{ $color->label }} </option>
              @endforeach
            </select>
          </div>
        </div>
      </div>
      <div class="px-4">
        <div class="lg:flex">
          <div class="lg:w-1/3">
            <label>Мужское/женское</label>
          </div>
          <div class="flex-1">
            <select wire:model="gender" class="form-select w-full">
              <option>Для всех</option>
              <option value="1">Мужское</option>
              <option value="2">Женское</option>
            </select>
          </div>
        </div>
      </div>
      <div class="px-4">
        <div class="lg:flex">
          <div class="lg:w-1/3">
            <label>Вес в граммах</label>
          </div>
          <div class="flex-1">
            <input wire:model="weight" type="text" class="form-input w-full">
          </div>
        </div>
      </div>
    </div>
    <div class="p-4 mt-6 flex justify-end bg-gray-100 shadow-inner">
      <button wire:click="handle" class="px-3 py-2 bg-indigo-600 text-white text-sm font-semibold rounded-full shadow">Создать</button>
    </div>
  </div>
</div>
