<div class="">
  <div class="flex items-center justify-between">
    <h1 class="text-2xl font-bold">Products</h1>
    <div class="flex justify-end">
      <a class="px-3 py-2 text-xs uppercase font-semibold text-white bg-indigo-600 rounded-full shadow hover:bg-indigo-500 hover:text-white transition duration-300" href="{{ route('admin.products.create') }}">Новый продукт</a>
    </div>
  </div>
  <div class="mt-6">
    <div class="flex items-center space-x-4">
      <div class="flex-1">
        <input wire:model="search" class="form-input w-full md:w-1/2" placeholder="Search">
      </div>
    </div>
    <div class="mt-8 shadow-md rounded-lg divide-y-2 divide-gray-300">
      <x-products.admin.products-list :products="$products" />
    </div>
  </div>
  <div>
    {{ $products->onEachSide(1)->links()}}
  </div>
</div>
