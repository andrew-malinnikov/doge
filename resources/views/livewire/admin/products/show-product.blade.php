<div class="space-y-12 px-4 md:px-0">
  <header class="flex items-top justify-between">
    <div>
      <h1 class="text-2xl font-semibold"><span class="text-xs uppercase font-bold text-indigo-600">{{ $product->category->name }} /</span>  {{ $product->name }}</h1>
      <span>
        @if (! $product->isPublished())
          <span class="inline-block mt-1 text-gray-600">Currently unpublished.</span>
        @endif
      </span>
    </div>

    <div class="flex items-top space-x-2">
      <span>
        <x-inline-notification destination="published" notification="Successfully published!" />
        <x-inline-notification destination="unpublished" notification="Unpublished" />
      </span>
      <div>
        @if (! $product->isPublished())
          <button wire:click.prevent="publish" class="px-3 py-2 bg-indigo-600 text-white text-sm font-semibold rounded-full shadow focus:outline-none">Publish</button>
        @endif
        @if ($product->isPublished())
          <button wire:click.prevent="unpublish" class="rounded-full border border-gray-300 px-4 py-2 bg-white text-base leading-6 font-medium text-gray-700 shadow-sm hover:text-gray-500 focus:outline-none focus:border-blue-300 focus:shadow-outline transition ease-in-out duration-150 sm:text-sm sm:leading-5">Unpublish</button>
        @endif
      </div>
    </div>
  </header>

  <div class="bg-white rounded-md shadow-md">
    <div class="space-y-4 px-4 py-6 md:px-8">
      <div>
        <label>Name</label>
        <div>
          <input wire:model="new_name" type="text" class="form-input w-full">
          <x-inline-validation-message field="new_name" />
        </div>
      </div>
      <div>
        <label>Price</label>
        <div>
          <input wire:model="new_price" type="text" class="form-input w-full">
          <x-inline-validation-message field="new_price" />
        </div>
      </div>
      <div>
        <label>Weight</label>
        <div>
          <input wire:model="new_weight" type="text" class="form-input w-full">
          <x-inline-validation-message field="new_weight" />
        </div>
      </div>
      <div>
        <label>Color</label>
        <div>
          <div class="flex space-x-2 items-center">
            <span class="inline-block w-5 h-5 rounded-full border border-gray-300" style="background: {{ $new_color_id }}"></span>
            <select wire:model="new_color_id" class="w-full form-select">
              <option class="hidden"></option>
              @foreach ($colors as $color)
                <option value="{{ $color->id }}">{{ $color->label }}</option>
              @endforeach
            </select>
            <x-inline-validation-message field="new_color_id" />
          </div>
        </div>
      </div>
      <div>
        <label>Gender</label>
        <div>
          <div class="flex space-x-2 items-center">
            <select wire:model="new_gender" class="w-full form-select">
              <option :value="null">Для всех</option>
              <option value="1">Male</option>
              <option value="2">Female</option>
            </select>
            <x-inline-validation-message field="new_color_id" />
          </div>
        </div>
      </div>
    </div>
    <div class="p-4 md:px-8 space-x-2 font-semibold flex justify-end items-center bg-gray-100">
      <x-inline-notification destination="productDetails" notification="Product updated!" />
      <button
        wire:click="updateDetails"
        class="rounded-full border border-transparent px-4 py-2 bg-indigo-600 text-base leading-6 font-medium text-white shadow-sm hover:bg-indigo-500 focus:outline-none focus:border-indigo-700 focus:shadow-outline-indigo transition ease-in-out duration-150 sm:text-sm sm:leading-5"
      >Save</button>
    </div>
  </div>

  <div class="bg-white rounded-md shadow-md">
    <header class="px-4 md:px-8 py-4 flex justify-between items-center bg-gray-100 border-b border-gray-300 rounded-t-md">
      <div>
        <h2 class="text-lg font-bold">Stock</h2>
      </div>
      <div>
        <button wire:click="$emit('showModal', 'addToStock')" class="flex items-center space-x-1 px-3 py-1 text-sm uppercase text-indigo-500 font-bold border border-indigo-500 rounded-full hover:bg-indigo-600 hover:text-white transition duration-300">
          <svg class="w-4 h-4" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"><circle cx="12" cy="12" r="10"></circle><line x1="12" y1="8" x2="12" y2="16"></line><line x1="8" y1="12" x2="16" y2="12"></line></svg>
          <span>Add</span>
        </button>
      </div>
    </header>
    <div class="divide-y divide-gray-200">
      @if ($availableStocks->count() > 0)
        @foreach ($availableStocks as $stock)
          <div class="p-4 flex">
            @if ($stock->size)
              <div class="w-1/2">
                <div class="text-center text-sm uppercase tracking-wide font-bold text-gray-500">size</div>
                <div class="text-center text-xl font-bold text-indigo-500">{{ $stock->size->value }}</div>
              </div>
            @endif
            <div class="w-1/2 {{ $stock->size ? '' : 'mx-auto' }}">
              <div class="text-center text-sm uppercase tracking-wide font-bold text-gray-500">quantity</div>
              <div class="text-center text-xl font-bold text-indigo-500">{{ $stock->quantity }}</div>
            </div>
          </div>
        @endforeach
      @else
        <div class="px-4 md:px-8 py-6">
          <p class="md:text-lg text-gray-700">На остатках пусто.</p>
        </div>
      @endif
    </div>
  </div>

  <div class="bg-white rounded-md shadow-md">
    <div class="px-4 md:px-8 py-4 flex justify-between items-center bg-gray-100 border-b border-gray-300 rounded-t-md">
      <h2 class="text-lg font-bold">Photos</h2>
    </div>
    <div class="pt-6">
      <div>
        @if ($product->photos()->count() > 0)
          <div wire:key="preview" class="grid grid-cols-1 gap-4 px-4 md:grid-cols-3 md:gap-12 md:px-8 ">
            @foreach ($product->photos()->get() as $photo)
              <div wire:key="{{ $loop->index }}">
                <div class="relative pb-3/4">
                  <img class="absolute inset-0 w-full h-full object-cover rounded shadow" src="{{ $photo->path() }}" />
                </div>
              </div>
            @endforeach
          </div>
        @else
          <div class="px-4 md:px-8 py-6">
            <p class="md:text-lg text-gray-700">Пока нет фоток.</p>
          </div>
        @endif
      </div>
      <div class="px-4 md:px-8 py-3 flex justify-end bg-gray-100">
        <div class="text-right">
          <div x-data="imageInput()" wire:key="file-input" class="relative">
            <button @click="$refs.input.click()" class="rounded-full border border-transparent px-4 py-2 bg-indigo-600 text-base leading-6 font-medium text-white shadow-sm hover:bg-indigo-500 focus:outline-none focus:border-indigo-700 focus:shadow-outline-indigo transition ease-in-out duration-150 sm:text-sm sm:leading-5" type="button">Add photo </button>
            <input x-ref="input" @change="set($refs.input.files[0])" type="file" accept="image/png, image/jpeg" class="cursor-pointer absolute block opacity-0 right-0 top-0" >
          </div>
          <x-inline-validation-message field="file" />
        </div>
      </div>
    </div>
  </div>

  <x-modals.add-to-stock :sizes="$sizes" />
</div>


@push('scripts')
<script>
  const imageInput = function() {
    return {

      set: async function(file) {
        livewire.emit('fileSelected', await this.convertBase64(file))
      },

      convertBase64(file) {
        return new Promise((resolve, reject) => {
          const reader = new FileReader();
          reader.readAsDataURL(file);
          reader.onload = () => resolve(reader.result);
          reader.onerror = error => reject(error);
        });
      }
    }
  }
</script>
@endpush
