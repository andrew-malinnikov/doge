<div>
  @if ($cart && ! $cart->isEmpty())
    <div
      class="ml-8"
      x-data="{ isOpen: false }"
    >
      <div>
        <button x-on:click="isOpen = ! isOpen" class="flex space-x-1">
          <svg class="h-6 w-6 stroke-current text-gray-800"  xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"><circle cx="9" cy="21" r="1"></circle><circle cx="20" cy="21" r="1"></circle><path d="M1 1h4l2.68 13.39a2 2 0 0 0 2 1.61h9.72a2 2 0 0 0 2-1.61L23 6H6"></path></svg>
          <span class="-mt-4 font-bold text-gray-600">+{{ $cart->totalItemsCount() }}</span>
        </button>
      </div>

      <div
        x-show.transition.duration.300ms="isOpen"
        class="fixed inset-0 z-10 bg-white overflow-auto min-h-screen flex flex-col lg:flex-row"
        style="display: none;"
      >
        <div class="flex-grow lg:flex-grow-0 lg:w-1/2 lg:overflow-auto lg:px-24">
          <div class="flex justify-end p-4 lg:fixed lg:top-0 lg:right-0 lg:z-10 lg:p-8">
            <button x-on:click.prevent="isOpen = false">
              <svg class="w-6 h-6 lg:w-8 lg:h-8" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"><line x1="18" y1="6" x2="6" y2="18"></line><line x1="6" y1="6" x2="18" y2="18"></line></svg>
            </button>
          </div>

          <div class="px-8 py-6">
            <h2 class="pb-6 text-xl font-semibold border-b border-gray-300 lg:text-2xl">Ваша корзина</h2>
          </div>

          <div class="px-8 pb-12 overflow-y-auto space-y-8 lg:space-y-12">
            @foreach ($items as $item)
              <div class="flex">
                <div class="w-32">
                  <div class="relative w-full pb-3/4">
                    <img class="absolute inset-0 h-full w-full object-cover border border-gray-100 rounded" src="{{ $item->photo }}">
                  </div>
                </div>
                <div class="flex-1 ml-4">
                  <h3 class="text-indigo-600 font-semibold text leading-5 lg:text-lg">{{ $item->productName }}</h3>
                  <div class="lg:mt-1">
                    @if ($item->sizeLabel)
                      <span class="px-1 border border-gray-400 text-xs text-gray-600 lg:px-2 lg:text-sm">{{ $item->sizeLabel }}</span>
                    @endif
                  </div>
                  <div class="mt-2 flex items-center space-x-2">
                    <button wire:click="decrease({{$item->productId}}, {{$item->sizeId}})" class="block border border-gray-400 p-1 hover:bg-gray-800 hover:text-white transition duration-300">
                      <svg class="w-2 h-2 lg:w-3 lg:h-3 stroke-current" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="4" stroke-linecap="round" stroke-linejoin="round"><line x1="5" y1="12" x2="19" y2="12"></line></svg>
                    </button>
                    <span class="lg:text-lg">{{ $item->quantity }}</span>
                    <button wire:click="increase({{$item->productId}}, {{$item->sizeId}})" {{$item->isInStock() ? '' : 'disabled'}} class="block border border-gray-400 p-1 hover:bg-gray-800 hover:text-white transition duration-300">
                      <svg class="w-2 h-2 lg:w-3 lg:h-3 stroke-current" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="4" stroke-linecap="round" stroke-linejoin="round"><line x1="12" y1="5" x2="12" y2="19"></line><line x1="5" y1="12" x2="19" y2="12"></line></svg>
                    </button>
                  </div>
                  <span>@if (!$item->isInStock()) <span class="text-red-500">У нас столько нету</span> @endif</span>
                  <x-inline-validation-message :field="'stock_capped:' . $item->productId.'-'.$item->sizeId" />
                </div>
                <div class="flex flex-col justify-around items-end">
                  <button wire:click="drop({{$item->productId}}, {{$item->sizeId}})" class="text-gray-600 hover:text-red-600 transition duration-300">
                    <svg class="w-4 h-4 stroke-current" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"><polyline points="3 6 5 6 21 6"></polyline><path d="M19 6v14a2 2 0 0 1-2 2H7a2 2 0 0 1-2-2V6m3 0V4a2 2 0 0 1 2-2h4a2 2 0 0 1 2 2v2"></path><line x1="10" y1="11" x2="10" y2="17"></line><line x1="14" y1="11" x2="14" y2="17"></line></svg>
                  </button>

                  <div>
                    <span class="font-bold text-gray-700 lg:text-xl lg:text-gray-800">{{ $item->cost()->format() }}</span>
                  </div>
                </div>
              </div>
            @endforeach
          </div>
        </div>

        <div class="sticky bottom-0 w-full px-8 py-12 space-y-6 shadow-lg bg-gray-100 lg:w-1/2 lg:overflow-hidden lg:flex lg:flex-col lg:justify-center lg:px-24 lg:bg-gray-200"> <div class="flex justify-between items-center">
            <div class="text-base flex items-center">
              <svg class="w-4 h-4 stroke-current text-gray-600" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="1" stroke-linecap="round" stroke-linejoin="round"><path d="M6 2L3 6v14a2 2 0 0 0 2 2h14a2 2 0 0 0 2-2V6l-3-4z"></path><line x1="3" y1="6" x2="21" y2="6"></line><path d="M16 10a4 4 0 0 1-8 0"></path></svg>
              <span class="ml-2 lg:text-lg">Товары</span>
            </div>
            <div class="text-gray-800 font-semibold lg:text-xl">{{ $cartCost->itemsCost->format() }}</div>
          </div>
          <div class="flex justify-between items-center border-b border-gray-400 pb-6">
            <div class="text-base flex items-center">
              <svg class="w-4 h-4 stroke-current text-gray-600" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="1" stroke-linecap="round" stroke-linejoin="round"><rect x="1" y="3" width="15" height="13"></rect><polygon points="16 8 20 8 23 11 23 16 16 16 16 8"></polygon><circle cx="5.5" cy="18.5" r="2.5"></circle><circle cx="18.5" cy="18.5" r="2.5"></circle></svg>
              <span class="ml-2 lg:text-lg">Доставка</span>
            </div>
            <div class="text-gray-800 font-semibold lg:text-xl">{{ $cartCost->shippingCost->format() }}</div>
          </div>
          <div class="flex justify-between items-center">
            <div class="text-base font-bold flex items-center">
              <svg class="w-4 h-4 fill-current text-gray-500" viewBox="0 0 20 20"><path d="M4 4a2 2 0 00-2 2v1h16V6a2 2 0 00-2-2H4z"></path><path d="M18 9H2v5a2 2 0 002 2h12a2 2 0 002-2V9zM4 13a1 1 0 011-1h1a1 1 0 110 2H5a1 1 0 01-1-1zm5-1a1 1 0 100 2h1a1 1 0 100-2H9z" clip-rule="evenodd" fill-rule="evenodd"></path></svg>
              <span class="ml-2 text-gray-800 lg:text-lg">Итого</span>
            </div>
            <div class="text text-lg text-gray-800 font-bold lg:text-2xl">{{ $cartCost->totalCost->format() }}</div>
          </div>
          <div>
            @if (request()->route()->getName() == 'checkout')
              <a href="#" x-on:click="isOpen = false" class="w-full inline-block text-center px-4 py-3 text-gray-800 font-bold bg-white uppercase tracking-widest rounderoundedd border-2 border-gray-800 hover:bg-gray-700 hover:text-white focus:outline-none transition duration-100 ">Продолжить</a>
            @else
              <a href="{{ route('checkout') }}" class="w-full inline-block text-center px-4 py-3 text-gray-800 font-bold bg-white uppercase tracking-widest rounderoundedd border-2 border-gray-800 hover:bg-gray-700 hover:text-white focus:outline-none transition duration-100 ">Оформляем</a>
            @endif
          </div>
        </div>
      </div>
    </div>
  @endif
</div>
