<div>
  <div class="px-5 lg:px-0">
    <div class="mt-8">
      <h2 class="text-3xl md:text-4xl inline-block bg-indigo-600 text-white px-1">{{ $category->name}}</h2>
      <div class="mt-2">
        <p class="text-gray-700 md:text-lg md:text-gray-700">{{ $category->description }}</p>
      </div>
    </div>
    <div class="mt-6 space-y-12 md:grid md:grid-cols-2 md:gap-6 md:space-y-0 lg:grid-cols-3">
      @foreach ($products as $product)
        <livewire:store.products.product-list-item :product="$product" :key="rand() * $product->id" />
      @endforeach
    </div>
  </div>
</div>
