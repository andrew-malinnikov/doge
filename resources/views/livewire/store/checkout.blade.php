<div class="flex flex-col bg-white lg:flex-row">
  <div class="px-10 py-10 flex-grow overflow-auto lg:w-1/2 lg:px-24">
    <div>
      <a class="inline-flex items-center text-sm text-gray-600 hover:underline" href="{{ route('store') }}">
        <svg class="w-4 h-4 stroke-current" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round">
          <polyline points="11 17 6 12 11 7"></polyline>
          <polyline points="18 17 13 12 18 7"></polyline>
        </svg>
        <span>Обратно в магазикен</span>
      </a>
    </div>
    <h1 class="pb-8 mt-4 text-gray-800 text-2xl font-semibold border-b border-gray-200">Оформляем</h1>
    <div>
      @if ($errors->has('cart'))
      <div class="mb-6 mt-4 px-4 py-4 flex rounded-md border border-red-200 bg-red-100">
        <div>
          <svg class="stroke-current w-5 h-5 text-red-400" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round">
            <circle cx="12" cy="12" r="10"></circle>
            <line x1="15" y1="9" x2="9" y2="15"></line>
            <line x1="9" y1="9" x2="15" y2="15"></line>
          </svg>
        </div>
        <div class="ml-4">
          <p class="text-red-500 leading-tight">{{ $errors->first('cart') }}</p>
        </div>
      </div>
      @endif
    </div>
    <h3 class="mt-10 font-bold text-gray-600 text-sm uppercase md:text-base">Как с вами связаться?</h3>
    <div class="mt-4 pb-8 border-b border-gray-200 space-y-4">
      <div>
        <label class="mb-1 inline-block md:text-lg">Ваш email</label>
        <div>
          <input wire:model.lazy="email" class="form-input md:text-lg w-full" placeholder="Укажите email">
          <x-inline-validation-message field="email" />
        </div>
      </div>
      <div>
        <label class="mb-1 inline-block md:text-lg">Телефон</label>
        <div>
          <input wire:model.lazy="phone" class="form-input md:text-lg w-full" type="tel" placeholder="89779001010">
          <x-inline-validation-message field="phone" />
        </div>
      </div>
    </div>
    <h3 class="mt-8 font-bold text-gray-600 text-sm uppercase md:text-base">Куда везем?</h3>
    <div class="mt-4 space-y-4">
      <div>
        <label class="mb-1 inline-block md:text-lg">Кому отправляем?</label>
        <div>
          <input wire:model.lazy="name" class="form-input md:text-lg w-full" placeholder="ФИО получателя">
          <x-inline-validation-message field="name" />
        </div>
      </div>
      <div>
        <label class="mb-1 inline-block md:text-lg">Улица, дом, квартира</label>
        <div>
          <input wire:model.lazy="line_1" class="form-input md:text-lg w-full" placeholder="Тверская 16, кв 150">
          <x-inline-validation-message field="line_1" />
        </div>
      </div>
      <div>
        <label class="mb-1 inline-block md:text-lg">Город</label>
        <div>
          <input wire:model.lazy="city" class="form-input md:text-lg w-full" placeholder="Москва">
          <x-inline-validation-message field="city" />
        </div>
      </div>
      <div>
        <label class="mb-1 inline-block md:text-lg">Страна</label>
        <div>
          <input wire:model.lazy="country" class="form-input md:text-lg w-full" placeholder="Россия">
          <x-inline-validation-message field="country" />
        </div>
      </div>
      <div>
        <label class="mb-1 inline-block md:text-lg">Почтовый индекс</label>
        <div>
          <input wire:model.lazy="zip_code" class="form-input md:text-lg w-full" placeholder="630000">
          <x-inline-validation-message field="zip_code" />
        </div>
      </div>
    </div>
  </div>
  <div class="sticky bottom-0 px-10 py-6 bg-gray-200 lg:w-1/2 lg:flex lg:flex-col lg:justify-center lg:px-24">
    <div>
      @if ($cart->isEmpty())
      <div class="lg:flex lg:justify-center">
        <div class="lg:space-y-4">
          <p class="text-gray-600 text-xs uppercase font-bold lg:text-base">В корзине ничего нет</p>
          <p><a class="text-gray-800 font-bold underline hover:text-indigo-700 lg:text-xl" href="{{route('store') }}">Выберите что-нибудь?</a></p>
        </div>
      </div>
      @else
      <div class="text-gray-600 text-center text-xs uppercase font-bold lg:text-xl lg:text-gray-800 lg:lowercase lg:font-normal">У вас товаров на {{ $cart->fresh()->calculate()->totalCost->format() }}</div>
      <div class="mt-4">
        <button wire:click="submit" class="py-2 w-full border border-gray-500 font-semibold text-lg text-gray-200 bg-gray-700 tracking-wide rounded transition duration-300 hover:bg-gray-600 lg:text-xl lg:tracking-wider">Размещаю заказ</button>
      </div>
      @endif
    </div>
  </div>
</div>
