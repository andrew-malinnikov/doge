<div class="rounded-lg">
  <div>
    <div class="relative pb-3/5">
      <img class="absolute object-cover inset-0 w-full h-full rounded" src="{{ $product->masterPhotoPath() }}">
    </div>
  </div>

  <div class="relative px-4 py-4 mx-4 shadow-lg bg-white  -mt-8">
    <div class="mt-3 flex justify-between items-start space-x-4">
      <h3><a href="#" class="text-lg font-semibold text-indigo-600 leading-3 md:text-xl">{{ $product->name }}</a></h3>
      <div class="flex-1">
        <div>
          @if ($product->category->isSizeable())
            <div class="space-x-1 flex-1 flex flex-no-wrap justify-end">
              @foreach ($product->stock()->get() as $stock)
                <button
                  wire:key="{{ $loop->index }}"
                  wire:click="toggleSelectedSize({{$stock->size->id}})"
                  class="
                    {{ $stock->size->id == $selectedSizeId ? 'bg-gray-700 text-white' : 'focus:bg-white focus:text-gray-700'}}
                    px-2 border border-gray-700 whitespace-no-wrap transition duration-300 focus:outline-none hover:bg-gray-700 hover:text-white
                  "
                >{{ $stock->size->value }}</button>
              @endforeach
            </div>
            <div class="flex justify-end">
              <x-inline-notification notification="Ага!" destination="sizeSelected" />
              <x-inline-validation-message field="size" />
            </div>
          @endif
        </div>
      </div>
    </div>
    <div class="mt-2 flex justify-between items-center">
      <div>
        <span class="font-bold text-lg md:text-xl md:text-gray-700">{{ $product->formatted_price }} ₽</span>
      </div>
      <div>
        @if (! $product->isOutOfStock())
          <div class="flex flex-col">
            <button wire:click="addToCart" class="px-4 py-1 border-2 border-gray-900 hover:bg-gray-700 hover:text-white transition duration-300 shadow-lg">Купить</button>
            <span class="mt-1"><x-inline-validation-message field="stock_capped" /></span>
          </div>
        @else
          <span class="bg-red-100 text-red-700 shadow text-xs px-2 py-1 md:text-sm">Пока не в наличии, скоро все будет!</span>
        @endif
      </div>
    </div>
  </div>
</div>
