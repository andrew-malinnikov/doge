<div>
  <nav class="flex justify-around border-b border-gray-300 lg:justify-start lg:space-x-8">
    <div wire:key="all" class="py-4 text-gray-700 {{ !$for ? 'border-b-2 border-indigo-500 text-indigo-600 font-semibold' : '' }}">
      <a wire:click.prevent="$set('for', '')" class="md:text-lg" href="#">Все сразу</a>
    </div>
    @foreach ($topLevelCategories as $category)
      <div wire:key="{{ $loop->index }}" class="py-4 text-gray-700 {{ $category->slug == $for ? 'border-b-2 border-indigo-500 text-indigo-600 font-semibold' : '' }}">
        <a wire:click.prevent="setParentCategory('{{$category->slug}}')" class="md:text-lg" href="#">{{ $category->name }}</a>
      </div>
    @endforeach
  </nav>

  <div wire:key="gender-filter">
    @if ($for == 'humans')
      <div class="py-4 py-4 flex justify-center space-x-8 py-4 lg:justify-start lg:space-x-4">
        <button wire:click.prevent="toggleGender(1)" class="{{ $gender == '1' ? 'bg-gray-600 text-white' : '' }} px-4 shadow-inner py-1 border border-gray-500 font-semibold tracking-wide focus:outline-none">Мужское</button>
        <button wire:click.prevent="toggleGender(2)" class="{{ $gender == '2' ? 'bg-gray-600 text-white' : '' }} px-4 shadow-inner py-1 border border-gray-500 font-semibold tracking-wide focus:outline-none">Женское</button>
      </div>
    @endif
  </div>

{{--   <div wire:key="search"  class="py-8 px-4 bg-white">
    <div class="relative">
      <div class="absolute inset-y-0 left-0 pl-4 flex items-center">
        <svg class="fill-current text-gray-600 w-4 h-4" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20"><path d="M12.9 14.32a8 8 0 1 1 1.41-1.41l5.35 5.33-1.42 1.42-5.33-5.34zM8 14A6 6 0 1 0 8 2a6 6 0 0 0 0 12z"></path></svg>
      </div>
      <input
        wire:model.lazy="search"
        class="transition-colors duration-100 ease-in-out focus:outline-0 border border-transparent focus:bg-white focus:border-gray-300 placeholder-gray-600 rounded-lg bg-gray-200 py-2 pr-4 pl-10 block w-full appearance-none leading-normal focus:outline-none"
        type="text"
        placeholder="Поиск"
      >
    </div>
  </div>
 --}}
  <div wire:key="store" class="space-y-24 divide-y-8 divide-gray-400">
    @foreach ($categories as $category)
      <livewire:store.categories.category-item 
        :key="rand() * $category->id"
        :category="$category" 
        :selected-gender="$gender"
      />
    @endforeach
  </div>
</div>
