  <header x-data="{isMenuOpen: false}" class="sticky top-0  z-10" >
    <div class="px-4 py-6 relative shadow bg-white lg:px-0">
      <div class="max-w-6xl flex justify-between items-center mx-auto">
        <div class="flex items-center space-x-8">
          <div>
            <a href="/">
              <img class="h-8 md:h-auto" src="/images/Sobaken.svg" alt="Собакен">
            </a>
          </div>
          <nav class="hidden md:block">
            @include('partials.navigation')
          </nav>
        </div>
        <div class="flex items-center">
          <livewire:store.cart/>
          <button
            class="block md:hidden ml-4 p-2 bg-white rounded shadow-lg hover:bg-gray-200 transition duration-300"
            @click.prevent="isMenuOpen = ! isMenuOpen"
          >
            <svg x-show="! isMenuOpen" class="w-6 h-6 stroke-current text-gray-800" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="1" stroke-linecap="round" stroke-linejoin="round"><line x1="3" y1="12" x2="21" y2="12"></line><line x1="3" y1="6" x2="21" y2="6"></line><line x1="3" y1="18" x2="21" y2="18"></line></svg>
            <svg x-show="isMenuOpen" class="w-6 h-6 stroke-current text-gray-800" style="display: none;" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="1" stroke-linecap="round" stroke-linejoin="round"><line x1="18" y1="6" x2="6" y2="18"></line><line x1="6" y1="6" x2="18" y2="18"></line></svg>
          </button>
        </div>
      </div>
    </div>
    <div x-show="isMenuOpen" style="display: none;">
      <div class="bg-gray-800 py-4">
        <ul class="space-y-2">
          <li><a class="{{ request()->route()->getName() == 'store' ? 'active-nav-mobile-link' : '' }} px-4 py-2 my-1 block text-gray-400 rounded mx-2 hover:bg-gray-700 hover:underline transition duration-300 hover:text-white" href="/store">Магазикен</a></li>
          <li><a class="{{ request()->route()->getName() == 'delivery' ? 'active-nav-mobile-link' : '' }} px-4 py-2 my-1 block text-gray-400 rounded mx-2 hover:bg-gray-700 hover:underline transition duration-300 hover:text-white" href="#">Доставка и оплата</a></li>
          <li><a class="{{ request()->route()->getName() == 'collaboration' ? 'active-nav-mobile-link' : '' }} px-4 py-2 my-1 block text-gray-400 rounded mx-2 hover:bg-gray-700 hover:underline transition duration-300 hover:text-white" href="#">Сотрудничество</a></li>
          <li><a class="{{ request()->route()->getName() == 'contact' ? 'active-nav-mobile-link' : '' }} px-4 py-2 my-1 block text-gray-400 rounded mx-2 hover:bg-gray-700 hover:underline transition duration-300 hover:text-white" href="#">Контакты</a></li>
          <li>
            @if(auth()->check() && auth()->user()->isAdmin())
              <a class="px-4 py-2 my-1 block text-gray-400 rounded mx-2 hover:bg-gray-700 hover:underline transition duration-300 hover:text-white" href="{{ route('admin.dashboard') }}">Админкен</a>
            @endif
          </li>
        </ul>
      </div>
    </div>
  </header>
