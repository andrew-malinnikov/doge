<div class="flex items-center">
{{--   <div> --}}
{{--     <a class="inline-block social-link inst" href="https://instagram.com/sobakenstore" target="_blank"> --}}
{{--       <img class="default-image" src="/images/inst.svg"> --}}
{{--       <img class="hover-image h-0" src="/images/inst_hover.svg"> --}}
{{--     </a> --}}
{{--   </div> --}}
{{--   <div class="ml-2"> --}}
{{--     <a class="inline-block social-link transition duration-300" href="https://www.facebook.com/sobakenstore/" target="_blank"> --}}
{{--       <img class="default-image transition duration-300" src="/images/fb.svg"> --}}
{{--       <img class="hover-image h-0 transition duration-300" src="/images/fb_hover.svg"> --}}
{{--     </a> --}}
{{--   </div> --}}
  <div class="ml-4">
    <a class="inline-block social-link" href="https://vk.com/sobakenstore" target="_blank">
      <img class="default-image" src="/images/vk.svg">
      <img class="hover-image h-0" src="/images/vk_hover.svg">
    </a>
  </div>
</div>
