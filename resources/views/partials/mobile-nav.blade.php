<div class="bg-white fixed inset-0 overflow-hidden z-10">
  <header class="flex justify-between items-center px-4 py-5 bg-orange-600 shadow-lg">
    <div>
      <a href="/"><img class="h-8" src="/images/logo.png" alt="Собакен"></a>
    </div>
    <div>
      <button>
        <svg class="w-6 h-6 stroke-current text-orange-200" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"><line x1="18" y1="6" x2="6" y2="18"></line><line x1="6" y1="6" x2="18" y2="18"></line></svg>
      </button>
    </div>
  </header>

  <div class="mt-10">
    <ul>
      <li><a class="px-8 py-4 my-1 block font-semibold hover:bg-orange-200 hover:underline transition duration-300 hover:text-orange-800" href="{{ route('store') }}">Магазикен</a></li>
      <li><a class="px-8 py-4 my-1 block font-semibold hover:bg-orange-200 hover:underline transition duration-300 hover:text-orange-800" href="#">Доставка и оплата</a></li>
      <li><a class="px-8 py-4 my-1 block font-semibold hover:bg-orange-200 hover:underline transition duration-300 hover:text-orange-800" href="#">Сотрудничество</a></li>
      <li><a class="px-8 py-4 my-1 block font-semibold hover:bg-orange-200 hover:underline transition duration-300 hover:text-orange-800" href="#">Контакты</a></li>
    </ul>
  </div>
</div>
