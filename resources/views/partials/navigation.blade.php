<ul class="flex space-x-6">
  <x-nav-link-item route="store">Магазикен</x-nav-link-item>
  <x-nav-link-item route="#">Доставка и оплата</x-nav-link-item>
  <x-nav-link-item route="#">Сотрудничество</x-nav-link-item>
  <x-nav-link-item route="#">Контакты</x-nav-link-item>
  <div>
    @if(auth()->check() && auth()->user()->isAdmin())
      <x-nav-link-item route="admin.dashboard">Админкен</x-nav-link-item>
    @endif
  </div>
</ul>
