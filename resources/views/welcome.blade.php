<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>Doge</title>
  <link href="https://fonts.googleapis.com/css2?family=Montserrat:wght@400;600;800;900&display=swap" rel="stylesheet">
  <link rel="stylesheet" type="text/css" href="/css/app.css">
</head>
<body class="antialiased font-sans text-gray-900">
  <div id="app">
    <header>
      <div class="max-w-6xl mx-auto relative px-4 lg:px-0">
        <div class="absolute top-0 right-0 mr-0 ml-auto w-full pb-3/5  md:pb-1/2 lg:pb-2/5">
          <img class="absolute h-full right-0" src="/images/doge_with_hands_lg.svg">
        </div>

        <div class="flex justify-between items-center py-6 lg:justify-start">
          <div class="relative">
            <div class="font-bold text-3xl tracking-widest">
              <a href="/"><img class="h-8 md:h-auto" src="/images/Sobaken.svg" alt="Собакен"></a>
            </div>
          </div>
          <div class="hidden lg:flex items-center justify-between flex-1 relative z-10">
            <div class="ml-10">
              <ul class="flex space-x-6">
                <x-nav-link-item route="store">Магазикен</x-nav-link-item>
                <x-nav-link-item route="home">Доставка и оплата</x-nav-link-item>
                <x-nav-link-item route="home">Сотрудничество</x-nav-link-item>
                <x-nav-link-item route="home">Контакты</x-nav-link-item>
              </ul>
            </div>

            <div>
              @include ('partials.links')
            </div>
          </div>
          <dropdown-menu class="relative lg:hidden" />
        </div>

        <div class="relative mt-8  md:mt-12 lg:mt-32">
          <h1 class="text-4xl leading-tight font-bold lg:text-6xl">
            Помогаем <br class="md:hidden"> людям <br> помогать животным
          </h1>
          <p class="mt-6 text-lg text-gray-700 lg:text-2xl">
            Продаём дизайнерские вещи, <br class="hidden md:inline lg:hidden"> прибыль отдаём в приюты
          </p>
        </div>
      </div>
    </header>

    <section class="mt-12 px-4 md:hidden">
      <div class="max-w-6xl mx-auto">
        @include ('partials.links')
      </div>
    </section>

    <section class="mt-12 px-4 lg:mt-32 lg:px-0">
      <div class="max-w-6xl mx-auto">
        <h2 class="font-bold text-2xl  md:text-3xl lg:text-4xl">Как мы помогаем</h2>
        <div class="lg:flex">
          <div class="lg:w-1/2">
            <div class="mt-10 md:mt-0 md:flex md:items-baseline">
              <div class="md:w-1/2">
                <div class="flex justify-center">
                  <img src="/images/hang.svg">
                </div>
                <p class="mt-2 md:text-lg">
                  Придумываем и дизайним разные вещички: носочки, шопперы, худи, футболки и другую милоту
                </p>
              </div>

              <div class="md:w-1/2">
                <div class="flex justify-center">
                  <img src="/images/beast_on_chair.png">
                </div>
                <p class="mt-2 md:text-lg">
                  Выбираем локальных мастеров <br> и натуральные материалы, которые <br> не вредят животным и окружающей среде
                </p>
              </div>
            </div>
          </div>
          <div class="hidden md:flex mt-20 lg:w-1/2 lg:mt-40">
            <div class="mt-12 flex items-center justify-center bg-center bg-no-repeat" style="background-image: url('/images/gold_border.png'); height: 257px;">
              <div class="w-1/2 -mt-40 -mr-8">
                <div class="flex justify-end">
                  <img src="/images/market.svg">
                </div>
                <p class="w-2/3 mr-0 ml-auto mt-4 text-lg">
                  Продаём вещички онлайн <br> и на ярмарках. Прибыль отдаем в приют г. Рязани  «Спасибо за жизнь»
                </p>
              </div>
              <div class="w-1/2 pl-16">
                <h3 class="text-lg font-bold">За 3 месяца</h3>
                <ul class="mt-2">
                  <li class="text-lg leading-8">февраль 12 000 ₽</li>
                  <li class="text-lg leading-8">март 12 000 ₽</li>
                  <li class="text-lg leading-8">апрель 12 000 ₽</li>
                </ul>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>

    <section class="md:hidden">
      <div class="max-w-6xl mx-auto">
        <div class="mt-10 px-4">
          <div class="flex justify-center">
            <img src="/images/market.svg">
          </div>
          <p class="mt-2">
            Продаём вещички онлайн <br> и на ярмарках. Прибыль отдаем в приют г. Рязани  «Спасибо за жизнь»
          </p>
        </div>

        <div class="mt-12 flex items-center justify-center bg-center bg-no-repeat" style="background-image: url('/images/gold_border.png'); height: 257px;">
          <div>
            <h3 class="text-lg font-bold">За 3 месяца</h3>
            <ul class="mt-2">
              <li class="text-lg leading-8">февраль 12 000 ₽</li>
              <li class="text-lg leading-8">март 12 000 ₽</li>
              <li class="text-lg leading-8">апрель 12 000 ₽</li>
            </ul>
          </div>
        </div>
      </div>
    </section>


    <section class="mt-12 py-6 px-4 lg:px-0">
      <div class="max-w-6xl mx-auto">
        <h2 class="text-2xl font-bold md:hidden">Наша миссия</h2>

        <div class="md:flex md:items-end">
          <div class="md:flex md:items-center md:w-2/5 lg:w-1/3">
            <div class="hidden md:block">
              <img src="/images/doge_partrait.svg">
            </div>
            <div class="flex flex-col items-start">
              <img src="/images/doge_in_da_house.svg">
              <p class="pl-10 text-xs italic font-pt-serif leading-5 md:leading-4">Эппи и Прима. Эппи уже дома. <br> А Прима собакен-боякен и ещё ждёт своих людей</p>
            </div>
          </div>

          <div class="mt-8 md:w-1/2 md:mt-0 lg:w-3/5 lg:ml-10">
            <h2 class="hidden md:block text-3xl font-bold lg:text-4xl">Наша миссия</h2>

            <p class="md:text-lg">Научить людей осознанному отношению к животным:
            как вести себя и понимать поведение животных,
            чтобы жить вместе в этом мире.</p>

            <p class="mt-6 md:text-lg">Сократить количество бездомных животных в России. Рассказать окружающим
            о способах помочь животным, попавшим в беду. </p>
          </div>
        </div>
      </div>
    </section>

    <section class="mt-12 lg:mt-24 px-4 lg:px-0">
      <div class="max-w-6xl mx-auto">
        <h2 class="text-2xl font-bold leading-7 md:text-3xl md:leading-8 lg:text-4xl">Кто мы, <br class="md:hidden"> зачем мы это делаем <br class="lg:hidden"> <span class="italic text-sobaken-red font-black">и при чём тут носки?</span></h2>
        <video controls class="w-full mt-8">
          <source src="/images/video.mp4" type="video/mp4">
        </video>
        <p class="mt-2 font-pt-serif text-base italic md:text-lg">
          Основатели проекта Рита и Лена рассказывают о приюте, производстве <br class="lg:hidden"> и гав-гав-гав!
        </p>
      </div>
    </section>

    <section class="mt-40 px-4">
      <div class="max-w-6xl mx-auto relative md:flex md:items-center lg:mt-24">
        <div class="hidden md:block md:w-1/2">
          <img src="/images/doges_with_lena.png">
          <span class="italic font-pt-serif ">Это Лена, пытается укротить безумцев :-)</span>
        </div>
        <div class="relative w-full md:w-1/2">
          <div class="absolute top-0 right-0 -mt-48 mr-4 md:mr-12 lg:mr-32"><img src="/images/doges_playing.svg"></div>
          <h2 class="text-2xl font-black lg:text-4xl">Цель проекта</h2>

          <div class="mt-6">
            <p class="md:text-lg lg:text-xl">Поддерживать финансово <br clear="lg:hidden"> и информационно городские и частные приюты.</p>

            <p class="mt-4 md:text-lg lg:text-xl">Собрать деньги на собственный мультифункциональный центр  для животных.</p>

            <p class="mt-4 md:text-lg lg:text-xl">Вести постоянную просветительную работу с детьми, студентами  <br> и взрослыми.</p>

            <div class="-ml-3 mt-4">
              <img src="/images/sock.svg">
            </div>

            <p class="mt-4 md:text-lg">Мы можем помогать и вашим пёселям — пишите на  <a class="text-blue-700 underline" href="mailto:sobakenstore.online@gmail.com">sobakenstore.online@gmail.com</a></p>
          </div>
        </div>
      </div>
    </section>

    <section class="mt-12 px-4 md:mt-20 lg:px-0">
      <div class="max-w-6xl mx-auto md:flex md:items-center">
        <div class="w-full md:w-2/3">
          <h2 class="text-2xl font-bold leading-8 md:text-3xl md:text-gray-800 md:leading-snug lg:text-4xl lg:leading-normal">Подписывайтесь на наши соцсети, делитесь информацией о нас <br> и покупайте наши вещички</h2>
          <div class="flex justify-center mt-8 md:hidden">
            <img src="/images/doge_hugs.svg">
          </div>

          <div class="mt-4">
            <p class="md:text-lg lg:text-xl">
              Магазин на сайте ещё в разработке. Принимаем заказы в инстаграм
              <a class="text-blue-700" href="https://instagram.com/sobakenstore">@sobakenstore</a>
            </p>

            <div class="flex justify-center mt-4 md:justify-start md:mt-10">
              <a class="social-link" href="https://instagram.com/sobakenstore" target="_blank">
                <img class="default-image" src="/images/cta.svg">
                <img class="hover-image h-0" src="/images/cta_hover.svg">
              </a>
            </div>
          </div>
        </div>
        <div class="hidden md:block md:w-1/3">
          <div class="flex justify-center mt-8">
            <img src="/images/doge_hugs.svg">
          </div>
        </div>
      </div>
    </section>

    <footer class="relative mt-12 pt-10 bg-gray-100">
      <div class="absolute top-0 w-full h-20 -mt-8 bg-center bg-no-repeat md:bg-left md:mt-0 lg:-mt-16 lg:h-40 lg:bg-center" style="background-image: url('/images/footer_line.svg')"></div>
      <div class="max-w-6xl mx-auto">
        <div class="px-4 md:flex md:flex-row-reverse md:items-end lg:justify-around">
          <div class="md:w-1/2 md:flex md:flex-col-reverse md:items-end md:pb-6 md:pr-4 lg:flex-row-reverse lg:w-2/3 lg:pb-16">
            <div class="md:mt-4 lg:w-1/2">
              <a href="/"><img src="/images/logo_footer.svg"></a>
              <p class="text-sm md:text-right md:text-lg lg:text-left mx-4">Помогаем людям <br> помогать животным</p>
            </div>

            <div class="w-1/2">
              <div class="mt-6 md:flex md:justify-end lg:justify-start">
                @include ('partials.links')
              </div>

              <div class="mt-1">
                <p class="text-gray-700 md:text-right md:text-lg lg:text-left lg:w-1/2">Пишем советы, выкладываем новую продукцию и новости проекта</p>
              </div>
            </div>
          </div>

          <div class="flex items-end pr-4 md:w-1/2 lg:w-1/3">
            <img src="/images/doge_footer.svg">
            <p class="pb-6 italic font-pt-serif text-xs md:text-base">Это Эльза, самый счастливый <br> и спокойный собакен в мире</p>
          </div>
        </div>
      </div>
    </footer>
  </div>
  <script src="/js/app.js"></script>
</body>
</html>
