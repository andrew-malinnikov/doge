<?php

Route::group(['layout' => 'components.admin'], function () {
    Route::livewire('/', 'admin.dashboard')->name('admin.dashboard');

    // Products
    Route::livewire('products', 'admin.products.index-products')->name('admin.products.index');
    Route::livewire('products/create', 'admin.products.create-product')->name('admin.products.create');
    Route::livewire('products/{product}', 'admin.products.show-product')->name('admin.products.show');

    // Categories
    Route::livewire('categories/create', 'admin.categories.create')->name('admin.categories.create');

    // Orders
    Route::livewire('orders', 'admin.orders.index')->name('admin.orders.index');
    Route::livewire('orders/{order}', 'admin.orders.show')->name('admin.orders.show');
});
