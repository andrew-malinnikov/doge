<?php

Route::group(['layout' => 'components.auth'], function () {
    Route::livewire('login', 'auth.login')->name('login')->middleware('guest');
    Route::livewire('register', 'auth.register')->name('register')->middleware('guest');
    Route::get('logout', 'LoginController@logout')->name('logout')->middleware('auth');
});
