<?php

Route::view('/', 'landing')->name('home');

Route::group(['prefix' => 'store', 'middleware' => ['mvp']], function () {
    Route::view('/', 'store.store')->name('store');
    Route::livewire('/checkout', 'store.checkout')->name('checkout');

    // Products
    Route::livewire('products/{product:slug}', 'store.products.show')->name('products.show');
});

Route::group(['prefix' => 'orders', 'layout' => 'components.store', 'middleware' => ['mvp']], function () {
    Route::livewire('{orderNumber}', 'orders.confirmation')->name('order-confirmation');
});
