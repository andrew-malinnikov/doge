module.exports = {
  theme: {
    extend: {
      padding: {
        '1/2': '50%',
        '1/3': '33.333333%',
        '2/3': '66.666667%',
        '1/4': '25%',
        '2/4': '50%',
        '3/4': '75%',
        '1/5': '20%',
        '2/5': '40%',
        '3/5': '60%',
        '4/5': '80%',
        '1/6': '16.666667%',
        '2/6': '33.333333%',
        '3/6': '50%',
        '4/6': '66.666667%',
        '5/6': '83.333333%',
        'full': '100%'
      },
      fontFamily: {
        sans: [
          'Montserrat',
        ],
        'pt-serif': [
          'PT Serif'
        ],
      },
      colors: {
        'sobaken-red': '#BC1035',
      }
    },
    transitionProperty: {
      'height': 'height',
    },
    customForms: theme => ({
      default: {
        'input, select, textarea': {
          borderRadius: theme('borderRadius.md'),
          backgroundColor: theme('colors.gray.100'),
          borderColor: theme('colors.gray.200'),
          padding: `${theme('spacing.1')} ${theme('spacing.2')}`,
          boxShadow: theme('boxShadow.sm'),
          color: theme('colors.gray.800'),
          '&:focus': {
            backgroundColor: theme('colors.white'),
          }
        },
        textarea: {
          fontSize: theme('fontSize.sm'),
          padding: `${theme('spacing.2')} ${theme('spacing.3')}`,
        },
        checkbox: {
          width: theme('spacing.4'),
          height: theme('spacing.4'),
          color: theme('colors.teal.500'),
        },
      },
    }),
  },
  variants: {
    display: ['responsive', 'hover'],
    visibility: ['responsive', 'hover'],
  },
  purge: {
      content: [
          './app/**/*.php',
          './resources/**/*.html',
          './resources/**/*.js',
          './resources/**/*.jsx',
          './resources/**/*.ts',
          './resources/**/*.tsx',
          './resources/**/*.php',
          './resources/**/*.vue',
          './resources/**/*.twig',
      ],
      options: {
          defaultExtractor: (content) => content.match(/[\w-/.:]+(?<!:)/g) || [],
          whitelistPatterns: [/-active$/, /-enter$/, /-leave-to$/, /show$/],
      },
  },
  plugins: [
    require('@tailwindcss/custom-forms'),
  ],
}
