<?php

namespace Tests\Factories;

use App\Users\User;

class UserFactory
{
    protected bool $asAdmin = false;

    public function create($params = [])
    {
        $user = factory(User::class)->create($params);

        if ($this->asAdmin) {
            config(['doge.admins' => array_merge(config('doge.admins'), [$user->email])]);
        }

        return $user;
    }

    public function asAdmin()
    {
        $this->asAdmin = true;

        return $this;
    }
}
