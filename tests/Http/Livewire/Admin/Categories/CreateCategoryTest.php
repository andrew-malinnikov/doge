<?php

use App\Http\Livewire\Admin\Categories\Create;
use App\Store\Product\Category;
use App\Store\Product\Size;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Livewire\Livewire;
use Tests\RolesConcerns;
use Tests\TestCase;

class CreateCategoryTest extends TestCase
{
    use DatabaseTransactions;
    use RolesConcerns;

    /**
     * @test
     * @covers \CreateCategory::handle
     */
    public function it_stores_category()
    {
        $this->actingAs($this->getAdmin());
        $testable = Livewire::test(Create::class)
            ->set('name', 'Category')
            ->set('slug', 'test-category')
            ->set('sizeType', Size::TYPE_LETTER)
            ->call('handle')
            ->assertRedirect(route('admin.products.index'));

        $this->assertEquals(1, Category::count());
        $category = Category::first();
        $this->assertEquals('Category', $category->name);
        $this->assertEquals('test-category', $category->slug);
        $this->assertEquals(Size::TYPE_LETTER, $category->size_type);
        $this->assertNull($category->parent_id);
    }

    /**
     * @test
     * @covers \CreateCategory::handle
     */
    public function it_stores_category_category_without_specifying_the_size_type()
    {
        $this->actingAs($this->getAdmin());
        $testable = Livewire::test(Create::class)
            ->set('name', 'Category')
            ->set('slug', 'test-category')
            ->call('handle')
            ->assertRedirect(route('admin.products.index'));

        $this->assertEquals(1, Category::count());
        $category = Category::first();
        $this->assertEquals('Category', $category->name);
        $this->assertNull($category->size_type);
        $this->assertNull($category->parent_id);
    }
}
