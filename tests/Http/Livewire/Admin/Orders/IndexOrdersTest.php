<?php

use App\Sales\Order\Order;
use App\Sales\Order\Status\OrderStatusManager;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Livewire\Livewire;
use Tests\TestCase;

class IndexOrdersTest extends TestCase
{
    use DatabaseTransactions;

    /**
     * @test
     * @covers \IndexOrders::handle
     */
    public function it_loads_orders()
    {
        $order1 = factory(Order::class)->create();
        $order2 = factory(Order::class)->create();

        Livewire::test('admin.orders.index')
            ->set('status', null)
            ->assertSee($order1->email)
            ->assertSee($order2->email);
    }

    /**
     * @test
     */
    public function it_fetches_orders_with_respect_to_filter_status()
    {
        $order1 = factory(Order::class)->create(['status' => OrderStatusManager::ACTIVE]);
        $order2 = factory(Order::class)->create(['status' => OrderStatusManager::AWAITING_PAYMENT]);

        Livewire::test('admin.orders.index')
            ->set('status', OrderStatusManager::AWAITING_PAYMENT)
            ->assertDontSee($order1->email)
            ->assertSee($order2->email);
    }

    /**
     * @test
     */
    public function it_filters_by_awaiting_payment_status_by_defualt()
    {
        Livewire::test('admin.orders.index')
            ->assertSet('status', OrderStatusManager::AWAITING_PAYMENT);
    }
}
