<?php

use App\Sales\Order\Order;
use App\Sales\Order\Status\AwaitingPayment;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Livewire\Livewire;
use Tests\TestCase;

class ShowTest extends TestCase
{
    use DatabaseTransactions;

    /**
     * @test
     * @covers \Show::handle
     */
    public function it_shows_an_order()
    {
        $order = factory(Order::class)->create();

        Livewire::test('admin.orders.show', ['order' => $order])
            ->assertSee($order->number);
    }

    /**
     * @test
     */
    public function it_shows_accessors_status_action()
    {
        $order = factory(Order::class)->create([
            'status' => (new AwaitingPayment())->get(),
        ]);

        $availableActions = (new AwaitingPayment())->accessors()
            ->map->livewireComponentName();

        Livewire::test('admin.orders.show', ['order' => $order])
            ->assertSeeLivewire($availableActions->first());
    }
}
