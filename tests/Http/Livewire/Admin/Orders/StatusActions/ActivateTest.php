<?php

use App\Sales\Order\Order;
use App\Sales\Order\Status\OrderStatusManager;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Livewire\Livewire;
use Tests\TestCase;

class ActivateTest extends TestCase
{
    use DatabaseTransactions;

    /**
     * @test
     */
    public function it_sets_order_to_active_status()
    {
        $order = factory(Order::class)->create([
            'status' => OrderStatusManager::AWAITING_PAYMENT,
        ]);

        Livewire::test('admin.orders.status-actions.activate', ['order' => $order])
            ->call('handle')
            ->assertEmitted('orderStatusChanged');

        $this->assertEquals(OrderStatusManager::ACTIVE, $order->fresh()->status);
    }
}
