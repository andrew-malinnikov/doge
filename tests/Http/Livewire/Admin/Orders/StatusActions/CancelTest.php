<?php

use App\Http\Livewire\Admin\Orders\StatusActions\Cancel;
use App\Sales\Order\Order;
use App\Sales\Order\Status\OrderStatusManager;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Livewire\Livewire;
use Tests\TestCase;

class CancelTest extends TestCase
{
    use DatabaseTransactions;

    /**
     * @test
     * @covers \Cancel::handle
     */
    public function it_transfers_order_to_cancelled_status()
    {
        $order = factory(Order::class)->create([
            'status' => OrderStatusManager::ACTIVE,
        ]);

        Livewire::test(Cancel::class, ['order' => $order])
            ->call('handle')
            ->assertEmitted('orderStatusChanged');

        $this->assertEquals(OrderStatusManager::CANCELLED, $order->fresh()->status);
    }
}
