<?php

namespace Tests\Http\Livewire\Admin\Orders\StatusActions;

use App\Http\Livewire\Admin\Orders\StatusActions\Deliver;
use App\Sales\Order\Order;
use App\Sales\Order\Status\OrderStatusManager;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Livewire\Livewire;
use Tests\TestCase;

class DeliverTest extends TestCase
{
    use DatabaseTransactions;

    /**
     * @test
     * @covers \Deliver::handle
     */
    public function it_sets_order_status_deliver()
    {
        $order = factory(Order::class)->create([
            'status' => OrderStatusManager::SHIPPED,
        ]);

        Livewire::test(Deliver::class, ['order' => $order])
            ->call('handle')
            ->assertEmitted('orderStatusChanged');

        $this->assertEquals(
            OrderStatusManager::DELIVERED,
            $order->fresh()->status
        );
    }
}
