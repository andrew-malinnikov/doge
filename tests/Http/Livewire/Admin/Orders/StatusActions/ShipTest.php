<?php

use App\Http\Livewire\Admin\Orders\StatusActions\Ship;
use App\Sales\Order\Order;
use App\Sales\Order\Status\OrderStatusManager;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Livewire\Livewire;
use Tests\TestCase;

class ShipTest extends TestCase
{
    use DatabaseTransactions;

    /**
     * @test
     * @covers \Ship::handle
     */
    public function it_can_mark_order_as_shipped()
    {
        $order = factory(Order::class)->create([
            'status' => OrderStatusManager::ACTIVE,
        ]);

        Livewire::test(Ship::class, ['order' => $order])
            ->set('trackingNumber', 'TEST_TRACKING_123')
            ->call('handle')
            ->assertEmitted('orderStatusChanged');

        $this->assertEquals(OrderStatusManager::SHIPPED, $order->fresh()->status()->get());
    }

    /**
     * @test
     * @covers \Ship::handle
     */
    public function it_requires_tracking_number_to_ship()
    {
        $order = factory(Order::class)->create([
            'status' => OrderStatusManager::ACTIVE,
        ]);

        Livewire::test(Ship::class, ['order' => $order])
            ->call('handle')
            ->assertHasErrors('trackingNumber');

        $this->assertEquals(OrderStatusManager::ACTIVE, $order->fresh()->status()->get());
    }
}
