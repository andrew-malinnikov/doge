<?php

namespace Tests\Http\Livewire\Admin\Products;

use App\Http\Livewire\Admin\Products\CreateProduct;
use App\Store\Color\Color;
use App\Store\Product\Category;
use App\Store\Product\Product;
use App\Store\Product\Size;
use Facades\Tests\Factories\UserFactory;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Livewire\Livewire;
use Tests\TestCase;

class CreateProductTest extends TestCase
{
    use DatabaseTransactions;

    protected function setUp(): void
    {
        parent::setUp();
        $this->admin = UserFactory::asAdmin()->create();
    }

    /**
     * @test
     */
    public function component_is_loaded()
    {
        $this->actingAs($this->admin)
            ->get('/admin/products/create')
            ->assertSuccessful()
            ->assertSeeLivewire('admin.products.create-product');
    }

    /**
     * @test
     * @covers \CreateProduct::handle
     */
    public function it_stores_new_product()
    {
        $category = factory(Category::class)->create();
        $color = factory(Color::class)->create();
        $size = factory(Size::class)->create();

        $this->actingAs($this->admin);
        $testable = Livewire::test(CreateProduct::class)
            ->set('sku', 'SKU_TEST')
            ->set('name', 'The product')
            ->set('price', '1900') // price passed in in rubles
            ->set('quantity', '3')
            ->set('color_id', $color->id)
            ->set('category_id', $category->id)
            ->set('size_id', $size->id)
            ->set('gender', '1')
            ->set('weight', '120')
            ->call('handle');

        $this->assertEquals(1, Product::all()->count());
        $product = Product::first();
        $testable->assertRedirect(route('admin.products.show', ['product' => $product]));
        $this->assertEquals('SKU_TEST', $product->sku);
        $this->assertEquals('The product', $product->name);
        $this->assertEquals('the-product', $product->slug);
        $this->assertEquals(190000, $product->price); // stored in cents
        $this->assertEquals('1', $product->gender);
        $this->assertTrue($product->category->is($category));
        $this->assertEquals(120, $product->weight);
        $this->assertEquals(1, $product->stock()->count());
        $productInStock = $product->stock->first();
        $this->assertEquals(3, $productInStock->quantity);
        $this->assertTrue($productInStock->size->is($size));
        $this->assertFalse($product->isPublished());
    }

    /**
     * @test
     * @covers \CreateProduct::handle
     */
    public function name_is_required()
    {
        $category = factory(Category::class)->create();
        $color = factory(Color::class)->create();
        $size = factory(Size::class)->create();

        $this->actingAs($this->admin);
        $testable = Livewire::test(CreateProduct::class)
            ->set('sku', 'SKU_TEST')
            ->set('name', '')
            ->set('price', '1900')
            ->set('quantity', '3')
            ->set('color_id', $color->id)
            ->set('category_id', $category->id)
            ->set('size_id', $size->id)
            ->set('weight', '120')
            ->call('handle')
            ->assertHasErrors(['name' => 'required']);
    }

    /**
     * @test
     * @covers \CreateProduct::handle
     */
    public function price_is_required()
    {
        $category = factory(Category::class)->create();
        $color = factory(Color::class)->create();
        $size = factory(Size::class)->create();

        $this->actingAs($this->admin);
        $testable = Livewire::test(CreateProduct::class)
            ->set('sku', 'SKU_TEST')
            ->set('name', 'Foo')
            ->set('price', '')
            ->set('quantity', '3')
            ->set('color_id', $color->id)
            ->set('category_id', $category->id)
            ->set('size_id', $size->id)
            ->set('weight', '120')
            ->call('handle')
            ->assertHasErrors(['price' => 'required']);
    }

    /**
     * @test
     * @covers \CreateProduct::handle
     */
    public function price_must_numeric()
    {
        $category = factory(Category::class)->create();
        $color = factory(Color::class)->create();
        $size = factory(Size::class)->create();

        $this->actingAs($this->admin);
        $testable = Livewire::test(CreateProduct::class)
            ->set('sku', 'SKU_TEST')
            ->set('name', 'Foo')
            ->set('price', 'not-price')
            ->set('quantity', '3')
            ->set('color_id', $color->id)
            ->set('category_id', $category->id)
            ->set('size_id', $size->id)
            ->set('weight', '120')
            ->call('handle')
            ->assertHasErrors(['price' => 'numeric']);
    }

    /**
     * @test
     * @covers \CreateProduct::handle
     */
    public function price_must_be_positive()
    {
        $category = factory(Category::class)->create();
        $color = factory(Color::class)->create();
        $size = factory(Size::class)->create();

        $this->actingAs($this->admin);
        $testable = Livewire::test(CreateProduct::class)
            ->set('sku', 'SKU_TEST')
            ->set('name', 'Foo')
            ->set('price', '-10')
            ->set('quantity', '3')
            ->set('color_id', $color->id)
            ->set('category_id', $category->id)
            ->set('size_id', $size->id)
            ->set('weight', '120')
            ->call('handle')
            ->assertHasErrors(['price' => 'min']);
    }

    /**
     * @test
     * @covers \CreateProduct::handle
     */
    public function category_id_is_required()
    {
        $category = factory(Category::class)->create();
        $color = factory(Color::class)->create();
        $size = factory(Size::class)->create();

        $this->actingAs($this->admin);
        $testable = Livewire::test(CreateProduct::class)
            ->set('sku', 'SKU_TEST')
            ->set('name', 'Foo')
            ->set('price', '10')
            ->set('quantity', '3')
            ->set('color_id', $color->id)
            ->set('category_id', '')
            ->set('size_id', $size->id)
            ->set('weight', '120')
            ->call('handle')
            ->assertHasErrors(['category_id' => 'required']);
    }

    /**
     * @test
     * @covers \CreateProduct::handle
     */
    public function category_id_must_be_existing_category_id()
    {
        $category = factory(Category::class)->create();
        $color = factory(Color::class)->create();
        $size = factory(Size::class)->create();

        $this->actingAs($this->admin);
        $testable = Livewire::test(CreateProduct::class)
            ->set('sku', 'SKU_TEST')
            ->set('name', 'Foo')
            ->set('price', '10')
            ->set('quantity', '3')
            ->set('color_id', $color->id)
            ->set('category_id', '10')
            ->set('size_id', $size->id)
            ->set('weight', '120')
            ->call('handle')
            ->assertHasErrors(['category_id' => 'exists']);
    }

    /**
     * @test
     * @covers \CreateProduct::handle
     */
    public function category_id_must_be_existing_color_id()
    {
        $category = factory(Category::class)->create();
        $color = factory(Color::class)->create();
        $size = factory(Size::class)->create();

        $this->actingAs($this->admin);
        $testable = Livewire::test(CreateProduct::class)
            ->set('sku', 'SKU_TEST')
            ->set('name', 'Foo')
            ->set('price', '10')
            ->set('quantity', '3')
            ->set('color_id', '1000')
            ->set('category_id', $category->id)
            ->set('size_id', $size->id)
            ->set('weight', '120')
            ->call('handle')
            ->assertHasErrors(['color_id' => 'exists']);
    }

    /**
     * @test
     * @covers \CreateProduct::handle
     */
    public function size_id_must_be_existing_size_id()
    {
        $category = factory(Category::class)->create();
        $color = factory(Color::class)->create();
        $size = factory(Size::class)->create();

        $this->actingAs($this->admin);
        $testable = Livewire::test(CreateProduct::class)
            ->set('sku', 'SKU_TEST')
            ->set('name', 'Foo')
            ->set('price', '10')
            ->set('quantity', '3')
            ->set('color_id', $color->id)
            ->set('category_id', $category->id)
            ->set('size_id', '1000')
            ->set('weight', '120')
            ->call('handle')
            ->assertHasErrors(['size_id' => 'exists']);
    }

    /**
     * @test
     * @covers \CreateProduct::handle
     */
    public function weight_must_be_numeric()
    {
        $category = factory(Category::class)->create();
        $color = factory(Color::class)->create();
        $size = factory(Size::class)->create();

        $this->actingAs($this->admin);
        $testable = Livewire::test(CreateProduct::class)
            ->set('sku', 'SKU_TEST')
            ->set('name', 'Foo')
            ->set('price', '10')
            ->set('quantity', '3')
            ->set('color_id', $color->id)
            ->set('category_id', $category->id)
            ->set('size_id', $size->id)
            ->set('weight', 'some')
            ->call('handle')
            ->assertHasErrors(['weight' => 'numeric']);
    }

    /**
     * @test
     * @covers \CreateProduct::handle
     */
    public function weight_must_be_postive()
    {
        $category = factory(Category::class)->create();
        $color = factory(Color::class)->create();
        $size = factory(Size::class)->create();

        $this->actingAs($this->admin);
        $testable = Livewire::test(CreateProduct::class)
            ->set('sku', 'SKU_TEST')
            ->set('name', 'Foo')
            ->set('price', '10')
            ->set('quantity', '3')
            ->set('color_id', $color->id)
            ->set('category_id', $category->id)
            ->set('size_id', $size->id)
            ->set('weight', '0')
            ->call('handle')
            ->assertHasErrors(['weight' => 'min']);
    }

    /**
     * @test
     */
    public function it_doesnt_provide_with_top_level_categories_to_select_from()
    {
        $parent = factory(Category::class)->create(['parent_id' => null]);
        $category = factory(Category::class)->create(['parent_id' => $parent->id]);

        Livewire::test('admin.products.create-product')
            ->assertDontSee($parent->id)
            ->assertSee($category->id);
    }
}
