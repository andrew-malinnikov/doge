<?php

namespace Tests\Http\Livewire\Admin\Products;

use App\Http\Livewire\Admin\Products\ShowProduct;
use App\Store\Color\Color;
use App\Store\Product\Photo\Photo;
use App\Store\Product\Product;
use App\Store\Product\ProductInStock;
use App\Store\Product\Size;
use Facades\Tests\Factories\UserFactory;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Storage;
use Livewire\Livewire;
use Tests\TestCase;

class ShowProductTest extends TestCase
{
    use DatabaseTransactions;
    use WithFaker;

    protected function setUp(): void
    {
        parent::setUp();
        $this->admin = UserFactory::asAdmin()->create();
    }

    /**
     * @test
     */
    public function it_is_loads_livewire_component_for_admin()
    {
        $product = factory(Product::class)->create();

        $this->actingAs($this->admin)
            ->get(route('admin.products.show', $product))
            ->assertSeeLivewire('admin.products.show-product');
    }

    /**
     * @test
     */
    public function it_is_not_accessible_for_non_logged_in_user()
    {
        $product = factory(Product::class)->create();

        $this->get(route('admin.products.show', $product))
            ->assertRedirect('login');
    }

    /**
     * @test
     * @covers \ShowProduct::handle
     */
    public function it_publishes_the_product()
    {
        $product = factory(Product::class)->create([
            'published_at' => null,
        ]);

        Livewire::test(ShowProduct::class, ['product' => $product])
            ->call('publish')
            ->assertEmitted('inlineFlashMessage', 'published');

        $product->refresh();
        $this->assertTrue($product->isPublished());
    }

    /**
     * @test
     * @covers \ShowProduct::handle
     */
    public function it_unpublishes_the_product()
    {
        $product = factory(Product::class)->create([
            'published_at' => now(),
        ]);

        Livewire::test(ShowProduct::class, ['product' => $product])
            ->call('unpublish')
            ->assertEmitted('inlineFlashMessage', 'unpublished');

        $product->refresh();
        $this->assertFalse($product->isPublished());
    }

    // /**
    //  * @test
    //  * @covers
    //  */
    // public function it_cant_publish_if_product_is_publisedh()
    // {
    //     $product = factory(Product::class)->create([
    //         'published_at' => now(),
    //     ]);

    //     Livewire::test(ShowProduct::class, ['product' => $product])
    //         ->assertDontSee('wire:click.prevent="publish"');
    // }

    // /**
    //  * @test
    //  * @covers
    //  */
    // public function it_cant_unpublish_if_product_is_already_unpublished()
    // {
    //     $product = factory(Product::class)->create([
    //         'published_at' => null,
    //     ]);

    //     Livewire::test(ShowProduct::class, ['product' => $product])
    //         ->assertDontSee('Unpublish');
    // }

    /**
     * @test
     */
    public function it_displays_current_stock()
    {
        $product = factory(Product::class)->create();
        factory(ProductInStock::class)->create([
            'product_id' => $product->id,
            'quantity' => 30,
            'size_id' => null,
        ]);

        Livewire::test(ShowProduct::class, ['product' => $product])
            ->assertSee('quantity')
            ->assertSee(30);
    }

    /**
     * @test
     */
    public function it_adds_product_to_stock()
    {
        $size = factory(Size::class)->create();
        $product = factory(Product::class)->create();

        Livewire::test(ShowProduct::class, ['product' => $product])
            ->set('new_stock_quantity', '4')
            ->set('new_stock_size_id', $size->id)
            ->call('addToStock')
            ->assertEmitted('hideModal', 'addToStock');

        $this->assertEquals(1, $product->stock()->count());
    }

    /**
     * @test
     */
    public function it_cant_add_to_stock_without_quantity()
    {
        $size = factory(Size::class)->create();
        $product = factory(Product::class)->create();

        Livewire::actingAs($this->admin)
            ->test(ShowProduct::class, ['product' => $product])
            ->set('new_stock_quantity', '')
            ->set('new_stock_size_id', $size->id)
            ->call('addToStock')
            ->assertHasErrors(['new_stock_quantity' => 'required']);

        $this->assertEquals(0, $product->stock()->count());
    }

    /**
     * @test
     */
    public function quantity_must_be_valid()
    {
        $size = factory(Size::class)->create();
        $product = factory(Product::class)->create();

        Livewire::actingAs($this->admin)
            ->test(ShowProduct::class, ['product' => $product])
            ->set('new_stock_quantity', 'soie')
            ->set('new_stock_size_id', $size->id)
            ->call('addToStock')
            ->assertHasErrors(['new_stock_quantity' => 'numeric']);

        $this->assertEquals(0, $product->stock()->count());
    }

    /**
     * @test
     */
    public function quantity_must_be_positive()
    {
        $size = factory(Size::class)->create();
        $product = factory(Product::class)->create();

        Livewire::actingAs($this->admin)
            ->test(ShowProduct::class, ['product' => $product])
            ->set('new_stock_quantity', '-2')
            ->set('new_stock_size_id', $size->id)
            ->call('addToStock')
            ->assertHasErrors(['new_stock_quantity' => 'min']);

        $this->assertEquals(0, $product->stock()->count());
    }

    /**
     * @test
     */
    public function quantity_must_be_integer()
    {
        $size = factory(Size::class)->create();
        $product = factory(Product::class)->create();

        Livewire::actingAs($this->admin)
            ->test(ShowProduct::class, ['product' => $product])
            ->set('new_stock_quantity', '2.3')
            ->set('new_stock_size_id', $size->id)
            ->call('addToStock')
            ->assertHasErrors(['new_stock_quantity' => 'integer']);

        $this->assertEquals(0, $product->stock()->count());
    }

    /**
     * @test
     */
    public function size_id_must_be_valid()
    {
        $size = factory(Size::class)->create();
        $product = factory(Product::class)->create();

        Livewire::actingAs($this->admin)
            ->test(ShowProduct::class, ['product' => $product])
            ->set('new_stock_quantity', '2')
            ->set('new_stock_size_id', 'not-a-size')
            ->call('addToStock')
            ->assertHasErrors(['new_stock_size_id' => 'exists']);

        $this->assertEquals(0, $product->stock()->count());
    }

    /**
     * @test
     */
    public function it_updates_product_details()
    {
        $color = factory(Color::class)->create();

        $product = factory(Product::class)->create([
            'name' => 'foo',
            'price' => 500,
            'color_id' => factory(Color::class),
            'weight' => 400,
            'gender' => 1,
        ]);

        Livewire::test(ShowProduct::class, ['product' => $product])
            ->fill([
                'new_name' => 'updated',
                'new_price' => '1000',
                'new_color_id' => $color->id,
                'new_weight' => '2000',
                'new_gender' => '2',
            ])
            ->call('updateDetails');

        $product->refresh();
        $this->assertEquals('updated', $product->name);
        $this->assertEquals(1000 * 100, $product->price);
        $this->assertEquals($color->id, $product->color_id);
        $this->assertEquals(2000, $product->weight);
        $this->assertEquals(2, $product->gender);
    }

    /**
     * @test
     * @covers \ShowProduct::uploadPhotos
     */
    public function it_uploads_product_photos()
    {
        Storage::fake('public');
        $product = factory(Product::class)->create();
        $file1 = UploadedFile::fake()->image('photo1.jpg', 30, 20);
        $encoded = base64_encode(file_get_contents($file1));

        $test = Livewire::test(ShowProduct::class, ['product' => $product])
            ->call('fileSelected', $encoded);

        $this->assertEquals(1, $product->photos()->count());
        $this->assertEquals(1, Photo::count());
        Storage::disk('public')->assertExists($file1Path = $product->photos()->first()->path);
    }

    // /**
    //  * @test
    //  * @covers \ShowProduct::uploadPhotos
    //  */
    // public function square_files_are_not_valid()
    // {
    //     Storage::fake('public');
    //     $product = factory(Product::class)->create();
    //     $file1 = UploadedFile::fake()->image('photo1.jpg', 20, 20);
    //     $encoded = base64_encode(file_get_contents($file1));

    //     $test = Livewire::test(ShowProduct::class, ['product' => $product])
    //         ->call('fileSelected', $encoded)
    //         ->assertHasErrors(['file']);

    //     $this->assertEquals(0, Photo::count());
    // }

    /**
     * @test
     */
    public function name_is_required_to_update_product()
    {
        $color = factory(Color::class)->create();

        $product = factory(Product::class)->create([
            'name' => 'foo',
            'price' => 500,
            'color_id' => factory(Color::class),
            'weight' => 400,
        ]);

        Livewire::test(ShowProduct::class, ['product' => $product])
            ->fill([
                'new_name' => '',
                'new_price' => '1000',
                'new_color_id' => $color->id,
                'new_weight' => '2000',
            ])
            ->call('updateDetails')
            ->assertHasErrors(['new_name' => 'required']);

        $product->refresh();
        $this->assertEquals('foo', $product->name);
    }

    /**
     * @test
     */
    public function price_is_required_to_update_product()
    {
        $color = factory(Color::class)->create();

        $product = factory(Product::class)->create([
            'name' => 'foo',
            'price' => 500,
            'color_id' => factory(Color::class),
            'weight' => 400,
        ]);

        Livewire::test(ShowProduct::class, ['product' => $product])
            ->fill([
                'new_name' => 'new name',
                'new_price' => '',
                'new_color_id' => $color->id,
                'new_weight' => '2000',
            ])
            ->call('updateDetails')
            ->assertHasErrors(['new_price' => 'required']);

        $product->refresh();
        $this->assertEquals('foo', $product->name);
    }

    /**
     * @test
     */
    public function price_must_be_numeric()
    {
        $color = factory(Color::class)->create();

        $product = factory(Product::class)->create([
            'name' => 'foo',
            'price' => 500,
            'color_id' => factory(Color::class),
            'weight' => 400,
        ]);

        Livewire::test(ShowProduct::class, ['product' => $product])
            ->fill([
                'new_name' => 'new name',
                'new_price' => 'not-price',
                'new_color_id' => $color->id,
                'new_weight' => '2000',
            ])
            ->call('updateDetails')
            ->assertHasErrors(['new_price' => ['numeric']]);

        $product->refresh();
        $this->assertEquals('foo', $product->name);
    }

    /**
     * @test
     */
    public function price_must_be_positive()
    {
        $color = factory(Color::class)->create();

        $product = factory(Product::class)->create([
            'name' => 'foo',
            'price' => 500,
            'color_id' => factory(Color::class),
            'weight' => 400,
        ]);

        Livewire::test(ShowProduct::class, ['product' => $product])
            ->fill([
                'new_name' => 'new name',
                'new_price' => '-2',
                'new_color_id' => $color->id,
                'new_weight' => '2000',
            ])
            ->call('updateDetails')
            ->assertHasErrors(['new_price' => ['min']]);

        $product->refresh();
        $this->assertEquals('foo', $product->name);
    }

    /**
     * @test
     */
    public function weight_is_must_be_postitive_integer_to_update_product()
    {
        $color = factory(Color::class)->create();

        $product = factory(Product::class)->create([
            'name' => 'foo',
            'price' => 500,
            'color_id' => factory(Color::class),
            'weight' => 400,
        ]);

        Livewire::test(ShowProduct::class, ['product' => $product])
            ->fill([
                'new_name' => 'new name',
                'new_price' => '2',
                'new_color_id' => $color->id,
                'new_weight' => '-33.3',
            ])
            ->call('updateDetails')
            ->assertHasErrors(['new_weight' => ['integer', 'min']]);

        $product->refresh();
        $this->assertEquals('foo', $product->name);
    }

    /**
     * @test
     */
    public function color_must_be_valid()
    {
        $color = factory(Color::class)->create();

        $product = factory(Product::class)->create([
            'name' => 'foo',
            'price' => 500,
            'color_id' => factory(Color::class),
            'weight' => 400,
        ]);

        Livewire::test(ShowProduct::class, ['product' => $product])
            ->fill([
                'new_name' => 'new name',
                'new_price' => '2',
                'new_color_id' => '123',
                'new_weight' => '33',
            ])
            ->call('updateDetails')
            ->assertHasErrors(['new_color_id' => 'exists']);

        $product->refresh();
        $this->assertEquals('foo', $product->name);
    }
}
