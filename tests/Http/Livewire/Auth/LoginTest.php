<?php

use App\Http\Livewire\Auth\Login;
use App\Users\User;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Livewire\Livewire;
use Tests\TestCase;

class LoginTest extends TestCase
{
    use DatabaseTransactions;

    /**
     * @test
     * @covers \Login::handle
     */
    public function it_loggs_user_in()
    {
        $user = factory(User::class)->create([
            'email' => 'andrew@gmail.com',
            'password' => bcrypt('secret'),
        ]);

        Livewire::test(Login::class)
            ->set('email', 'andrew@gmail.com')
            ->set('password', 'secret')
            ->call('handle')
            ->assertRedirect(route('admin.dashboard'));

        $this->assertTrue(auth()->check());
    }

    /**
     * @test
     * @covers \Login::handle
     */
    public function it_doesnt_log_in_with_invalid_credentials()
    {
        $user = factory(User::class)->create([
            'email' => 'andrew@gmail.com',
            'password' => bcrypt('secret'),
        ]);

        $component = Livewire::test(Login::class)
            ->set('email', 'andrew@gmail.com')
            ->set('password', 'wrong')
            ->call('handle');

        $this->assertFalse($component->payload['redirectTo']);
        $this->assertFalse(auth()->check());
    }
}
