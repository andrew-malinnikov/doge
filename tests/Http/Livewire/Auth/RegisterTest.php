<?php

namespace Tests\Http\Livewire\Auth;

use App\Http\Livewire\Auth\Register;
use App\Users\User;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Support\Facades\Hash;
use Livewire\Livewire;
use Tests\TestCase;

class RegisterTest extends TestCase
{
    use DatabaseTransactions;

    /**
     * @test
     * @covers \Register::handle
     */
    public function it_registers_a_user()
    {
        $registrer = Livewire::test(Register::class)
            ->set('name', 'Jackson')
            ->set('email', 'jackson@example.com')
            ->set('password', 'secret')
            ->set('password_confirmation', 'secret')
            ->call('handle');

        $this->assertTrue(auth()->check());
        $this->assertEquals(1, User::count());
        tap(User::first(), function ($user) {
            $this->assertEquals('Jackson', $user->name);
            $this->assertEquals('jackson@example.com', $user->email);
            $this->assertTrue(Hash::check('secret', $user->password));
            $this->assertEquals(auth()->id(), $user->id);
        });

        $registrer->assertRedirect(route('admin.dashboard'));
    }
}
