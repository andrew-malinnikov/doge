<?php

use App\Http\Livewire\Store\Cart;
use App\Store\Cart\CartRepository;
use App\Store\Cart\ProductInCart;
use App\Store\Product\Product;
use App\Store\Product\ProductInStock;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Livewire\Livewire;
use Tests\TestCase;

class CartTest extends TestCase
{
    use DatabaseTransactions;

    /**
     * @test
     * @covers \Cart::increase
     */
    public function it_increases_item_qty()
    {
        $cart = resolve(CartRepository::class)->resolve();
        $product = factory(Product::class)->create();
        factory(ProductInCart::class)->create([
            'product_id' => $product->id,
            'cart_id' => $cart->id,
            'quantity' => 3,
            'size_id' => null,
        ]);
        factory(ProductInStock::class)->create([
            'product_id' => $product->id,
            'quantity' => 4,
            'size_id' => null,
        ]);

        Livewire::test(Cart::class)
            ->call('increase', $product->id, $sizeId = null);

        $this->assertEquals(1, $cart->fresh()->getItems()->count());
        $this->assertEquals(4, $cart->fresh()->getItems()->first()->quantity);
    }

    /**
     * @test
     * @covers \Cart::increase
     */
    public function it_cant_increase_qty_if_there_is_no_more_in_stock()
    {
        $cart = resolve(CartRepository::class)->resolve();
        $product = factory(Product::class)->create();
        factory(ProductInCart::class)->create([
            'product_id' => $product->id,
            'cart_id' => $cart->id,
            'quantity' => 2,
        ]);
        factory(ProductInStock::class)->create([
            'product_id' => $product->id,
            'quantity' => 2,
        ]);

        Livewire::test(Cart::class)
            ->call('increase', $product->id, $sizeId = null);

        $this->assertEquals(1, $cart->fresh()->getItems()->count());
        $this->assertEquals(2, $cart->fresh()->getItems()->first()->quantity);
    }

    /**
     * @test
     */
    public function it_decreases_the_item_qty()
    {
        $cart = resolve(CartRepository::class)->resolve();
        $product = factory(Product::class)->create();
        factory(ProductInCart::class)->create([
            'product_id' => $product->id,
            'cart_id' => $cart->id,
            'quantity' => 3,
            'size_id' => null,
        ]);

        Livewire::test(Cart::class)
            ->call('decrease', $product->id, $sizeId = null);

        $this->assertEquals(1, $cart->fresh()->getItems()->count());
        $this->assertEquals(2, $cart->fresh()->getItems()->first()->quantity);
    }

    /**
     * @test
     */
    public function it_drops_a_cart_item()
    {
        $cart = resolve(CartRepository::class)->resolve();
        $product = factory(Product::class)->create();
        factory(ProductInCart::class)->create([
            'product_id' => $product->id,
            'cart_id' => $cart->id,
            'quantity' => 3,
            'size_id' => null,
        ]);

        Livewire::test(Cart::class)
            ->call('drop', $product->id, $sizeId = null);

        $this->assertEquals(0, $cart->fresh()->getItems()->count());
    }
}
