<?php

use App\Store\Product\Category;
use App\Store\Product\Product;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Livewire\Livewire;
use Tests\TestCase;

class CategoryItemTest extends TestCase
{
    use DatabaseTransactions;

    /**
     * @test
     * @covers \CategoryItem::render
     */
    public function it_loads_products_for_the_category()
    {
        $category = factory(Category::class)->create();
        $product = factory(Product::class)->create(['category_id' => $category->id]);

        Livewire::test('store.categories.category-item', ['category' => $category])
            ->assertSee($product->name);
    }

    /**
     * @test
     * @covers \CategoryItem::render
     */
    public function it_doesnt_load_unpublished_products()
    {
        $category = factory(Category::class)->create();
        $product = factory(Product::class)->create([
            'category_id' => $category->id,
            'published_at' => null,
        ]);

        Livewire::test('store.categories.category-item', ['category' => $category])
            ->assertDontSee($product->name);
    }

    /**
     * @test
     * @covers \CategoryItem::render
     */
    public function it_loads_products_based_on_the_selected_gender_preference()
    {
        $category = factory(Category::class)->create();
        $product = factory(Product::class)->create([
            'category_id' => $category->id,
            'gender' => 1,
            'name' => 'Носки'
        ]);
        $product2 = factory(Product::class)->create([
            'category_id' => $category->id,
            'gender' => 2,
            'name' => 'Одеяло'
        ]);

        Livewire::test(
            'store.categories.category-item',
            ['category' => $category, 'selectedGender' => null]
        )
            ->assertSee($product->name)
            ->assertSee($product2->name)
            ->set('selectedGender', 2)
            ->assertDontSee($product->name)
            ->assertSee($product2->name);
    }

    /**
     * @test
     * @covers \CategoryItem::render
     */
    public function it_loads_products_with_no_gender_preferences_always()
    {
        $category = factory(Category::class)->create();
        $product = factory(Product::class)->create([
            'category_id' => $category->id,
            'gender' => null,
        ]);
        $product2 = factory(Product::class)->create([
            'category_id' => $category->id,
            'gender' => 2,
        ]);

        Livewire::test(
            'store.categories.category-item',
            ['category' => $category, 'selectedGender' => null]
        )
            ->assertSee($product->name)
            ->assertSee($product2->name)
            ->set('selectedGender', 2)
            ->assertSee($product->name)
            ->assertSee($product2->name)
            ->set('selectedGender', 1)
            ->assertSee($product->name)
            ->assertDontSee($product2->name);
    }
}
