<?php

namespace Tests\Http\Livewire\Store;

use App\Sales\Order\Order;
use App\Store\Cart\CartRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Support\Facades\Event;
use Livewire\Livewire;
use Tests\TestCase;

class CheckoutTest extends TestCase
{
    use DatabaseTransactions;

    /**
     * @test
     * @covers \Checkout::submit
     */
    public function it_places_a_new_order()
    {
        Event::fake();
        $this->seed('TestProductInCartAndInStockSeeder');

        $checkout = Livewire::test('store.checkout')
            ->fill([
                'email' => 'jack@email.com',
                'phone' => '89778781010',
                'name' => 'Jack Otwell',
                'line_1' => 'Тверская 10, кв 50',
                'city' => 'Москва',
                'country' => 'Россия',
                'zip_code' => '640000',
            ])
            ->call('submit');

        $this->assertEquals(1, Order::count());
        $order = Order::first();
        $checkout->assertRedirect(route('order-confirmation', ['orderNumber' => $order->number]));
        $this->assertNull(resolve(CartRepository::class)->resolveFromSession());
    }

    /**
     * @test
     * @covers \Checkout::submit
     */
    public function it_validates_the_stock_before_placing_a_new_order()
    {
        Event::fake();
        $this->seed('TestProductInCartSeeder');

        $checkout = Livewire::test('store.checkout')
            ->fill([
                'email' => 'jack@email.com',
                'phone' => '89778781010',
                'name' => 'Jack Otwell',
                'line_1' => 'Тверская 10, кв 50',
                'city' => 'Москва',
                'country' => 'Россия',
                'zip_code' => '640000',
            ])
            ->call('submit')
            ->assertHasErrors('cart');

        $this->assertEquals(0, Order::count());
    }

    /**
     * @test
     * @covers \Checkout::submit
     */
    public function cart_is_required_to_place_an_order()
    {
        Livewire::test('store.checkout')
            ->fill([
                'email' => 'the@email.com',
                'phone' => '89778781010',
                'line_1' => 'Тверская 10, кв 50',
                'city' => 'Москва',
                'country' => 'Россия',
                'zip_code' => '640000',
            ])
            ->call('submit')
            ->assertHasErrors(['cart']);

        $this->assertEquals(0, Order::count());
    }

    /**
     * @test
     * @covers \Checkout::submit
     */
    public function email_is_required()
    {
        $this->seed('TestProductInCartSeeder');

        Livewire::test('store.checkout')
            ->fill([
                'email' => '',
                'phone' => '89778781010',
                'line_1' => 'Тверская 10, кв 50',
                'city' => 'Москва',
                'country' => 'Россия',
                'zip_code' => '640000',
            ])
            ->call('submit')
            ->assertHasErrors(['email' => ['required']]);

        $this->assertEquals(0, Order::count());
    }

    /**
     * @test
     * @covers \Checkout::submit
     */
    public function email_must_be_valid()
    {
        $this->seed('TestProductInCartSeeder');

        Livewire::test('store.checkout')
            ->fill([
                'email' => 'not-email',
                'phone' => '89778781010',
                'line_1' => 'Тверская 10, кв 50',
                'city' => 'Москва',
                'country' => 'Россия',
                'zip_code' => '640000',
            ])
            ->call('submit')
            ->assertHasErrors(['email' => ['email']]);

        $this->assertEquals(0, Order::count());
    }

    /**
     * @test
     * @covers \Checkout::submit
     */
    public function line_1_field_is_required()
    {
        $this->seed('TestProductInCartSeeder');

        Livewire::test('store.checkout')
            ->fill([
                'email' => 'jack@email.com',
                'phone' => '89778781010',
                'line_1' => '',
                'city' => 'Москва',
                'country' => 'Россия',
                'zip_code' => '640000',
            ])
            ->call('submit')
            ->assertHasErrors(['line_1' => ['required']]);

        $this->assertEquals(0, Order::count());
    }

    /**
     * @test
     * @covers \Checkout::submit
     */
    public function city_field_is_required()
    {
        $this->seed('TestProductInCartSeeder');

        Livewire::test('store.checkout')
            ->fill([
                'email' => 'jack@email.com',
                'phone' => '89778781010',
                'line_1' => 'some street',
                'city' => '',
                'country' => 'Россия',
                'zip_code' => '640000',
            ])
            ->call('submit')
            ->assertHasErrors(['city' => ['required']]);

        $this->assertEquals(0, Order::count());
    }

    /**
     * @test
     * @covers \Checkout::submit
     */
    public function country_field_is_required()
    {
        $this->seed('TestProductInCartSeeder');

        Livewire::test('store.checkout')
            ->fill([
                'email' => 'jack@email.com',
                'phone' => '89778781010',
                'line_1' => 'some street',
                'city' => 'cit',
                'country' => '',
                'zip_code' => '640000',
            ])
            ->call('submit')
            ->assertHasErrors(['country' => ['required']]);

        $this->assertEquals(0, Order::count());
    }

    /**
     * @test
     * @covers \Checkout::submit
     */
    public function zip_code_field_is_required()
    {
        $this->seed('TestProductInCartSeeder');

        Livewire::test('store.checkout')
            ->fill([
                'email' => 'jack@email.com',
                'phone' => '89778781010',
                'line_1' => 'some street',
                'city' => 'cit',
                'country' => 'Russia',
                'zip_code' => '',
            ])
            ->call('submit')
            ->assertHasErrors(['zip_code' => ['required']]);

        $this->assertEquals(0, Order::count());
    }
}
