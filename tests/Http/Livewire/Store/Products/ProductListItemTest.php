<?php

namespace Tests\Http\Livewire\Store\Products;

use App\Store\Cart\Cart;
use App\Store\Cart\CartRepository;
use App\Store\Product\Category;
use App\Store\Product\Product;
use App\Store\Product\ProductInStock;
use App\Store\Product\Size;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Livewire\Livewire;
use Tests\TestCase;

class ProductListItemTest extends TestCase
{
    use DatabaseTransactions;

    /** @test */
    public function it_loads_the_product()
    {
        $product = factory(Product::class)->create();

        Livewire::test('store.products.product-list-item', ['product' => $product])
            ->assertSee($product->name);
    }

    /**
     * @test
     */
    public function it_displays_available_sizes_in_stock()
    {
        $category = factory(Category::class)->create(['size_type' => Size::TYPE_DIGIT]);
        $product = factory(Product::class)->create(['category_id' => $category->id]);
        $size = factory(Size::class)->create(['value' => 'XL']);
        $size2 = factory(Size::class)->create(['value' => 'M']);
        factory(ProductInStock::class)->create([
            'size_id' => $size->id,
            'quantity' => 10,
            'product_id' => $product->id,
        ]);
        factory(ProductInStock::class)->create([
            'size_id' => $size2->id,
            'quantity' => 10,
            'product_id' => $product->id,
        ]);

        Livewire::test('store.products.product-list-item', ['product' => $product])
            ->assertSee('M')
            ->assertSee('XL');
    }

    /**
     * @test
     */
    public function it_sets_selected_size()
    {
        $product = factory(Product::class)->create();
        $size = factory(Size::class)->create();

        Livewire::test('store.products.product-list-item', ['product' => $product])
            ->call('toggleSelectedSize', $size->id)
            ->assertSet('selectedSizeId', $size->id)
            ->call('toggleSelectedSize', $size->id)
            ->assertSet('selectedSizeId', null)
            ->call('toggleSelectedSize', $size->id)
            ->assertSet('selectedSizeId', $size->id);
    }

    /** @test */
    public function it_creates_a_cart_and_adds_an_item_to_it()
    {
        $product = factory(Product::class)->create();
        $size = factory(Size::class)->create();
        factory(ProductInStock::class)->create([
            'size_id' => $size->id,
            'product_id' => $product->id,
            'quantity' => 3,
        ]);

        $test = Livewire::test('store.products.product-list-item', ['product' => $product])
            ->set('selectedSizeId', $size->id)
            ->call('addToCart')
            ->assertEmitted('cartStateChanged');

        $this->assertTrue(session()->has('cart_id'));
        $this->assertEquals(1, Cart::count());
        $cart = Cart::first();
        $this->assertEquals($cart->id, session('cart_id'));
        $this->assertEquals(1, $cart->items()->count());
        $cartItem = $cart->items->first();
        $this->assertEquals($product->id, $cartItem->pivot->product_id);
        $this->assertEquals(1, $cartItem->pivot->quantity);
        $this->assertEquals($size->id, $cartItem->pivot->size_id);
    }

    /** @test */
    public function it_uses_existing_cart_in_session_to_add_item_to()
    {
        $cart = resolve(CartRepository::class)->resolve();

        $product = factory(Product::class)->create();
        $size = factory(Size::class)->create();
        factory(ProductInStock::class)->create([
            'size_id' => $size->id,
            'product_id' => $product->id,
            'quantity' => 3,
        ]);

        $test = Livewire::test('store.products.product-list-item', ['product' => $product])
            ->set('selectedSizeId', $size->id)
            ->call('addToCart');

        $this->assertTrue(session()->has('cart_id'));
        $this->assertEquals(1, Cart::count());
        $cart = Cart::first();
        $this->assertEquals($cart->id, session('cart_id'));
        $this->assertEquals(1, $cart->items()->count());
        $cartItem = $cart->items->first();
        $this->assertEquals($product->id, $cartItem->pivot->product_id);
        $this->assertEquals(1, $cartItem->pivot->quantity);
        $this->assertEquals($size->id, $cartItem->pivot->size_id);
    }

    /** @test */
    public function it_cant_add_a_sizeable_product_to_cart_without_size_specified()
    {
        $product = factory(Product::class)->create();
        $size = factory(Size::class)->create();
        factory(ProductInStock::class)->create([
            'size_id' => $size->id,
            'product_id' => $product->id,
            'quantity' => 3,
        ]);

        $test = Livewire::test('store.products.product-list-item', ['product' => $product])
            ->set('selectedSizeId', null)
            ->call('addToCart')
            ->assertHasErrors('size');

        $this->assertFalse(session()->has('cart_id'));
        $this->assertEquals(0, Cart::count());
    }
}
