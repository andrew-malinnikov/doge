<?php

namespace Tests\Http\Livewire\Store;

use App\Store\Cart\CartRepository;
use App\Store\Product\Category;
use App\Store\Product\Product;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Livewire\Livewire;
use Tests\TestCase;

class StoreTest extends TestCase
{
    use DatabaseTransactions;

    /**
     * @test
     * @covers \Store::handle
     */
    public function it_loads_livewire_component()
    {
        $this->get(route('store'))
            ->assertSeeLivewire('store.store');
    }

    /**
     * @test
     */
    public function it_loads_a_cart_from_session()
    {
        $cart = resolve(CartRepository::class)->resolve();

        $store = Livewire::test('store.store');

        $this->assertTrue($cart->is($store->viewData('cart')));
    }

    /**
     * @test
     */
    public function it_fetches_all_the_children_categories()
    {
        $parent = factory(Category::class)->create();
        $categoryA = factory(Category::class)->create(['parent_id' => $parent->id]);
        $categoryB = factory(Category::class)->create(['parent_id' => $parent->id]);
        factory(Product::class)->create(['category_id' => $categoryA->id]);
        factory(Product::class)->create(['category_id' => $categoryB->id]);

        Livewire::test('store.store')
            ->assertSee($categoryA->name)
            ->assertSee($categoryB->name);
    }

    /**
     * @test
     */
    public function it_fetches_products_by_a_parent_category()
    {
        $parentA = factory(Category::class)->create();
        $parentB = factory(Category::class)->create();
        $categoryA = factory(Category::class)->create(['name' => 'CategoryA', 'parent_id' => $parentA->id]);
        $categoryB = factory(Category::class)->create(['name' => 'CategoryB', 'parent_id' => $parentB->id]);
        factory(Product::class)->create(['category_id' => $categoryA->id]);
        factory(Product::class)->create(['category_id' => $categoryB->id]);

        Livewire::test('store.store')
            ->assertSee('CategoryA')
            ->assertSee('CategoryB')
            ->set('for', (string) $parentB->slug)
            ->assertSee('CategoryB')
            ->assertDontSee('CategoryA');
    }

    /**
     * @test
     */
    public function it_doesnt_show_top_level_categories()
    {
        $categoryA = factory(Category::class)->create([
            'parent_id' => null,
        ]);
        factory(Product::class)->create(['category_id' => $categoryA->id]);

        $store = Livewire::test('store.store');

        $categories = $store->lastRenderedView->gatherData()['categories'];
        $this->assertTrue($categories->isEmpty());
    }

    /**
     * @test
     */
    public function it_doesnt_show_empty_categories()
    {
        $categoryA = factory(Category::class)->create([
            'parent_id' => factory(Category::class),
            'name' => 'The parent',
        ]);

        Livewire::test('store.store')
            ->assertDontSee('The parent');
    }

    /**
     * @test
     */
    public function it_doesnt_show_unpuslished_products()
    {
        $categoryA = factory(Category::class)->create([
            'parent_id' => factory(Category::class),
        ]);
        factory(Product::class)->create([
            'category_id' => $categoryA->id,
            'published_at' => null,
        ]);

        Livewire::test('store.store')
            ->assertDontSee($categoryA->name);
    }

    /**
     * @test
     */
    public function it_sets_gender_filter_preference()
    {
        Livewire::test('store.store')
            ->call('toggleGender', 1)
            ->assertSet('gender', 1)
            ->call('toggleGender', 1)
            ->assertSet('gender', null)
            ->call('toggleGender', 2)
            ->assertSet('gender', 2)
            ->call('toggleGender', 1)
            ->assertSet('gender', 1);
    }
}
