<?php

namespace Tests;

use Facades\Tests\Factories\UserFactory;

trait RolesConcerns
{
    public function getAdmin()
    {
        return UserFactory::asAdmin()->create();
    }
}
