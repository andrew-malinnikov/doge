<?php

use App\Sales\Order\OrderItem;
use App\Store\Product\Product;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;

class OrderItemTest extends TestCase
{
    use DatabaseTransactions;

    /**
     * @test
     * @covers \OrderItem::handle
     */
    public function it_has_path_to_the_product()
    {
        $product = factory(Product::class)->create();
        $orderItem = factory(OrderItem::class)->create([
            'product_id' => $product->id,
        ]);
        $this->assertEquals(
            route('products.show', [$product]),
            $orderItem->productPath()
        );

        $orderItem = factory(OrderItem::class)->create([
            'product_id' => null,
        ]);
        $this->assertEquals('#', $orderItem->productPath());
    }
}
