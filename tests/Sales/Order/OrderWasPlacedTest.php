<?php

use App\Sales\Order\OrderWasPlaced;
use App\Store\Stock\Restock;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;

class OrderWasPlacedTest extends TestCase
{
    use DatabaseTransactions;

    /**
     * @test
     * @covers \OrderWasPlaced::handle
     */
    public function listeners_are_firing()
    {
        $restock = $this->spy(Restock::class);

        $event = new OrderWasPlaced(1);
        event($event);

        $restock->shouldHaveReceived('handle')->once()->with($event);
    }
}
