<?php

use App\Sales\Order\Order;
use App\Sales\Order\Status\Active;
use App\Sales\Order\Status\Events\OrderWasActivated;
use App\Sales\Order\Status\OrderStatusManager;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Support\Facades\Event;
use Tests\TestCase;

class ActiveTest extends TestCase
{
    use DatabaseTransactions;

    /**
     * @test
     */
    public function it_transitions_an_order_to_active_status()
    {
        Event::fake();
        $order = factory(Order::class)->create(['status' => OrderStatusManager::AWAITING_PAYMENT]);

        (new Active())->transfer($order);

        $this->assertEquals(OrderStatusManager::ACTIVE, $order->status);
        Event::assertDispatched(OrderWasActivated::class, fn ($event) => $event->orderId == $order->id);
    }
}
