<?php

use App\Sales\Order\Order;
use App\Sales\Order\Status\Cancelled;
use App\Sales\Order\Status\Events\OrderWasCancelled;
use App\Sales\Order\Status\OrderStatusManager;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Support\Facades\Event;
use Tests\TestCase;

class CancelledTest extends TestCase
{
    use DatabaseTransactions;

    /**
     * @test
     * @covers \Cancelled::handle
     */
    public function it_updates_orders_status()
    {
        Event::fake();
        $order = factory(Order::class)->create([
            'status' => OrderStatusManager::ACTIVE,
        ]);

        (new Cancelled())->transfer($order);

        $this->assertEquals(OrderStatusManager::CANCELLED, $order->fresh()->status);
        Event::assertDispatched(OrderWasCancelled::class, fn ($event) => $event->orderId == $order->id);
    }
}
