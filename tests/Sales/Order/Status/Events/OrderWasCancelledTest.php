<?php

use App\Sales\Order\Status\Events\OrderWasCancelled;
use App\Store\Stock\ReleaseToStock;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;

class OrderWasCancelledTest extends TestCase
{
    use DatabaseTransactions;

    /**
     * @test
     * @covers \OrderWasCancelled::handle
     */
    public function listeners_are_firing()
    {
        $stock = $this->spy(ReleaseToStock::class);

        $event = new OrderWasCancelled(1);
        event($event);

        $stock->shouldHaveReceived('handle')->once()->with($event);
    }
}
