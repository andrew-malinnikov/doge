<?php

use App\Sales\Order\Status\AwaitingPayment;
use App\Sales\Order\Status\OrderStatusManager;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;

class OrderStatusManagerTest extends TestCase
{
    use DatabaseTransactions;

    /**
     * @test
     * @covers \OrderStatusManager::handle
     */
    public function it_resolves_corresponding_order_status()
    {
        $this->assertInstanceOf(
            AwaitingPayment::class,
            (new OrderStatusManager(OrderStatusManager::AWAITING_PAYMENT))->get()
        );
    }

    /**
     * @test
     * @covers \OrderStatusManager::handle
     */
    public function it_throws_exception_for_invalid_status()
    {
        $this->expectException('InvalidArgumentException');
        new OrderStatusManager('foo');
    }
}
