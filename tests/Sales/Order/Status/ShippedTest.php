<?php

use App\Sales\Order\Order;
use App\Sales\Order\Status\Events\OrderWasShipped;
use App\Sales\Order\Status\OrderStatusManager;
use App\Sales\Order\Status\Shipped;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Support\Facades\Event;
use Tests\TestCase;

class ShippedTest extends TestCase
{
    use DatabaseTransactions;

    /**
     * @test
     * @covers \Shipped::transfer
     */
    public function it_transfers_order_to_shipped_status()
    {
        Event::fake();
        $order = factory(Order::class)->create([
            'status' => OrderStatusManager::ACTIVE,
        ]);

        (new Shipped())->transfer($order, $trackingNumber = 'TEST_TRACKING_123');

        $this->assertEquals(OrderStatusManager::SHIPPED, $order->fresh()->status);
        $this->assertEquals('TEST_TRACKING_123', $order->tracking_number);
        Event::assertDispatched(OrderWasShipped::class, fn ($event) => $event->orderId == $order->id);
    }
}
