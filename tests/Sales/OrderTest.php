<?php

namespace Tests\Sales;

use App\Sales\Order\Address;
use App\Sales\Order\Number\OrderNumber;
use App\Sales\Order\Order;
use App\Sales\Order\OrderWasPlaced;
use App\Sales\Order\PlaceOrderData;
use App\Sales\Order\Status\OrderStatus;
use App\Sales\Order\Status\OrderStatusManager;
use App\Store\Cart\Calculator\CartCalculator;
use App\Store\Cart\Cart;
use App\Store\Cart\CartCost;
use App\Store\Cart\ProductInCart;
use App\Store\Product\Category;
use App\Store\Product\Price;
use App\Store\Product\Product;
use App\Store\Product\Size;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Support\Facades\Event;
use Tests\TestCase;

class OrderTest extends TestCase
{
    use DatabaseTransactions;

    /**
     * @test
     * @covers \Order::handle
     */
    public function it_can_be_placed()
    {
        Event::fake();
        OrderNumber::shouldReceive('generate')->andReturn('TEST_12345');
        $size = factory(Size::class)->create(['value' => 'XL']);
        $category = factory(Category::class)->create(['name' => 'top stuff']);
        $product = factory(Product::class)->create([
            'category_id' => $category->id,
            'name' => 'socks',
        ]);
        $cart = factory(Cart::class)->create();
        factory(ProductInCart::class)->create([
            'cart_id' => $cart->id,
            'product_id' => $product->id,
            'quantity' => 2,
            'size_id' => $size->id,
        ]);
        factory(ProductInCart::class)->create([
            'cart_id' => $cart->id,
            'product_id' => $product2 = factory(Product::class)->create(),
            'quantity' => 3,
            'size_id' => null,
        ]);
        $data = new PlaceOrderData([
            'cart' => $cart,
            'phone' => '123412341',
            'email' => 'jack@email.com',
            'address' => new Address([
                'name' => 'Jackson Otwell',
                'line_1' => 'test street 1 apt 4',
                'city' => 'moscow',
                'country' => 'Russia',
                'zip_code' => '630000',
                'delivery_notes' => 'Leave at the door.',
            ]),
        ]);
        $calculator = $this->mock(CartCalculator::class);
        $calculator->shouldReceive('calculate')->andReturn(new CartCost([
            'itemsCost' => new Price(10000),
            'shippingCost' => new Price(8000),
            'totalCost' => new Price(10000 + 8000),
        ]));

        $order = Order::place($data);

        $this->assertEquals(1, Order::count());
        $this->assertEquals(OrderStatusManager::AWAITING_PAYMENT, $order->status);
        $this->assertEquals('TEST_12345', $order->number);
        $this->assertEquals('jack@email.com', $order->email);
        $this->assertEquals('123412341', $order->phone);
        $this->assertEquals(10000, $order->items_cost);
        $this->assertEquals(8000, $order->shipping_cost);
        $this->assertEquals(10000 + 8000, $order->total_cost);
        $this->assertEquals('Jackson Otwell', $order->shipping_name);
        $this->assertEquals('test street 1 apt 4', $order->shipping_address_line_1);
        $this->assertEquals('moscow', $order->shipping_address_city);
        $this->assertEquals('Russia', $order->shipping_address_country);
        $this->assertEquals('630000', $order->shipping_address_zip_code);
        $this->assertEquals('Leave at the door.', $order->shipping_delivery_notes);
        $this->assertEquals(2, $order->items()->count());
        $orderItems = $order->items;
        $firstItem = $orderItems->shift();
        $this->assertEquals('socks', $firstItem->name);
        $this->assertEquals('XL', $firstItem->size_label);
        $this->assertEquals($size->id, $firstItem->size_id);
        $this->assertEquals(2, $firstItem->quantity);
        $this->assertEquals($product->id, $firstItem->product_id);
        $this->assertEquals($category->id, $firstItem->category_id);

        $secondItem = $orderItems->shift();
        $this->assertEquals($product2->name, $secondItem->name);
        $this->assertEquals(null, $secondItem->size_label);
        $this->assertEquals(null, $secondItem->size_id);
        $this->assertEquals(3, $secondItem->quantity);
        $this->assertEquals($product2->id, $secondItem->product_id);
        $this->assertEquals($product2->category_id, $secondItem->category_id);
        Event::assertDispatched(OrderWasPlaced::class, fn ($event) => $event->orderId == $order->id);
    }

    /**
     * @test
     * @covers \Order::status
     */
    public function it_has_status()
    {
        $order = factory(Order::class)->create([
            'status' => OrderStatusManager::ACTIVE,
        ]);

        $this->assertEquals(OrderStatusManager::ACTIVE, $order->status);
        $this->assertInstanceOf(OrderStatus::class, $order->status());
    }

    /**
     * @test
     * @covers \Order::costs()
     */
    public function it_has_costs()
    {
        $order = factory(Order::class)->create([
            'total_cost' => 10000,
            'shipping_cost' => 500,
            'items_cost' => 2000,
        ]);

        $this->assertInstanceOf(CartCost::class, $order->costs());
        $this->assertEquals(10000, $order->costs()->totalCost->get());
    }
}
