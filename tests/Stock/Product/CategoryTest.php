<?php

use App\Store\Product\Category;
use App\Store\Product\Size;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;

class CategoryTest extends TestCase
{
    use DatabaseTransactions;

    /**
     * @test
     * @covers \Category::handle
     */
    public function it_checks_if_contains_products_that_must_have_size()
    {
        $this->assertTrue((new Category(['size_type' => Size::TYPE_DIGIT]))->isSizeable());

        $this->assertFalse((new Category(['size_type' => null]))->isSizeable());
    }
}
