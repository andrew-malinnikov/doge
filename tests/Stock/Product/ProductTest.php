<?php

namespace Tests\Stock\Product;

use App\Store\Product\Product;
use App\Store\Product\ProductInStock;
use App\Store\Product\Size;
use App\Store\StockData;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;

class ProductTest extends TestCase
{
    use DatabaseTransactions;

    /**
     * @test
     * @covers
     */
    public function it_sets_slug_automatically_based_on_name()
    {
        $product = factory(Product::class)->create(['slug' => null, 'name' => 'Превосходный пирожок']);
        $this->assertEquals('превосходный-пирожок', $product->slug);
    }

    /**
     * @test
     * @covers
     */
    public function it_sets_slug_with_whats_provided()
    {
        $product = factory(Product::class)->create(['slug' => 'this-is-slug', 'name' => 'Превосходный пирожок']);
        $this->assertEquals('this-is-slug', $product->slug);
    }

    /**
     * @test
     * @covers \Product::handle
     */
    public function it_can_be_added_to_stock()
    {
        $product = factory(Product::class)->create(['name' => 'foo']);
        $size = factory(Size::class)->create();

        $productInStock = $product->addToStock(new StockData(['quantity' => 10, 'size' => $size]));

        $this->assertEquals(1, $product->stock()->count());
        $this->assertInstanceOf(ProductInStock::class, $productInStock);
        tap($product->stock()->first(), function ($productInStock) use ($size) {
            $this->assertEquals($size->id, $productInStock->size_id);
            $this->assertEquals(10, $productInStock->quantity);
        });
    }

    /**
     * @test
     * @covers \Product::handle
     */
    public function it_can_be_added_to_stock_without_size()
    {
        $product = factory(Product::class)->create(['name' => 'foo']);

        $productInStock = $product->addToStock(new StockData(['quantity' => 10]));

        $this->assertEquals(1, $product->stock()->count());
        $this->assertInstanceOf(ProductInStock::class, $productInStock);
        tap($product->stock()->first(), function ($productInStock) {
            $this->assertNull($productInStock->size_id);
            $this->assertEquals(10, $productInStock->quantity);
        });
    }

    /**
     * @test
     * @covers \Product::handle
     */
    public function it_increases_its_quantity_if_already_in_stock()
    {
        $product = factory(Product::class)->create(['name' => 'foo']);
        $size = factory(Size::class)->create();
        factory(ProductInStock::class)->create([
            'product_id' => $product->id,
            'size_id' => $size->id,
            'quantity' => 10,
        ]);

        $productInStock = $product->addToStock(new StockData(['quantity' => 5, 'size' => $size]));

        $this->assertEquals(1, $product->stock()->count());
        $this->assertInstanceOf(ProductInStock::class, $productInStock);
        tap($product->stock()->first(), function ($productInStock) use ($size) {
            $this->assertEquals($size->id, $productInStock->size_id);
            $this->assertEquals(15, $productInStock->quantity);
        });
    }

    /**
     * @test
     */
    public function it_decrements_stock()
    {
        $product = factory(Product::class)->create();
        $stock = factory(ProductInStock::class)->create([
            'product_id' => $product->id,
            'quantity' => 3,
            'size_id' => null,
        ]);
        $stockData = new StockData([
            'quantity' => 2,
            'size' => null,
        ]);

        $product->subtractStock($stockData);

        $this->assertEquals(1, $stock->fresh()->quantity);
    }

    /**
     * @test
     */
    public function it_decrements_stock_considering_size()
    {
        $product = factory(Product::class)->create();
        $size = factory(Size::class)->create();
        $stock1 = factory(ProductInStock::class)->create([
            'product_id' => $product->id,
            'quantity' => 3,
            'size_id' => null,
        ]);
        $stock2 = factory(ProductInStock::class)->create([
            'product_id' => $product->id,
            'quantity' => 3,
            'size_id' => $size->id,
        ]);
        $stockData = new StockData([
            'quantity' => 2,
            'size' => $size,
        ]);

        $product->subtractStock($stockData);

        $this->assertEquals(3, $stock1->fresh()->quantity);
        $this->assertEquals(1, $stock2->fresh()->quantity);
    }

    /**
     * @test
     */
    public function it_drops_the_stock_if_new_quantity_is_0()
    {
        $product = factory(Product::class)->create();
        $size = factory(Size::class)->create();
        $stock = factory(ProductInStock::class)->create([
            'product_id' => $product->id,
            'quantity' => 3,
            'size_id' => $size->id,
        ]);
        $stockData = new StockData([
            'quantity' => 3,
            'size' => $size,
        ]);

        $product->subtractStock($stockData);

        $this->assertNull($stock->fresh());
        $this->assertEquals(0, ProductInStock::count());
    }

    /**
     * @test
     * @covers \Product::isOutOfStock
     */
    public function it_checks_if_product_is_completely_out_of_stock()
    {
        $product = factory(Product::class)->create();
        $this->assertTrue($product->isOutOfStock());

        factory(ProductInStock::class)->create([
            'product_id' => $product->id,
            'quantity' => 10,
        ]);
        $this->assertFalse($product->isOutOfStock());
    }

    /**
     * @test
     */
    public function it_can_be_searched_by_a_keyword()
    {
        $product = factory(Product::class)->create([
            'name' => 'Beautiful socks',
        ]);

        $found = Product::search('tiful')->first();

        $this->assertTrue($found->is($product));
    }
}
