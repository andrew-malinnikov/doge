<?php

use App\Store\Cart\Calculator\ItemsCost;
use App\Store\Cart\Cart;
use App\Store\Cart\ProductInCart;
use App\Store\Product\Product;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;

class ItemsCostTest extends TestCase
{
    use DatabaseTransactions;

    /**
     * @test
     * @covers \ItemsCost::handle
     */
    public function it_summs_all_the_cart_items_costs_together()
    {
        $cart = factory(Cart::class)->create();
        $socks = factory(Product::class)->create(['price' => 1000]);
        $hoodie = factory(Product::class)->create(['price' => 2000]);
        factory(ProductInCart::class)->create([
            'cart_id' => $cart->id,
            'product_id' => $socks->id,
            'quantity' => 2,
        ]);
        factory(ProductInCart::class)->create([
            'cart_id' => $cart->id,
            'product_id' => $hoodie->id,
            'quantity' => 3,
        ]);

        $cartCost = (new ItemsCost())->withItems($cart->getItems())->calculate();

        $this->assertEquals(8000, $cartCost->get());
    }
}
