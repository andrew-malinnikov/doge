<?php

namespace Tests\Store\Cart\Calculator;

use App\Store\Cart\Calculator\ShippingCost;
use App\Store\Cart\Cart;
use App\Store\Cart\ProductInCart;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;

class ShippingCostTest extends TestCase
{
    use DatabaseTransactions;

    /**
     * @test
     * @covers \ShippingCost::handle
     */
    public function it_calculates_shipping_cost()
    {
        $cart = factory(Cart::class)->create();
        factory(ProductInCart::class)->create(['cart_id' => $cart->id]);

        $shippingCost = (new ShippingCost())->withItems($cart->getItems())->calculate();

        $this->assertEquals(25000, $shippingCost->get());
    }

    /**
     * @test
     * @covers \ShippingCost::handle
     */
    public function it_is_0_if_there_is_no_items()
    {
        $cart = factory(Cart::class)->create();

        $shippingCost = (new ShippingCost())->withItems($cart->getItems())->calculate();

        $this->assertEquals(0, $shippingCost->get());
    }
}
