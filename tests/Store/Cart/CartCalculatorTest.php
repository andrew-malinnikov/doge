<?php

use App\Store\Cart\Calculator\CartCalculator;
use App\Store\Cart\Calculator\ItemsCost;
use App\Store\Cart\Calculator\ShippingCost;
use App\Store\Cart\Cart;
use App\Store\Cart\CartCost;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;

class CartCalculatorTest extends TestCase
{
    use DatabaseTransactions;

    /**
     * @test
     * @covers \CartCalculator::handle
     */
    public function it_calculates_cost_by_summing_up_component_costs()
    {
        $cart = factory(Cart::class)->create();

        $itemsCost = $this->mock(ItemsCost::class);
        $itemsCost->shouldReceive('withItems')->andReturnSelf()->shouldReceive('calculate')->andReturnSelf();
        $itemsCost->shouldReceive('get')->andReturn(5000);

        $shippingCost = $this->mock(ShippingCost::class);
        $shippingCost->shouldReceive('withItems')->andReturnSelf()->shouldReceive('calculate')->andReturnSelf();
        $shippingCost->shouldReceive('get')->andReturn(1000);

        $costs = (new CartCalculator())->calculate($cart);

        $this->assertInstanceOf(CartCost::class, $costs);
        $this->assertEquals(5000, $costs->itemsCost->get());
        $this->assertEquals(1000, $costs->shippingCost->get());
        $this->assertEquals(6000, $costs->totalCost->get());
    }
}
