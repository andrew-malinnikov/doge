<?php

use App\Store\Cart\CartItem;
use App\Store\Product\Product;
use App\Store\Product\ProductInStock;
use App\Store\Product\Size;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;

class CartItemTest extends TestCase
{
    use DatabaseTransactions;

    /**
     * @test
     * @covers \CartItem::handle
     */
    public function it_checks_if_there_is_the_stock_enough_to_fulfill_it()
    {
        $product = factory(Product::class)->create();
        $stock = factory(ProductInStock::class)->create([
            'product_id' => $product->id,
            'quantity' => 4,
        ]);
        $cartItem = new CartItem([
            'productId' => $product->id,
            'quantity' => 3,
        ]);

        $this->assertTrue($cartItem->isInStock());
    }

    /**
     * @test
     * @covers \CartItem::handle
     */
    public function it_checks_if_there_is_the_stock_enough_to_fulfill_it_for_proper_size()
    {
        $product = factory(Product::class)->create();
        $sizeA = factory(Size::class)->create();
        $sizeB = factory(Size::class)->create();
        $stock = factory(ProductInStock::class)->create([
            'product_id' => $product->id,
            'quantity' => 3,
            'size_id' => $sizeA->id,
        ]);
        $stock = factory(ProductInStock::class)->create([
            'product_id' => $product->id,
            'quantity' => 2,
            'size_id' => $sizeB->id,
        ]);
        $cartItemA = new CartItem([
            'productId' => $product->id,
            'sizeId' => $sizeA->id,
            'quantity' => 3,
        ]);
        $cartItemB = new CartItem([
            'productId' => $product->id,
            'sizeId' => $sizeB->id,
            'quantity' => 3,
        ]);

        $this->assertTrue($cartItemA->isInStock());
        $this->assertFalse($cartItemB->isInStock());
    }

    /**
     * @test
     * @covers \CartItem::cost()
     */
    public function it_calculates_its_cost()
    {
        $product = factory(Product::class)->create(['price' => 120000]);
        $item = new CartItem([
            'productId' => $product->id,
            'quantity' => 3,
        ]);

        $this->assertEquals(120000 * 3, $item->cost()->get());
        $this->assertEquals('3600 ₽', $item->cost()->format());
    }
}
