<?php

namespace Tests\Store\Cart;

use App\Store\Cart\Cart;
use App\Store\Cart\ProductInCart;
use App\Store\Product\Product;
use App\Store\Product\ProductInStock;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;

class CartItemsCollectionTest extends TestCase
{
    use DatabaseTransactions;

    /**
     * @test
     * @covers \CartItemsCollection::isSufficientStock
     */
    public function it_validates_each_of_its_items_is_in_stock()
    {
        $product = factory(Product::class)->create();
        $productB = factory(Product::class)->create();
        $cart = factory(Cart::class)->create();
        factory(ProductInCart::class)->create([
            'cart_id' => $cart->id,
            'product_id' => $product->id,
            'size_id' => null,
            'quantity' => 5,
        ]);
        factory(ProductInCart::class)->create([
            'cart_id' => $cart->id,
            'product_id' => $productB->id,
            'size_id' => null,
            'quantity' => 5,
        ]);
        $stock = factory(ProductInStock::class)->create([
            'product_id' => $product->id,
            'size_id' => null,
            'quantity' => 10,
        ]);
        $stock = factory(ProductInStock::class)->create([
            'product_id' => $productB->id,
            'size_id' => null,
            'quantity' => 10,
        ]);

        $this->assertTrue($cart->getItems()->isSufficientStock());
    }

    /**
     * @test
     * @covers \CartItemsCollection::isSufficientStock
     */
    public function it_is_not_sufficient_stock_if_at_least_one_item_is_out_of_stock()
    {
        $product = factory(Product::class)->create();
        $productB = factory(Product::class)->create();
        $cart = factory(Cart::class)->create();
        factory(ProductInCart::class)->create([
            'cart_id' => $cart->id,
            'product_id' => $product->id,
            'size_id' => null,
            'quantity' => 5,
        ]);
        factory(ProductInCart::class)->create([
            'cart_id' => $cart->id,
            'product_id' => $productB->id,
            'size_id' => null,
            'quantity' => 5,
        ]);
        $stock = factory(ProductInStock::class)->create([
            'product_id' => $product->id,
            'size_id' => null,
            'quantity' => 3,
        ]);
        $stock = factory(ProductInStock::class)->create([
            'product_id' => $productB->id,
            'size_id' => null,
            'quantity' => 10,
        ]);

        $this->assertFalse($cart->getItems()->isSufficientStock());
    }
}
