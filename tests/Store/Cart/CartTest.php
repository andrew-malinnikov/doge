<?php

namespace Tests\Store\Cart;

use App\Store\Cart\Calculator\CartCalculator;
use App\Store\Cart\Cart;
use App\Store\Cart\CartCost;
use App\Store\Cart\CartItem;
use App\Store\Cart\CartItemsCollection;
use App\Store\Cart\NotEnoughItemsInStockException;
use App\Store\Cart\ProductInCart;
use App\Store\Product\Price;
use App\Store\Product\Product;
use App\Store\Product\ProductInStock;
use App\Store\Product\Size;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;

class CartTest extends TestCase
{
    use DatabaseTransactions;

    /**
     * @test
     * @covers \Cart::handle
     */
    public function it_can_add_an_item()
    {
        $product = factory(Product::class)->create();
        $size = factory(Size::class)->create();
        factory(ProductInStock::class)->create([
            'product_id' => $product->id,
            'size_id' => $size->id,
            'quantity' => 10,
        ]);

        $cart = factory(Cart::class)->create();
        $item = new CartItem([
            'productId' => $product->id,
            'quantity' => 3,
            'sizeId' => $size->id,
        ]);

        $cart->addItem($item);

        $this->assertEquals(1, $cart->items()->count());
        $item = $cart->items()->first();
        $this->assertEquals(3, $item->pivot->quantity);
        $this->assertEquals($product->id, $item->pivot->product_id);
        $this->assertEquals($size->id, $item->pivot->size_id);
    }

    /**
     * @test
     * @covers \Cart::handle
     */
    public function it_can_add_another_item_to_existing_one_in_the_cart_then_it_will_increase_qty()
    {
        $product = factory(Product::class)->create();
        $size = factory(Size::class)->create();
        factory(ProductInStock::class)->create([
            'product_id' => $product->id,
            'size_id' => $size->id,
            'quantity' => 10,
        ]);

        $cart = factory(Cart::class)->create();
        factory(ProductInCart::class)->create([
            'cart_id' => $cart->id,
            'product_id' => $product->id,
            'quantity' => 3,
            'size_id' => $size->id,
        ]);

        $item = new CartItem([
            'productId' => $product->id,
            'quantity' => 3,
            'sizeId' => $size->id,
        ]);

        $cart->addItem($item);

        $this->assertEquals(1, $cart->items()->count());
        $item = $cart->items()->first();
        $this->assertEquals(6, $item->pivot->quantity);
        $this->assertEquals($product->id, $item->pivot->product_id);
        $this->assertEquals($size->id, $item->pivot->size_id);
    }

    /**
     * @test
     * @covers \Cart::handle
     */
    public function it_adds_same_product_of_different_size_as_another_item()
    {
        $product = factory(Product::class)->create();
        $size = factory(Size::class)->create();
        $size2 = factory(Size::class)->create();
        factory(ProductInStock::class)->create([
            'product_id' => $product->id,
            'size_id' => $size->id,
            'quantity' => 10,
        ]);
        factory(ProductInStock::class)->create([
            'product_id' => $product->id,
            'size_id' => $size2->id,
            'quantity' => 10,
        ]);

        $cart = factory(Cart::class)->create();
        factory(ProductInCart::class)->create([
            'cart_id' => $cart->id,
            'product_id' => $product->id,
            'quantity' => 3,
            'size_id' => $size->id,
        ]);

        $item = new CartItem([
            'productId' => $product->id,
            'quantity' => 3,
            'sizeId' => $size2->id,
        ]);

        $cart->addItem($item);

        $this->assertEquals(2, $cart->items()->count());
        $items = $cart->items()->get();
        $item = $items->shift();
        $this->assertEquals(3, $item->pivot->quantity);
        $this->assertEquals($product->id, $item->pivot->product_id);
        $this->assertEquals($size->id, $item->pivot->size_id);

        $item = $items->shift();
        $this->assertEquals(3, $item->pivot->quantity);
        $this->assertEquals($product->id, $item->pivot->product_id);
        $this->assertEquals($size2->id, $item->pivot->size_id);
    }

    /**
     * @test
     * @covers \Cart::addItem
     */
    public function it_adds_none_sizeable_product()
    {
        $product = factory(Product::class)->create();
        $cart = factory(Cart::class)->create();
        factory(ProductInStock::class)->create([
            'product_id' => $product->id,
            'quantity' => 10,
        ]);

        $item = new CartItem([
            'productId' => $product->id,
            'quantity' => 3,
        ]);
        $cart->addItem($item);

        $this->assertEquals(1, $cart->items()->count());
        $item = $cart->items()->first();
        $this->assertEquals(3, $item->pivot->quantity);
        $this->assertEquals($product->id, $item->pivot->product_id);
        $this->assertNull($item->pivot->size_id);
    }

    /**
     * @test
     * @covers \Cart::addItem
     */
    public function it_adds_another_none_sizeable_product()
    {
        $product = factory(Product::class)->create();
        factory(ProductInStock::class)->create([
            'product_id' => $product->id,
            'quantity' => 10,
        ]);

        $cart = factory(Cart::class)->create();
        factory(ProductInCart::class)->create([
            'cart_id' => $cart->id,
            'product_id' => $product->id,
            'size_id' => null,
            'quantity' => 3,
        ]);

        $item = new CartItem([
            'productId' => $product->id,
            'quantity' => 5,
        ]);
        $cart->addItem($item);

        $this->assertEquals(1, $cart->items()->count());
        $item = $cart->items()->first();
        $this->assertEquals(8, $item->pivot->quantity);
        $this->assertEquals($product->id, $item->pivot->product_id);
        $this->assertNull($item->pivot->size_id);
    }

    /**
     * @test
     * @covers \Cart::addItem
     */
    public function it_cant_add_an_item_if_there_is_not_enough_in_stock()
    {
        $product = factory(Product::class)->create();
        factory(ProductInStock::class)->create([
            'product_id' => $product->id,
            'quantity' => 2,
        ]);
        $cart = factory(Cart::class)->create();

        $item = new CartItem([
            'productId' => $product->id,
            'quantity' => 3,
        ]);

        try {
            $cart->addItem($item);
        } catch (NotEnoughItemsInStockException $e) {
            $this->assertEquals(0, $cart->items()->count());

            return;
        }

        $this->fail('Item was added to cart when there were not enough in stock.');
    }

    /**
     * @test
     * @covers \Cart::removeItem
     */
    public function it_decreases_qty_of_the_item_in_the_cart()
    {
        $cart = factory(Cart::class)->create();
        $product = factory(Product::class)->create();
        factory(ProductInCart::class)->create([
            'product_id' => $product->id,
            'cart_id' => $cart->id,
            'quantity' => 3,
            'size_id' => null,
        ]);
        $item = new CartItem(['productId' => $product->id, 'quantity' => 2]);

        $cart->removeItem($item);

        $cart->refresh();
        $this->assertEquals(1, $cart->getItems()->count());
        $this->assertEquals(1, $cart->getItems()->first()->quantity);
    }

    /**
     * @test
     * @covers \Cart::removeItem
     */
    public function it_removes_the_item_completelty_if_resulting_qty_less_or_equals_to_0()
    {
        $cart = factory(Cart::class)->create();
        $product = factory(Product::class)->create();
        factory(ProductInCart::class)->create([
            'product_id' => $product->id,
            'cart_id' => $cart->id,
            'quantity' => 1,
            'size_id' => null,
        ]);
        $item = new CartItem(['productId' => $product->id, 'quantity' => 1]);

        $cart->removeItem($item);

        $cart->refresh();
        $this->assertEquals(0, $cart->getItems()->count());
    }

    /**
     * @test
     */
    public function it_drops_cart_item_all_together()
    {
        $cart = factory(Cart::class)->create();
        $product = factory(Product::class)->create();
        factory(ProductInCart::class)->create([
            'product_id' => $product->id,
            'cart_id' => $cart->id,
            'quantity' => 3,
            'size_id' => null,
        ]);
        $item = new CartItem(['productId' => $product->id, 'quantity' => 1]);

        $cart->dropItem($item);

        $cart->refresh();
        $this->assertEquals(0, $cart->getItems()->count());
    }

    /**
     * @test
     */
    public function it_has_custom_items_collection()
    {
        $cart = factory(Cart::class)->create();
        $product = factory(Product::class)->create();
        $size = factory(Size::class)->create();
        factory(ProductInCart::class)->create([
            'cart_id' => $cart->id,
            'product_id' => $product->id,
            'size_id' => $size->id,
            'quantity' => 10,
        ]);
        factory(ProductInCart::class)->create([
            'cart_id' => $cart->id,
        ]);
        factory(ProductInCart::class)->create([
            'cart_id' => $cart->id,
        ]);

        $items = $cart->getItems();

        $this->assertInstanceOf(CartItemsCollection::class, $items);
        $this->assertEquals(3, $items->count());
        $firstItem = $items->shift();
        $this->assertEquals($product->id, $firstItem->productId);
        $this->assertEquals($size->id, $firstItem->sizeId);
        $this->assertEquals(10, $firstItem->quantity);
        $this->assertEquals($size->value, $firstItem->sizeLabel);
        $this->assertEquals($product->name, $firstItem->productName);
    }

    /**
     * @test
     */
    public function it_counts_total_items_qty()
    {
        $cart = factory(Cart::class)->create();
        $product = factory(Product::class)->create();
        $size = factory(Size::class)->create();
        factory(ProductInCart::class)->create([
            'cart_id' => $cart->id,
            'product_id' => $product->id,
            'size_id' => $size->id,
            'quantity' => 3,
        ]);
        factory(ProductInCart::class)->create([
            'cart_id' => $cart->id,
            'quantity' => 3,
        ]);
        factory(ProductInCart::class)->create([
            'cart_id' => $cart->id,
            'quantity' => 1,
        ]);

        $this->assertEquals(7, $cart->totalItemsCount());
    }

    /**
     * @test
     * @covers \Cart::calculate
     */
    public function it_calculates_its_cost()
    {
        $cart = factory(Cart::class)->create();
        $costs = new CartCost([
            'itemsCost' => new Price(1500),
            'shippingCost' => new Price(100),
            'totalCost' => new Price(1600),
        ]);

        $calculator = $this->mock(CartCalculator::class);
        $calculator->shouldReceive('calculate')->once()->with($cart)->andReturn($costs);

        $calculated = $cart->calculate();

        $this->assertInstanceOf(CartCost::class, $calculated);
    }
}
