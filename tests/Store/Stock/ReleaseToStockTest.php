<?php

use App\Sales\Order\Order;
use App\Sales\Order\OrderItem;
use App\Sales\Order\Status\Events\OrderWasCancelled;
use App\Store\Product\Product;
use App\Store\Product\Size;
use App\Store\Stock\ReleaseToStock;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;

class ReleaseToStockTest extends TestCase
{
    use DatabaseTransactions;

    /**
     * @test
     * @covers \ReleaseToStock::handle
     */
    public function it_releases_order_items_back_to_stock()
    {
        $order = factory(Order::class)->create();
        $product = factory(Product::class)->create();
        $size = factory(Size::class)->create();
        factory(OrderItem::class)->create([
            'order_id' => $order->id,
            'product_id' => $product->id,
            'size_id' => $size->id,
            'quantity' => 3,
        ]);
        $this->assertEquals(0, $product->stock()->count());

        $event = new OrderWasCancelled($order->id);
        resolve(ReleaseToStock::class)->handle($event);

        $this->assertEquals(1, $product->stock()->count());
        $stock = $product->stock->first();
        $this->assertEquals(3, $stock->quantity);
        $this->assertEquals($size->id, $stock->size_id);
    }
}
