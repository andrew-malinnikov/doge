<?php

use App\Sales\Order\Order;
use App\Sales\Order\OrderItem;
use App\Sales\Order\OrderWasPlaced;
use App\Store\Product\Product;
use App\Store\Product\ProductInStock;
use App\Store\Stock\Restock;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;

class RestockTest extends TestCase
{
    use DatabaseTransactions;

    /**
     * @test
     * @covers \Restock::handle
     */
    public function it_restocks_the_stock()
    {
        $order = factory(Order::class)->create();
        $product = factory(Product::class)->create();
        $stock = factory(ProductInStock::class)->create([
            'product_id' => $product->id,
            'quantity' => 3,
            'size_id' => null,
        ]);
        factory(OrderItem::class)->create([
            'order_id' => $order->id,
            'product_id' => $product->id,
            'quantity' => 2,
            'size_id' => null,
        ]);

        $event = new OrderWasPlaced($order->id);
        resolve(Restock::class)->handle($event);

        $this->assertEquals(1, $stock->fresh()->quantity);
    }
}
