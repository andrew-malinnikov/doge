<?php

use App\Users\User;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class UserTest extends TestCase
{
    use DatabaseTransactions;
    use WithFaker;

    /**
     * @test
     * @covers \User::handle
     */
    public function it_checks_admin_role()
    {
        $email = $this->faker->unique()->safeEmail;
        config(['doge.admins' => [$email]]);
        $user = $user = factory(User::class)->make([
            'email' => $email,
        ]);
        $this->assertTrue($user->isAdmin());

        $this->assertFalse(
            factory(User::class)->make(['email' => 'test@example.com'])->isAdmin()
        );
    }
}
